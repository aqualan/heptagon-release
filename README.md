# HEPTAGON
## Alan Aquino // Alexander Ashery // Ryan Dungee

![alt text](https://bytebucket.org/aqualan/atlas/raw/37bf209af0234eef03bbd43a35ff21aa7184d341/LogowithText.jpg "wow such logo very hep")

### Required Python2.7 Packages
1. pymssql (Python-SQL wrapper)
2. xlsxwriter (.xlsx file generator)
3. xlrd (.xlsx file reader)
4. PyVISA (Python-VISA wrapper)
5. NumPy (computation)
6. SciPy (computation)
7. PySide (GUI)
8. matplotlib (graphing)
9. wiringpi2 (digital pin I/O)
10. python-smbus (low-level connection to GPIO)

### Python2.7 Setup
1. Go the [National Instruments website](http://www.ni.com/visa/) to download VISA, which allows us to communicate with scientific instruments.
2. Install PYVisa [here](http://pyvisa.readthedocs.org/en/latest/getting.html), which acts as a wrapper module for NI-VISA. Look for the NIVISA Runtime Engine.
3. Verify correct PYVisa installation by executing the following in either Windows or OSX:

	```python
	import visa
	rm = visa.ResourceManager()
	rm.list_resources()
	```

	After installing PyVISA, you'll need to make a change to `highlevel.py` in order for `loadwaveform.py` to play nice. Look for the line that reads out

	```
	count = self.write_raw(message.encode(enco))
	```

	and change it to

	```
	count = self.write_raw(message)
	```

	On OSX, the file in question is found under `/Library/Python/2.7/site-packages/PyVISA-1.5.dev4-py2.7.egg/pyvisa`.

4. OSX only: Install [Homebrew](http://brew.sh/) on OSX if you don't have it already. It's a great package manager that you can use for just about anything. Install the most up-to-date version of Python 2.7 with `brew install python`. 

5. Install `pip`, which you can use to install all of your Python packages. For OSX or Unix, you can just do the following:

	```
	sudo pip install freetds pymssql xlsxwriter xlrd pyvisa numpy scipy pyside matplotlib wiringpi2 python-smbus
	```

### Simple Stuff

Headers should include the following.

```python
import visa
```
Get the instrument descriptor using either National Instrument's VISA Interactive Control, or checking device settings physically. It may look something like `USB0::0x0699::0x040B::C021222::INSTR`

Open a new instance of the Resouce Manager, and then set your instrument as a variable. Then use `write()` to use VISA-specific commands.

```python
rm = visa.ResourceManager()
scope = rm.get_instrument(instrumentdescriptor)
scope.write("*IDN?")
print 'Instrument successfully identified itself as' + scope.read()

print 'Capturing screen image...'
scope.write('save:image "E:/pyScript.tif"')
```

For queries, instead of having to do separate `instr.write()` and `instr.read()` commands, you can simply do `instr.ask()` with the relevant command passed in as an argument.

### Raspberry Pi Setup
First install `vim`, with `sudo apt-get install vim` because `vim` is great.

In its current state, the Pi will act as a controller for the AMB testing board's 40-channel analog switching circuit.

If you're lacking an HDMI monitor, you can `ssh` into a Raspberry Pi if you share your internet connection over Ethernet. On OSX, go under System Preferences > Sharing. If the Pi has its default network settings enabled, your computer will act as the DHCP server, assigning itself 192.168.2.1 and the Pi 192.168.2.2. Perform `ssh pi@192.168.2.2`, entering "raspberry" as the password if defaults have been kept. `cat` your public key to `~/.ssh/authorized_keys` if you don't feel like entering a password every time you `ssh`. With a shared internet connection, you can easily `sudo apt-get install` or `sudo apt-get update` most packages and programs.

Note that the Pi natively runs Raspian, a Debian-based OS. Because of this, we cannot run NI-VISA, because National Instruments has only published signed versions for RedHat, Scientific Linux, and OpenSUSE. 

TODO: Get VISA working on the Pi.

### Switchboard Setup
We use MCP23017 serial port expanders to give the Pi an additional 16 digital pins per IC. MCP23017s are addressed via an I2C bus, and are individually designated with 3-bit address, meaning we can connect up to (8) 23017s for up to 128 additional pins. Note that if you're using more than (2) 23017s concurrently, the 23017s should be running on their own dedicated power and NOT the Pi's power rails, which have a current limit of 50 mA. 

To connect multiple 23017s, connect the SDL pins with a common line, and the SDA pins with another common line. These pins function as a communication bus, and the chips will only respond to commands issued to their specific address.

Consult a [datasheet of the MCP23017](http://ww1.microchip.com/downloads/en/DeviceDoc/21952b.pdf) and wire up the pins as follows:

* VDD : +3.3V

* VSS : GND

* SCL : SCL on the Pi (pin 5 on the Rev. 2)

* SDA : SDA on the Pi (pin 3 on the Rev. 2)

* RESET : +3.3V (the pin is labeled such that GND/floating resets the chip registers, while HIGH preserves state)

* A2/A1/A0 : Unique address bits for each 23017. Wire to HIGH (3.3V) for 1, GND (0V) for 0. For one chip, wire as GND/GND/GND. For two chips, make the second one GND/GND/HIGH. For three, GND/HIGH/GND. You get the idea.

You installed `python-smbus` earlier, which also included `i2c-tools` for locating I2C-capable devices on the Pi. We'll need to enable a few things on the Pi to give us access to the I2C bus.

We first edit the blacklist to enable I2C as a service.

`sudo vim /etc/modprobe.d/raspi-blacklist.conf`

Comment out the blacklist command for I2C such that the file looks like this:

```bash
# blacklist spi and i2c by default (many users don't need them)

blacklist spi-bcm2708
#blacklist i2c-bcm2708
```

Write and quit. Now do `sudo vim /etc/modules` and add `i2c-dev` so that the module is initialized during boot.

```bash
# /etc/modules: kernel modules to load at boot time.
#
# This file contains the names of kernel modules that will be run
# at boot time, one per line. Lines beginning with "#" are ignored.
# Parameters can be specified after the module name.

snd-bcm2835
i2c-dev
```

Write and quit. Reboot the Pi to activate the new settings. `sudo reboot`

Once back in Raspian, connect the Pi's SCL and SDA pins to the 23017(s) and run `sudo i2cdetect -y 1` on a Rev. 2 to scan the buses for capable devices. You should get an output that looks something like the following, where `--` indicates no response, and a hex number indicates a device at that address. In this example, (3) 23017s are connected with the address pins set as 000, 001, and 010.

```bash
pi@raspberrypi ~ $ sudo i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: 20 21 22 -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
```

Repeat the command a few times to ensure that the chips remain on the bus. If you see one or more chips sometimes there, sometimes not, then you probably have a loose wire somewhere; double-check all connections.

Attached is a simple program that goes over how to issue commands over the 23017s. To verify connectivity, throw LEDs onto the GPA0 pins or check the outputs with a voltmeter.

```python
# An example program for talking to multiple 23017s.

# The Python library, where we give it an easier name to call by
from time import sleep
import wiringpi2 as wiringpi 

# For waiting
from time import sleep

# The lowest available number is 65, but you can choose anything that you'd like. For multiple 23017s, the pin_bases must be at least 16 digits apart so that each pin address is distinct.
example_pin_base = 65 
pin_base1 = 100
pin_base2 = 200
pin_base3 = 300

# The I2C addresses indicated by i2c-detect. 0x20 corresponds to the chip wired as 000.
i2c_addr1 = 0x20
i2c_addr2 = 0x21
i2c_addr3 = 0x22

print("Initializing...")

# Initialize wiringpi
wiringpi.wiringPiSetup()

# Establish communications with and assign pins to the 23017s
wiringpi.mcp23017Setup(pin_base1, i2c_addr1)
wiringpi.mcp23017Setup(pin_base2, i2c_addr2)
wiringpi.mcp23017Setup(pin_base3, i2c_addr3)

# Set pin GPA0 on each 23017 to output.
wiringpi.pinMode(100, 1)
wiringpi.pinMode(200, 1)
wiringpi.pinMode(300, 1)

# Set each pin to LOW output
wiringpi.digitalWrite(100, 0)
wiringpi.digitalWrite(200, 0)
wiringpi.digitalWrite(300, 0)

print("Turning on lights!")

# We use a try/finally block so that if any error occurs, the 23017s are always put back to an inert state.
try:
	while True:
		# Turn pins HIGH
		wiringpi.digitalWrite(100, 1)
		sleep(1)
		wiringpi.digitalWrite(200, 1)
		sleep(1)
		wiringpi.digitalWrite(300, 1)
		sleep(1)

		# Turn pins LOW
		wiringpi.digitalWrite(100, 0)
		sleep(1)
		wiringpi.digitalWrite(200, 0)
		sleep(1)
		wiringpi.digitalWrite(300, 0)
		sleep(1)
finally:
	# On Ctrl-C or any error, return to a safe state.
	print("Quitting!")

	# Set all pins to 0V
	wiringpi.digitalWrite(100, 0)
	wiringpi.digitalWrite(200, 0)
	wiringpi.digitalWrite(300, 0)

	# Set all pins back to input
	wiringpi.pinMode(100, 0)
	wiringpi.pinMode(200, 0)
	wiringpi.pinMode(300, 0)
```

For more information on running the MCP23017s with a Raspberry Pi, check out the great tutorial [here](http://raspi.tv/2013/how-to-use-wiringpi2-for-python-on-the-raspberry-pi-in-raspbian#basics).