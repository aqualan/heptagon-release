#!/usr/bin/env python

'''
oscilloscope

Function:   Holds the oscilloscopeTab class, which holds all of the widgets
            for the oscilloscope tab. It's parent is the mainApplication class.
            Also holds the channelBox class, which is the layout/widgets for a channel
            control panel in oscilloscopeTab

Classes:    oscilloscopeTab, channelBox

Notes:      Functionality has to be incorporated for the save_state and load_state functions
            in oscilloscopeTab

            An additional channelBox shoudld be made in oscilloscopeTab for the math channel.
            This should be indicated as channel 5
'''

import sys
from PySide.QtCore import(Qt, Slot)
from PySide.QtGui import(QWidget, QTabWidget, QVBoxLayout, QPushButton, QHBoxLayout, QGroupBox, QCheckBox,
                         QLineEdit, QComboBox, QLabel, QFileDialog, QRadioButton, QApplication)

from waveform_preview import waveformPreview
from ScopeXL import export
from ImportScopeSettings import do as import_settings
from ExportScopeSettings import do as export_settings

class oscilloscopeTab(QWidget):
    
    '''
    The tab for holding widgets that are used to control/communicate with
    the oscilloscope.

    Functions: set_scope, get_waveform, save_waveform, save_state, load_state
    '''
    kind = 'controller'
    
    def __init__(self, parent=None):

        #---Initializing the widget.---#
        QWidget.__init__(self, parent)

        #---Generating widget contents.---#
        self.tabBar = QTabWidget()
        #---Tab 1: Controls---#
        
        #Generating Channel 1 Controls
        self.chanBox1 = channelBox('Channel 1 Settings', 1, self)
        self.chanBox2 = channelBox('Channel 2 Settings', 2, self)
        self.chanBox3 = channelBox('Channel 3 Settings', 3, self)
        self.chanBox4 = channelBox('Channel 4 Settings', 4, self)

        #Layout for holding control buttons
        self.controlButtonsLayout = QVBoxLayout()
        
        #Button for setting the scope to the user input
        self.setButton = QPushButton('Set Scope', self)
        self.setButton.setFixedWidth(175)
        self.setButton.setFixedHeight(60)
        self.setButton.clicked.connect(self.set_scope)
        self.setButton.setDisabled(1)

        #Button for saving scope state
        self.saveStateButton = QPushButton('Save Configuration')
        self.saveStateButton.setFixedWidth(175)
        self.saveStateButton.setFixedHeight(60)
        self.saveStateButton.clicked.connect(self.save_config)
        self.saveStateButton.setDisabled(1)

        #Button for loading scope state
        self.loadStateButton = QPushButton('Load Configuration')
        self.loadStateButton.setFixedWidth(175)
        self.loadStateButton.setFixedHeight(60)
        self.loadStateButton.clicked.connect(self.load_config)
        self.loadStateButton.setDisabled(1)

        #Button for grabbing waveforms
        self.grabWaveformButton = QPushButton('Grab Waveforms')
        self.grabWaveformButton.setFixedWidth(175)
        self.grabWaveformButton.setFixedHeight(60)
        self.grabWaveformButton.clicked.connect(self.grab_waveform)
        self.grabWaveformButton.setDisabled(1)

        #Formatting button row
        self.controlButtonsLayout.addWidget(self.setButton, alignment=Qt.AlignCenter)
        self.controlButtonsLayout.addWidget(self.saveStateButton, alignment=Qt.AlignCenter)
        self.controlButtonsLayout.addWidget(self.loadStateButton, alignment=Qt.AlignCenter)
        self.controlButtonsLayout.addWidget(self.grabWaveformButton, alignment=Qt.AlignCenter)
        self.controlButtonsLayout.setAlignment(Qt.AlignCenter)

        #Settings up layout for channel controls
        self.topLayout = QHBoxLayout()
        self.topLayout.addWidget(self.chanBox1)
        self.topLayout.addWidget(self.chanBox2)
        self.topLayout.addWidget(self.chanBox3)
        self.topLayout.addWidget(self.chanBox4)
        self.topLayout.addLayout(self.controlButtonsLayout)

        #Generating math channel controls
        self.chanBoxMath = QGroupBox('Math Channel Settings', self)

        self.mathOnOff = QCheckBox('On/Off', self)
        self.mathOnOff.toggled.connect(self.setMathOnOff)
        self.mathWriteOnOff = QCheckBox('Write New Operation', self)
        self.mathWriteOnOff.toggled.connect(self.set_mathwrite)
        self.mathexpress = QLineEdit()

        self.mathLayoutLine1 = QHBoxLayout()
        self.mathLayoutLine1.addWidget(self.mathOnOff)
        self.mathLayoutLine1.addWidget(self.mathWriteOnOff)
        self.mathLayout = QVBoxLayout()
        self.mathLayout.addLayout(self.mathLayoutLine1)
        self.mathLayout.addWidget(self.mathexpress)
        self.chanBoxMath.setLayout(self.mathLayout)
        self.chanBoxMath.setFixedWidth(220)
        self.chanBoxMath.setFixedHeight(80)
        self.chanBoxMath.setDisabled(1)

        self.refBox = QGroupBox('Send Reference Waveforms')

        self.refFileName = QLineEdit()
        self.refFileName.setReadOnly(1)
        self.refFileButton = QPushButton('...')
        self.refFileButton.setFixedWidth(50)
        self.refFileButton.clicked.connect(self.load_ref)
        self.chooseRefBox = QComboBox()
        self.chooseRefBox.addItems(['Reference 1', 'Reference 2',
                                    'Reference 3', 'Reference 4'])

        self.refLayoutLine1 = QHBoxLayout()
        self.refLayoutLine1.addWidget(QLabel('Select File: '))
        self.refLayoutLine1.addWidget(self.refFileName)
        self.refLayoutLine1.addWidget(self.refFileButton)

        self.refLayoutLine2 = QHBoxLayout()
        self.refLayoutLine2.addWidget(QLabel('Send waveform to: '))
        self.refLayoutLine2.addWidget(self.chooseRefBox, alignemtn=Qt.AlignLeft)
        self.refLayoutLine2.setAlignment(Qt.AlignLeft)

        self.refLeftLayout = QVBoxLayout()
        self.refLeftLayout.addLayout(self.refLayoutLine1)
        self.refLeftLayout.addLayout(self.refLayoutLine2)

        self.sendRefButton = QPushButton('Send Reference Waveform')
        self.sendRefButton.clicked.connect(self.send_reference)
        self.sendRefButton.setFixedHeight(50)
        self.sendRefButton.setFixedWidth(165)
        self.sendRefButton.setDisabled(1)

        self.refLayout = QHBoxLayout()
        self.refLayout.addLayout(self.refLeftLayout)
        self.refLayout.addWidget(self.sendRefButton)
        self.refBox.setLayout(self.refLayout)
        self.refBox.setFixedHeight(80)

        self.bottomLayout = QHBoxLayout()
        self.bottomLayout.addWidget(self.chanBoxMath)
        self.bottomLayout.addWidget(self.refBox)

        self.Layout = QVBoxLayout()
        self.Layout.addLayout(self.topLayout)
        self.Layout.addLayout(self.bottomLayout)
        self.setLayout(self.Layout)
        self.setFixedWidth(825)
        self.setFixedHeight(520)

        self.parent = parent

    @Slot()
    def grab_waveform(self):

        '''
        This function pulls the waveform data off of the oscilloscope and preview it in a new window.
        '''

        #Generating the preview window
        self.window = QWidget()
        self.window.setWindowModality(Qt.ApplicationModal)
        self.window.setWindowTitle('Oscilloscope Waveforms')

        #Screen for displaying the waveform data
        screen = waveformPreview(self.window)
        screen.setFixedWidth(475)
        screen.setFixedHeight(400)

        #Box for holding export settings
        commBox = QGroupBox('Export Waveform Data', self.window)

        self.titleBox = QLineEdit(self.window)
        
        titleLayout = QHBoxLayout()
        titleLayout.addWidget(QLabel('Set file title: '))
        titleLayout.addWidget(self.titleBox)

        self.waveformlist, self.timelist, self.vunits = self.parent.scope.get_waveforms() #Saving waveforms
        colors = ['yellow', 'blue', 'purple', 'gray', 'red'] #waveform colors

        #Generating checkboxes for selecting waveforms and plotting the waveforms.
        self.channelList = []
        channelLayout = QHBoxLayout()
        for i in range(self.parent.scope.numberChannels):
            checkbox = QCheckBox('Channel %i' % (i+1), self.window)
            self.channelList.append(checkbox)
            channelLayout.addWidget(checkbox)

            if self.waveformlist[i] != None:
                screen.oscilloscopePlot(colors[i], self.timelist[i], self.waveformlist[i])
                checkbox.setEnabled(1)
                checkbox.setCheckState(Qt.Checked)
            else: checkbox.setDisabled(1)

        #Math checkbox/preview
        checkbox = QCheckBox('Math Channel', self.window)
        self.channelList.append(checkbox)
        if self.waveformlist[-1] != None:
            screen.oscilloscopePlot(colors[-1], self.timelist[-1], self.waveformlist[-1])
            checkbox.setEnabled(1)
            checkbox.setCheckState(Qt.Checked)
        else: checkbox.setDisabled(1)
        channelLayout.addWidget(checkbox)

        #Button for exporting waveform data
        exportWaveButton = QPushButton('Export Data')
        exportWaveButton.clicked.connect(self.save_waveform)
        exportWaveButton.setFixedWidth(150)
        exportWaveButton.setFixedHeight(60)

        #Formatting communication box
        commLayout = QVBoxLayout()
        commLayout.addLayout(titleLayout)
        commLayout.addWidget(QLabel('Select Channels to Export:'))
        commLayout.addLayout(channelLayout)
        commLayout.addWidget(exportWaveButton)
        commBox.setLayout(commLayout)

        #Formatting window layout
        grabLayout = QVBoxLayout()
        grabLayout.addWidget(screen)
        grabLayout.addWidget(commBox)
        self.window.setLayout(grabLayout)
        self.window.show()
        
    @Slot()
    def send_reference(self):

        '''
        This function sends reference waveforms to the oscilloscope from a file.
        '''
        
        index = int(self.chooseRefBox.currentIndex()) + 1
        filename = self.refFileName.text().encode('ascii', 'ignore').strip()
        if filename == '':
            self.parent.scope_nofileerror()
            return
        
        try: self.parent.scope.loadRefs(index, filename)
        except:
            self.parent.general_nameerror()
            return

    @Slot()
    def load_ref(self):

        '''
        This function gets the name of a file for loading a reference waveform to the oscilloscope.
        '''

        chooseFile = QFileDialog()
        chooseFile.setNameFilter('Text Files (*.txt)')
        if chooseFile.exec_():
            fileName = chooseFile.selectedFiles()
            fileName = fileName[0]
            fileName.encode('ascii', 'ignore')
            self.refFileName.setText(fileName)

    @Slot()
    def set_mathwrite(self, state):

        '''
        This function enables or disables functionality for writing a new math expression to the oscilloscope.
        '''

        if state:
            self.mathexpress.setEnabled(1)
        else:
            self.mathexpress.setDisabled(1)

    @Slot()
    def save_config(self):

        '''
        This function saves the configuration of an oscilloscope to a .txt file.
        '''

        chooseSaveData = QFileDialog()
        chooseSaveData.setAcceptMode(QFileDialog.AcceptSave)
        chooseSaveData.setDefaultSuffix(unicode('txt'))
        if chooseSaveData.exec_():
            saveName = chooseSaveData.selectedFiles()
            saveName = saveName[0]
            saveName.encode('ascii', 'ignore')
            try:
                export_settings(saveName, self.parent.scope)
                return
            except:
                self.nameerror.emit()
                return

    @Slot()
    def load_config(self):

        '''
        This function loads a previous configuration to the oscilloscope from a .txt file.
        '''

        chooseFile = QFileDialog()
        chooseFile.setNameFilter('Text Files (*.txt)')
        if chooseFile.exec_():
            fileName = chooseFile.selectedFiles()
            fileName = fileName[0]
            fileName.encode('ascii', 'ignore')
            try:
                import_settings(fileName, self.parent.scope)
                return
            except:
                self.nameerror.emit()
                return

    @Slot()
    def setMathOnOff(self, state):

        '''
        Action performed by mathOnOff. Indicates if the user wants the math channel
        switched on or off. The function checks if the user has placed a checkmark
        in the box and saves the information in the mathState attribute of mainApplication's
        instance of the sillyscope class.
        '''

        if state:
            self.mathexpress.setEnabled(1)
            self.parent.scope.mathState = 'ON'
        else:
            self.mathexpress.setDisabled(1)
            self.parent.scope.mathState = 'OFF'

    @Slot()
    def set_scope(self):

        '''
        Action performed by setButton. It sets the settings on the scope to the settings
        indicated in the tab by calling the set_scope function from the sillyscope class.

        Arguments: None
        '''
        
        self.parent.scope.set_scope()

    @Slot()
    def save_waveform(self):

        '''
        Action performed by saveWaveformButton Opens a file dialog that allows the user to export
        the oscilloscope data into an excel file.

        Arguments: None
        '''
        
        onoffs = [] #Will be used to hold a list of booleans that indicate if the associated channel is having
                    #data exported.

        #Populating onoffs
        for i in range(self.parent.scope.numberChannels + 1):
            if self.channelList[i].isChecked():
                onoffs.append(True)
            else:
                onoffs.append(False)

        #Opening up a file dialog
        chooseSaveData = QFileDialog()
        chooseSaveData.setAcceptMode(QFileDialog.AcceptSave) #Setting the file dialog as a save dialog
        chooseSaveData.setDefaultSuffix(unicode('xlsx')) #Setting file format to excel.
        if chooseSaveData.exec_():
            saveName = chooseSaveData.selectedFiles()
            saveName = saveName[0]
            saveName.encode('ascii', 'ignore')
            
            #Exporting the data using the export function
            export(saveName, self.titleBox.text().encode('ascii', 'ignore'),
                   self.waveformlist, self.timelist, onoffs)

        self.window.close()
            

class channelBox(QGroupBox):

    '''
    Class for holding the widgets/layout of each channel control panel that is placed into the
    oscilloscopeTab class. It inherits attributes from PySide's QGroupBox class. For initialization,
    it takes in the name of the channel and the channel's number, as well as its parent (the
    oscilloscopeTab class.

    Functions: setOnOff, setInvert, setImpedance50, setImpedanceMEG
    '''

    def __init__(self, channel_name, channel_number, parent=None):

        #---Initializing the class---#
        QGroupBox.__init__(self, channel_name, parent) #Initializing QGroupBox attributes
        
        #Setting dimensions
        self.setFixedHeight(300)
        self.setFixedWidth(130)

        #---Generating widgets.---#

        #On/Off Checkbox
        self.OnOff = QCheckBox('On/Off', self)
        self.OnOff.toggled.connect(self.setOnOff)

        #Invert signal checkbox
        self.invert = QCheckBox('Invert Signal', self)
        self.invert.toggled.connect(self.setInvert)

        #Channel impedance controls
        self.impedanceLabel = QLabel('Set Impedance:')
        self.impedanceLabel.setFixedHeight(20)
        self.radio50 = QRadioButton('50 Ohms', self)
        self.radio50.toggled.connect(self.setImpedance50)
        self.radio1M = QRadioButton('1M Ohms', self)
        self.radio1M.toggled.connect(self.setImpedanceMEG)

        #Reference waveforms
        self.referenceLabel = QLabel('Reference Waveforms:')
        self.referenceLabel.setFixedHeight(20)
        self.referenceOnOff = QCheckBox('On/Off', self)
        self.referenceOnOff.toggled.connect(self.setReferenceOnOff)

        #---Setting box layout.---#
        self.Layout = QVBoxLayout()
        self.Layout.addWidget(self.OnOff)
        self.Layout.addSpacing(10)
        self.Layout.addWidget(self.invert)
        self.Layout.addSpacing(10)
        self.Layout.addWidget(self.impedanceLabel)
        self.Layout.addWidget(self.radio50)
        self.Layout.addWidget(self.radio1M)
        self.Layout.addSpacing(10)
        self.Layout.addWidget(self.referenceLabel)
        self.Layout.addWidget(self.referenceOnOff)
        self.setLayout(self.Layout)

        self.setDisabled(1) #Disables channel functionality on initialization

        self.channel_number = channel_number #Storing the channel number in a local variable

        self.parent = self.parent() #Storing the parent location in a local variable

    @Slot()
    def setOnOff(self, state):

        '''
        Action peformed by the OnOff checkbox. Indicates if the user wants the corresponding channel
        to be set on or off. The function checks to see if the checkbox is checked or unchecked,
        and then writes the corresponding channel state to the stateStore list in mainApplication's
        instance of the sillyscope class.

        Arguments:

            state: The check state of the checkbox.
        '''

        if state:
            self.parent.parent.scope.stateStore[self.channel_number - 1] = 'ON'
            self.radio50.setEnabled(1)
            self.radio1M.setEnabled(1)
            self.invert.setEnabled(1)
        else:
            self.parent.parent.scope.stateStore[self.channel_number - 1] = 'OFF'
            self.radio50.setDisabled(1)
            self.radio1M.setDisabled(1)
            self.invert.setDisabled(1)

    @Slot()
    def setInvert(self, state):

        '''
        Action performed by the invert checkbox. Indicates if the user wnats the corresponding channel's
        signal to be inverted or not. The function checks to see if the checkbox is checked or unchecked,
        and then writes the corresponding channel inversion state to the invertStore list in mainApplication's
        instance of the sillyscope class.

        Arguments:

            state: The check state of the checkbox.
        '''

        if state:
            self.parent.parent.scope.invertStore[self.channel_number - 1] = 'ON'
        else:
            self.parent.parent.scope.invertStore[self.channel_number - 1] = 'OFF'

    @Slot()
    def setImpedance50(self):

        '''
        Action performed by the radio50 radio button. Indicates that the user wants the corresponding
        channel's impedance to be set to 50 ohms. The function is activated when the radio button is clicked,
        and then writes the corresponding impedance state to the impedanceStore list in mainApplication's
        instane of the sillyscope class.

        Arguments: None
        '''
        
        self.parent.parent.scope.impedanceStore[self.channel_number - 1] = 'FIFTY'

    @Slot()
    def setImpedanceMEG(self):

        '''
        Action performed by the radioMEG radio button. Indicates that the user wants the corresponding
        channel's impedance to be set to 1 Mohm. The function is activated when the radio button is clicked,
        and then writes the corresponding impedance state to the impedanceStore list in mainApplication's
        instance of the sillyscope class.

        Arguments: None
        '''
        
        self.parent.parent.scope.impedanceStore[self.channel_number - 1] = 'MEG'

    @Slot()
    def setReferenceOnOff(self, state):

        '''
        Action performed by the referenceOnOff checkbox. Indicates that the user wants the corresponding
        channel's reference waveform to be set on or off. The function is activated when the checkbox is
        toggled, and then writes the corresponding state to the referenceStore list in mainApplication's
        instance of the sillyscope class.
        '''

        if state:
            self.parent.parent.scope.referenceStore[self.channel_number - 1] = 'ON'
        else:
            self.parent.parent.scope.referenceStore[self.channel_number - 1] = 'OFF'

        
#For previewing the tab outside of the main application
if __name__ == "__main__":
    qtApp = QApplication(sys.argv)
    settingsWindow = oscilloscopeTab()
    settingsWindow.show()
    sys.exit(qtApp.exec_())
