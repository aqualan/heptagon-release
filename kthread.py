#!/usr/bin/env python

'''
kthread.py

Function:     Holds the KThread class, which is a killable thread, as well
              as KException, which is used to kill the thread.

Classes:      KException, KThread

Notes:        This code has lovingly been borrowed from Connelly Barnes, to whom
              all of the credit for this code is due. It has only been changed
              in that where the original code uses Python threads, this version
              of the kthread uses PySide QThreads instead. To see the original
              kthread, visit:
              https://mail.python.org/pipermail/python-list/2004-May/281943.html
'''
import sys
import trace

from PySide.QtCore import QThread

class KException(Exception):
  '''
  A unique exception raised only when the user has indicated that they
  want to kill a thread.
  '''
  def _init__(self):
    return

class KThread(QThread):
  
  """A subclass of QThread, with a kill() method."""
  
  def __init__(self, *args, **keywords):
    QThread.__init__(self, *args, **keywords)
    self.killed = False

  def start(self):
    """Start the thread."""
    self.__run_backup = self.run
    self.run = self.__run      # Force the Thread to install our trace.
    QThread.start(self)

  def __run(self):
    """Hacked run function, which installs the trace."""
    sys.settrace(self.globaltrace)
    self.__run_backup()
    self.run = self.__run_backup

  def globaltrace(self, frame, why, arg):
    if why == 'call':
      return self.localtrace
    else:
      return None

  def localtrace(self, frame, why, arg):
    if self.killed:
      if why == 'line':
        raise KException()
    return self.localtrace

  def kill(self):
    self.killed = True
