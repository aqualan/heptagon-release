'''
Runs the automated AMB testing off of a Raspberry Pi. See documentation for how
the Pi should be hooked up to the switchboard and multiplexers.

The switchboard with the MCP23017 serial port expanders is necessary because
the Pi only have 17 native digital I/O pins. We can hook up to 8 expanders if we
so chose to, because each expander is referenced with a biased hardware address.
This is set by tying the A0/A1/A2 pins on each 23017 to either +3V or GND. Each
23017 is connected to the Pi by common SDA/SCL lines via the I2C protocol, and
are referenced by the Pi by their hardware address.

Pins 3 and 5 on the Rev. 2 Pi belong to SDA and SCL respectively. To keep 
current draw through the Pi reasonable, we will supply +3V to each expander 
externally instead of pulling it from the Pi's pins. 

PACKAGES:
Uses the wiring2pi Python package in order to simplify pin programming.
'''

import wiring2pi as wiringpi
import visa
import AMBTestCLI as test

# Lowest available number in wiring2pi is 65; each 23017 has 16 pins
pinbase1 = 100 	# Controls input switches for channels 1-16
pinbase2 = 200 	# Controls input switches for channels 17-32
pinbase3 = 300 	# Controls input switches for channels 33-40 and
			   	# Controls mux selects for ADC pairs 1-8
pinbase4 = 400 # Controls mux selects for ADC pairs 9-24
pinbase5 = 500 # Controls mux selects for ADC pairs 25-40

scopedescriptor = "12"
scope = None

# Connect to the scope and obtain a handle to pass to AMBTest
def connect():
	rm = visa.ResourceManager()
	scope = rm.get_instrument(scopedescriptor)

# Get the pins ready for output
def initialize():
	# Addressing for the expanders starts at 0x20; verify connectivity with 
	# sudo i2cdetect -y 1 for a Rev. 2 Pi; -y 1 for a Rev. 1
	expander1 = 0x20
	expander2 = 0x21
	expander3 = 0x22
	#expander4 = 0x23
	#expander5 = 0x24

	# Initialize wiringpi
	wiringpi.wiringPiSetup()
	# Initialize 23017 interface
	wiringpi.mcp23017Setup(pinbase1, expander1)
	wiringpi.mcp23017Setup(pinbase2, expander2)
	wiringpi.mcp23017Setup(pinbase3, expander3)
	#wiringpi.mcp23017Setup(pinbase4, expander4)
	#wiringpi.mcp23017Setup(pinbase5, expander5)

	# Initialize all expander pins to 0
	for i in range(1, 6):
		for j in range(i*100, i*100 + 16):
			wiringpi.pinMode(i, 1) # Sets pin as output
			wiringpi.digitalWrite(i, 0) # Sets output as GND

# Fully automatic test; runs 1 channel at a time
def fullAutoTest():
	# Ask about reference waveforms
	test.initialize(scope)
	test.checkRefs()
	r1 = test.measureWave(5, "ref1")
	r2 = test.measureWave(6, "ref2")
	r3 = test.measureWave(7, "ref3")
	r4 = test.measureWave(8, "ref4")
	for group in range(1, 11):
		for channel in range(1, 5):
			# Turn on correct pins
			wiringpi.digitalWrite(channel, 1) # Input HIGH
			'''TODO'''
			#wiringpi.digitalWrite(channel+41, 1) # Output HIGH

			# Perform measurements
			ch1  = test.measureWave(1, "ch1")
			test.checkParams(1, ch1, r1)
			ch2  = test.measureWave(2, "ch2")
			test.checkParams(2, ch2, r2)
			ch3  = test.measureWave(3, "ch3")
			test.checkParams(3, ch3, r3)
			math = test.measureWave(4, "math")
			test.checkParams(4, math, r4)

			print("Channel %d complete. Switching." %(i))
			# Turn off previously tested pins.
			wiringpi.digitalWrite(channel, 0) # Input LOW
			'''TODO'''
			#wiringpi.digitalWrite(channel+41, 0) # Output LOW

# We use a try/finally block to call our functions so that even on failure,
# the pins are all reset to safe values.
try:
	connect()
	print("Connection to instruments successful.\n")
	initialize()
	raw_input("Python-Pi interface initialized." + 
		"Press any key to begin automatic 1-channel test.\n")
finally:
	# Reset all pins to safe values.
	for i in range(1, 6):
		for j in range(i*100, i*100 + 16):
			wiringpi.digitalWrite(i, 0) # Sets output as GND
			wiringpi.pinMode(i, 0) # Sets pin as input

	# Turn off measurements
	for i in range (0, 9):
		scope.write("measurement:meas%d:state off" %(i))
