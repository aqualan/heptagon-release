#!/usr/bin/env python

'''
resultsTab.py

Function:       Holds the classes for generating in-application results previews.

Classes:        linearityResultsTab

Notes:          *When the auto test is complete, the results preview tab should
                 be a class in this file named autoResultsTab.
'''

import sys
from PySide.QtCore import(Qt, Slot)
from PySide.QtGui import(QWidget, QVBoxLayout, QHBoxLayout, QGroupBox, QLabel, QTableWidget,
                         QPushButton,QMessageBox, QFileDialog, QApplication, QTableWidgetItem)
from linearitytest import exportLinearity
from waveform_preview import waveformPreview

class linearityResultsTab(QWidget):

    '''
    The tab for previewing the results from a linearity test.

    Functions: export_results, close_tab
    '''

    kind = 'results'

    def __init__(self, which, results, index, parent=None):

        QWidget.__init__(self, parent)

        self.parent = parent
        self.index = index
        self.exported = False #Tells whether or not the data held in the tab has been exported yet.

        #Getting data from the dictionary returned by the linearity test.
        self.results = results
        self.type = which
        self.slope = results.get('slope')
        self.intercept = results.get('intercept')
        self.r_value = results.get('r_value')
        self.r2_value = self.r_value**2
        self.std_err = results.get('std_err')
        self.inputvalues = results.get('inputvalues')
        self.outputvalues = results.get('outputvalues')
        self.outputflag = results.get('outputflag')
        self.regression = []
        self.percentdiff = results.get('percentdiff')
        self.fullscale = results.get('fullscale')
        for i in range(len(self.inputvalues)): self.regression.append(self.inputvalues[i]*self.slope + self.intercept) #Generating regression values

        self.Layout = QVBoxLayout()

        self.topLayout = QHBoxLayout()

        #Box for displaying test results data
        self.resultsBox = QGroupBox('Results')
        self.resultsBox.setFixedWidth(350)
        
        self.resultsLayoutLine1 = QHBoxLayout()
        self.resultsLayoutLine1.addWidget(QLabel('Slope: %s' % self.slope))
        self.resultsLayoutLine1.addWidget(QLabel('Y-Intercept: %s' % self.intercept))
        self.resultsLayoutLine1.addWidget(QLabel('R Value: %s' % self.r_value))

        self.resultsLayoutLine2 = QHBoxLayout()
        self.resultsLayoutLine2.addWidget(QLabel('R^2 Value: %s' % self.r2_value))
        self.resultsLayoutLine2.addWidget(QLabel('Standard Error: %s' % self.std_err))

        #Table for displaying data
        self.dataTable = QTableWidget()
        self.dataTable.setRowCount(len(self.inputvalues))
        self.dataTable.setColumnCount(5)
        self.dataTable.setHorizontalHeaderLabels(('Input Signal', 'Output Signal',
                                                  'Regression Points', 'Percent Difference', 'Full Scale Error'))
        for i in range(len(self.inputvalues)):
            tableItem = QTableWidgetItem(str(self.inputvalues[i]))
            tableItem.setFlags(tableItem.flags() != Qt.ItemIsEditable)
            self.dataTable.setItem(i, 0, tableItem)
            tableItem = QTableWidgetItem(str(self.outputvalues[i]))
            tableItem.setFlags(tableItem.flags() != Qt.ItemIsEditable)
            self.dataTable.setItem(i, 1, tableItem)
            tableItem = QTableWidgetItem(str(self.regression[i]))
            tableItem.setFlags(tableItem.flags() != Qt.ItemIsEditable)
            self.dataTable.setItem(i, 2, tableItem)
            tableItem = QTableWidgetItem(str(self.percentdiff[i]))
            tableItem.setFlags(tableItem.flags() != Qt.ItemIsEditable)
            self.dataTable.setItem(i, 3, tableItem)
            tableItem = QTableWidgetItem(str(self.fullscale[i]))
            tableItem.setFlags(tableItem.flags() != Qt.ItemIsEditable)
            self.dataTable.setItem(i, 4, tableItem)
        self.dataTable.resizeColumnsToContents()

        #Button for saving the linearity test results to an excel file
        self.exportDataButton = QPushButton('Export Test Results')
        self.exportDataButton.clicked.connect(self.export_results)
        self.exportDataButton.setFixedWidth(120)

        self.resultsBoxLayout = QVBoxLayout()

        self.resultsBoxLayout.addLayout(self.resultsLayoutLine1)
        self.resultsBoxLayout.addLayout(self.resultsLayoutLine2)
        self.resultsBoxLayout.addWidget(self.dataTable)
        self.resultsBoxLayout.addWidget(self.exportDataButton)
        self.resultsBox.setLayout(self.resultsBoxLayout)

        #Graph for displaying data
        self.linScreen = waveformPreview()
        self.linScreen.linearityPlot(slope=self.slope, intercept=self.intercept,
                                         inputsig=self.inputvalues, outputsig=self.outputvalues)
        self.linScreen.setFixedWidth(350)
        self.linScreen.setFixedHeight(300)
        self.linScreen.setLayout(QVBoxLayout().addWidget(self.linScreen))

        #Formatting top layout
        self.topLayout.addWidget(self.resultsBox)
        self.topLayout.addWidget(self.linScreen)

        #Layout for holding graphs
        self.graphLayout = QHBoxLayout()


        #Graph for displaying percent deviation
        self.devScreen = waveformPreview()
        self.devScreen.deviationPlot(self.inputvalues, self.percentdiff)
        self.devScreen.setFixedWidth(350)
        self.devScreen.setFixedHeight(300)
        self.devScreen.setLayout(QVBoxLayout().addWidget(self.devScreen))

        #Graph for displaying full scale deviation
        self.fullScreen = waveformPreview()
        self.fullScreen.fullscalePlot(self.inputvalues, self.fullscale)
        self.fullScreen.setFixedWidth(350)
        self.fullScreen.setFixedHeight(300)

        #Adding graphs to graph layout
        self.graphLayout.addWidget(self.fullScreen)
        self.graphLayout.addWidget(self.devScreen)

        #Formatting the bottom panel
        self.Layout.addLayout(self.topLayout)
        self.Layout.addLayout(self.graphLayout)
        self.setLayout(self.Layout)

        #Sets the tab so it is deleted if it is closed.
        self.setAttribute(Qt.WA_DeleteOnClose, True)

        #Generating/formatting window that shows if the tab is closed before results have been shown
        self.closewindow = QMessageBox()
        self.closewindow.setText('The results from this linearity test have not ' +
                                           'been saved yet. Would you like to save these results?')
        self.closewindow.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
        self.closewindow.setDefaultButton(QMessageBox.Save)
        

    @Slot()
    def export_results(self):

        '''
        Action performed by exportDataButton. Opens a file dialog that allows the user to save
        the results from the linearity test to an excel file.

        Arguments: None
        '''
        
        chooseSaveData = QFileDialog()
        chooseSaveData.setAcceptMode(QFileDialog.AcceptSave)
        chooseSaveData.setDefaultSuffix(unicode('xlsx'))
        if chooseSaveData.exec_():
            saveName = chooseSaveData.selectedFiles()
            saveName = saveName[0]
            saveName.encode('ascii', 'ignore')
            calex = 1 if self.type == 'Calibration' else 0
            exportLinearity(saveName, self.results, calibrationExport=calex)
            self.exported = True

    def close_tab(self):

        '''
        This function checks to see if the tabs data has been saved. If the data has been saved, the tab is closed and deleted.
        If not, the tab asks the user to confirm their decision or save the data.
        '''

        if self.exported: self.deleteLater()
        else:
            reply = QMessageBox.warning(self, 'Save Results?',
                                         "The data from this test has not been saved yet.\n Would you like to " +
                                        "save the results from this tab?", QMessageBox.Save |
                                         QMessageBox.Discard | QMessageBox.Cancel, QMessageBox.Save)
            if reply == QMessageBox.Save:
                self.export_results()
                if self.exported: self.deleteLater()
            elif reply == QMessageBox.Discard: self.deleteLater()
            else: return


if __name__ == '__main__':

    qtApp = QApplication(sys.argv)
    results = {'slope': 1, 'intercept': 0, 'r_value': 1, 'std_err': 0, 'inputvalues': [0, 1, 2, 3], 'outputvalues': [0, 1, 2, 3], 'outputflag': 'max', 'percentdiff': [0.1, 0.1, 0.1, 0.1], 'fullscale': [0.1, 0.1, 0.1, 0.1]}
    window = linearityResultsTab('Linearity', results, 1)
    window.show()
    sys.exit(qtApp.exec_())
    
