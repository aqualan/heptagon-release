'''
Imports essential settings from a file into the oscilloscope

Usage: 	Execute directly with `python ImportScopeSettings.py`; the program will
		take care of the rest.

Arguments: 	None
'''

# For the 4034 scope in Mitch's Lab
# instrumentdescriptor = 'USB0::0x0699::0x040B::C021222::INSTR'

# For the 4054 scope in Mitch's Lab
instrumentdescriptor = 'USB0::0x0699::0x0401::C020278::INSTR'

import visa
import StringIO
import csv
import sys

# Method for easy sending of commands in a command group
def parse(scope, lines, index):
	while lines[index] != "":
		# Write command at row [index] to the scope
		scope.write(lines[index])
		index += 1

# Set acquire settings
def setupAcquire(scope, lines, index):
	#print("Setting acquire parameters from line %d..." %(index))
	parse(scope, lines, index)

# Set analog settings
def setupAnalog(scope, lines, channel, index):
	#print("Setting up analog channel %d from line %d..." %(channel, index))
	while lines[index] != "":
		if ":PRO:ID:TYP" in lines[index] or "SER" in lines[index]:
			# Ignore probe type and serial number, as these cannot be set.
			pass
		else :
			# Write command at row[index] to the scope
			scope.write(lines[index])
		index += 1

# Set horizontal settings
def setupHorizontal(scope, lines, index):
	#print("Starting with the horizontal settings from line %d..." %(index))
	parse(scope, lines, index)

# Set math settings	
def setupMath(scope, lines, index):
	#print("Setting math parameters from line %d..." %(index))
	parse(scope, lines, index)

# Set trigger settings
def setupTrigger(scope, lines, trigChannel, index):
	#print("Moving onto Trigger %s settings from line %d..." %(trigChannel, index))
	parse(scope, lines, index)

# martychangwhatdoido = The file to read from
def do(martychangwhatdoido, scope):
	#filename = raw_input("Please enter a filename to import from.\n>>> ")
	filename = martychangwhatdoido
	settings = open(filename, 'r')

	# Connect to instrument
	rm = visa.ResourceManager()
	# scope = rm.get_instrument(instrumentdescriptor)
	scope.write('header off')
	# Array with the line-by-line elements
	lines = [line.strip() for line in settings]

	# Main method
	i = 0 # Line index
	for line in lines:
		if(line == "1."):
			setupHorizontal(scope, lines, i+1)
		elif(line == "2."):
			setupAnalog(scope, lines, 1, i+1)
		elif(line == "3."):
			setupAnalog(scope, lines, 2, i+1)
		elif(line == "4."):
			setupAnalog(scope, lines, 3, i+1)
		elif(line == "5."):
			setupAnalog(scope, lines, 4, i+1)
		elif(line == "6."):
			setupTrigger(scope, lines, "A", i+1)
		elif(line == "7."):
			setupTrigger(scope, lines, "B", i+1)
		elif(line == "8."):
			setupTrigger(scope, lines, "Global", i+1)
		elif(line == "9."):
			setupAcquire(scope, lines, i+1)
		elif(line =="10."):
			setupMath(scope, lines, i+1)
		elif(line == "AFG"):
			#print("You tried to load AFG settings into the scope. Don't do that.")
			sys.exit()
		else:
			pass
		i = i+1
	#print("%d lines parsed. Scope is ready to go!" %(i))
