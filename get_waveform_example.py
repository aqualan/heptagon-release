#-------------------------------------------------------------------------------
#  Get a screen catpure from DPO4000 series scope and save it to a file

# python        2.7         (http://www.python.org/)
# pyvisa        1.5         (http://pyvisa.sourceforge.net/)
# numpy         1.6.2       (http://numpy.scipy.org/)
# MatPlotLib    1.0.1       (http://matplotlib.sourceforge.net/)
#-------------------------------------------------------------------------------

import visa
import numpy as np
from struct import unpack
import pylab

rm = visa.ResourceManager()
scope = rm.get_instrument('USB0::0x0699::0x0401::C020278::INSTR')

scope.write('DATA:SOU CH2')
scope.write('DATA:WIDTH 1')
scope.write('DATA:ENC RPB')

scope.write('HEAD OFF')
ymult = float(scope.ask('WFMOUTPRE:YMULT?'))
yzero = float(scope.ask('WFMOUTPRE:YZERO?'))
yoff = float(scope.ask('WFMOUTPRE:YOFF?'))
xincr = float(scope.ask('WFMOUTPRE:XINCR?'))
scope.write('HEAD ON')

scope.write('CURVE?')
data = scope.read_raw()
headerlen = 2 + int(data[7])
header = data[:headerlen]
ADC_wave = data[headerlen:-1]

ADC_wave = np.array(unpack('%sB' % len(ADC_wave),ADC_wave))

Volts = (ADC_wave - yoff) * ymult  + yzero

Time = np.arange(0, xincr * len(Volts), xincr)

pylab.plot(Time, Volts)
pylab.show()

