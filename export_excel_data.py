from xlwt import *

def write_file(data_list, time_list, title, list_name):
    '''Writes data from a list to an excel file.'''

    #Styling
    style = easyxf(
        'align: horizontal center'
        )

    #Creating workbook/worksheet
    w = Workbook()
    ws = w.add_sheet(title)

    #Adding title
    ws.write_merge(0, 0, 0, 2, title, style)

    #Adding data
    for i in range(len(data_list)):
               ws.write(i + 2, 0, time_list[i], style)
               ws.write(i + 2, 1, data_list[i], style)

    #Save file
    w.save(list_name)
