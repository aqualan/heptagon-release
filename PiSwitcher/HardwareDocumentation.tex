\documentclass{article}

\usepackage{graphicx} % For images
\newcommand{\inlinecode}{\texttt} % For in-line code.
\usepackage{float}
\usepackage{hyperref} % For clickable ToC
\hypersetup{
	colorlinks,
	citecolor=black,
	filecolor=black,
	linkcolor=black,
	urlcolor=black
}

% For code block
\usepackage{listings}

\lstset{language=Python, tabsize=4, basicstyle=\ttfamily, breaklines=true}

\title{PiSwitcher Documentation}
\author{Alan Aquino - aaquino@seas.upenn.edu}

\begin{document}

\maketitle

\begin{abstract}
The following text goes over the hardware involved with a RaspberryPi-controlled interface for switching the input and output analog signals between different channels on a test board. Designed in the summer of 2014, a prototype using a Rev.2 Pi, TS5A3166 analog switches, and MCP23017 serial port expanders was demonstrated to provide proof-of-concept and to act as an alternative to a multiplexer-based system. Using independent analog switches, one can drive an arbitrary number of inputs concurrently. The WiringPi Python library acts as an interface for the user to control switch states with the Raspberry Pi. As of this writing, the switching has been tested to be working from a command-line interface, but is not linked up to the HEPTAGON GUI. 
\end{abstract}

\tableofcontents

\section{Components}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{pi.png}
\end{figure}

A Revision 2 Raspberry Pi microcontroller acts as the brains of the switching hardware. Thanks to its relative inexpensiveness and the ease with which it can be programmed, the Pi proved to be an easy choice when it came for designing the circuit. In conjunction with WiringPi2, the Pi abstracts the driving of HI/LO signals by simply sending a HI/LO command from the terminal.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{pipinout.jpg}
\end{figure}

Pictured above are the pins available on the Raspberry Pi. Even though +3V and +5V outputs are available, due to current limitations on the Pi, we choose to power our components externally. The general purpose IO (GPIO) pins are our digital outputs, which can be set to HI/LO. An astute observer will note that there are only 17 usable GPIO pins, which is not enough to be able to control 40 analog switches. That's where the following component comes in.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{23017.png}
\end{figure}

The Microchip Technology (MCP) 23017 is a serial port expander that allows us to add GPIO pins to the Raspberry Pi. A controller can communicate with a 23017 via the I2C serial protocol, over the SDA (serial data) and SCL (serial clock) lines. The address pins (A2-A0) allow up to (8) 23017s to be connected with common SCL/SDA lines. Biasing an address pin to VDD sets it as '1', while biasing one to GND sets it as '0'. Discussed in Section \ref{sec:operation}, this allows each 23017 to be individually referenced and controlled. 
\begin{center}
	\begin{tabular}{| c | c |}
	 \hline
	 A2/A1/A0 & Hardware Address \\ 
	 \hline
	 000 & 0x20 \\
	 001 & 0x21 \\
	 010 & 0x22 \\
	 011 & 0x23 \\
	 100 & 0x24 \\
	 101 & 0x25 \\
	 110 & 0x26 \\
	 111 & 0x27 \\
	 \hline
	\end{tabular}
\end{center}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{3166.png}
\end{figure}

The last part of our circuit are the analog switches themselves, Texas Instruments TS5A3166DCKRs. To get them breadboarded, they have been soldered onto adapters due to their small package size. An input signal can be driven to either NO or to COM, and the output can be tied to the pin that is not input. The 3166s develop their lowest resistance (~1 Ohm) at +5V. Logic HI is defined as +2V to +5V, while Logic LO is defined as +0.8V down to GND. 

\section{Failsafes}
The 23017s maintain state via onboard shift registers. If the operating voltage is dropped, then the registers will reset such that all pins are set into INPUT mode, and transmit GND when checked with a multimeter.

The TS5A3166s are NO (normally open) switches. Either a loss in voltage or the 23017s dropping their outputs, which act as the switch control signals, will cause the 3166s to open.

\section{Operation} 
\label{sec:operation}
Make sure that the Pi is set up with the proper Python libraries installed, a list of which can be found on HEPTAGON's repo on Bitbucket, along with a modification to enable the I2C protocol. 

Included in the parts box should be a Rev.2 Pi labelled as \#4, which has been preloaded with the necessary files and libraries. It has also been assigned a static IP address of 192.168.1.100.

Set up the circuit as follows. \textbf{From FN Generator} can be some arbitrary signal, while \textbf{To Testing Board} can be an arbitrary output like a scope probe.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\textwidth]{schematic.png}
\end{figure}

Then, perform the following steps. An example program performing all of the below functionality is included in the HEPTAGON repo's README.md and in Section \ref{sec:ex}.
\begin{enumerate}
	\item Connect the Pi to the circuit as in the schematic above. Ensure that the Pi is grounded to the same GND as the circuit. 
	\item On your computer, manually set your Ethernet port's IP address to be any one between 192.168.1.2 and 192.168.1.254, \textbf{excluding} 192.168.1.100, as that is taken by the Raspberry Pi.
	\item Power up the Pi by attaching its USB power to an electrical outlet.
	\item Attach an Ethernet cable between your computer and the Pi.
	\item SSH into the Pi with \inlinecode{ssh pi@192.168.1.100}. \textbf{raspberry} is the default password
	\item Run \inlinecode{sudo i2cdetect -y 1} to make sure that each 23017 is connected. In the below example, (3) 23017s wired as 000, 001, and 010 have been detected.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\textwidth]{i2cdetect.png}
	\end{figure}
	\item Either run a program with \inlinecode{sudo python program.py}, or open a Python terminal with \inlinecode{sudo python}. Both need to be run as a superuser because setting the GPIO pins requires access to memory.
	\item Import WiringPi2 and run the setup method.
	\item For each 23017 in your circuit, choose a unique \textbf{pin\_base} with an integer value above 70. An apt assignment would be 100 for 23017 \#1, 200 for 23017 \#2, 300 for 23017 \#3, an so on. The bases must be selected such that they do not overlap.
	\item From the previous output of \inlinecode{i2cdetect}, note the addresses that showed up.
	\item For each 23017, run \inlinecode{wiringpi.mcp23017Setup(pin\_base, address)}.
	\item Now set each pin controlling a switch as output. The command for that is \inlinecode{wiringpi.pinMode(pin, 1)}. Conversely, writing a pin mode as 0 will set the pin as an input.
	\item You can now set an output pin as HI or LO. The command to do that is \inlinecode{wiringpi.digitalWrite(pin, 1)} or \inlinecode{wiringpi.digitalWrite(pin, 0)}.
	\item When you are done manipulating the circuit, set each output pin back to LO, and then make all pins input to put them back into a safe state. Alternatively, kill power to the circuit, and the pins will all reset.
\end{enumerate}

\section{Future Work}
Raspian, the Debian-based OS that the Pi runs by default, unfortunately cannot run VISA, the interface library used to communicate with the function generator and scope. If it could, we could run the whole program straight off of the Pi. Because we cannot, the workaround is as follows.

Any laptop set up with our code and libraries can talk to the generator and scope. Thus, in order to synchronize generator and scope options, any code utilizing the Pi and the instruments should include snippets from \inlinecode{example-server.py} and \inlinecode{example-client.py}. The laptop would be set as a server, while the Pi would be set as a client, communicating via a serial socket over Ethernet. The code found in \inlinecode{AutoTest.py} shows how the example server code would be integrated into a program, while \inlinecode{AutoTestPi.py} shows how the example client code would be used.

To send commands for the Pi to open a switch, a user running any variant or snippet of \inlinecode{example-server.py} would add an instruction to \inlinecode{insnQueue}. The client will constantly poll the server for a new command, which the server would dispatch if \inlinecode{insnQueue} is not empty. When a command is received and processed at the client, the client will transmit a confirmation message that the server listens to.

A command to set an input switch should be formatted as \inlinecode{\textbf{ICH}:\#\#:ON/OFF}, where \inlinecode{\#\#} is the 2-digit representation of the channel. It would be up to the client to translate \inlinecode{\#\#} to the appropriate pin. Similarly, sending an output switch should be formatted as \inlinecode{\textbf{OCH}:\#\#:ON/OFF}.

To operate the server/client combination, the server program should be started first, followed by the client. As an example, try running \inlinecode{example-server.py} followed by \inlinecode{example-client.py} on the Raspberry Pi. Try it in reverse, and see that it does not work. 

\section{Example Pin Manipulation Program}
\begin{lstlisting}
# An example program for talking to multiple 23017s.

# The Python library, where we give it an easier name to call by
from time import sleep
import wiringpi2 as wiringpi 

# For waiting
from time import sleep

# The lowest available number is 65, but you can choose anything that you'd like. For multiple 23017s, the pin_bases must be at least 16 digits apart so that each pin address is distinct.
example_pin_base = 65 
pin_base1 = 100
pin_base2 = 200
pin_base3 = 300

# The I2C addresses indicated by i2c-detect. 0x20 corresponds to the chip wired as 000.
i2c_addr1 = 0x20
i2c_addr2 = 0x21
i2c_addr3 = 0x22

print("Initializing...")

# Initialize wiringpi
wiringpi.wiringPiSetup()

# Establish communications with and assign pins to the 23017s
wiringpi.mcp23017Setup(pin_base1, i2c_addr1)
wiringpi.mcp23017Setup(pin_base2, i2c_addr2)
wiringpi.mcp23017Setup(pin_base3, i2c_addr3)

# Set pin GPA0 on each 23017 to output.
wiringpi.pinMode(100, 1)
wiringpi.pinMode(200, 1)
wiringpi.pinMode(300, 1)

# Set each pin to LOW output
wiringpi.digitalWrite(100, 0)
wiringpi.digitalWrite(200, 0)
wiringpi.digitalWrite(300, 0)

print("Turning on lights!")

# We use a try/finally block so that if any error occurs, the 23017s are always put back to an inert state.
try:
	while True:
		# Turn pins HIGH
		wiringpi.digitalWrite(100, 1)
		sleep(1)
		wiringpi.digitalWrite(200, 1)
		sleep(1)
		wiringpi.digitalWrite(300, 1)
		sleep(1)

		# Turn pins LOW
		wiringpi.digitalWrite(100, 0)
		sleep(1)
		wiringpi.digitalWrite(200, 0)
		sleep(1)
		wiringpi.digitalWrite(300, 0)
		sleep(1)
finally:
	# On Ctrl-C or any error, return to a safe state.
	print("Quitting!")

	# Set all pins to 0V
	wiringpi.digitalWrite(100, 0)
	wiringpi.digitalWrite(200, 0)
	wiringpi.digitalWrite(300, 0)

	# Set all pins back to input
	wiringpi.pinMode(100, 0)
	wiringpi.pinMode(200, 0)
	wiringpi.pinMode(300, 0)
\end{lstlisting}

\section{Questions}
\label{sec:ex}
Any questions about functionality should be directed to Alan, whose email is attached at the top of this document.

\end{document}
