instrumentdescriptor = 'USB0::0x0699::0x040B::C021222::INSTR'

import visa

# Setup
rm = visa.ResourceManager()
scope = rm.get_instrument(instrumentdescriptor)
scope.write("*IDN?")
print 'Instrument successfully identified itself as' + scope.read()

print 'Capturing screen image...'
scope.write('save:image "E:/pyScript.tif"')

raw_input('Press any key to terminate.')