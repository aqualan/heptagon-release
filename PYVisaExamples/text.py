instrumentdescriptor = ''

import visa

print 'Sending test message'

command = "DISPlay:WINDow:TEXT:DATA "
message = "24-inch PYTHONS"

instrumentdescriptor = "USB0::0x0699::0x040B::C021222::INSTR"

rm = visa.ResourceManager()
AFG = rm.get_instrument(instrumentdescriptor)

AFG.timeout = 5000

AFG.write('*rst')
AFG.write('*cls')

AFG.write(command + message)

raw_input('Press any key to terminate.')
