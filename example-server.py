# Example script for socket server (your laptop)
# Usage: python example-server.py

import socket
import sys
from thread import *
import Queue

HOST = '' # Intentionally blank.
PORT = 8888

insnQueue = Queue.Queue()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'

try:
	s.bind((HOST, PORT))
except socket.error, msg:
	print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
	sys.exit()

print 'Socket bind complete'

s.listen(5)
print 'Socket now listening'

insnQueue.put("Insn1")
insnQueue.put("Insn2")
insnQueue.put("Insn3")
insnQueue.put("Insn4")
insnQueue.put("Insn5")
insnQueue.put("ICH:09:ON")
insnQueue.put("Insn6")
insnQueue.put("Insn7")
insnQueue.put("Insn8")
insnQueue.put("Insn9")
insnQueue.put("Insn10")
insnQueue.put("ICH:09:OFF")


# Handle connections
def clientthread(conn):
	# Infinite loop so that functions do not terminate and the thread does not
	# end
	while True:
		# Receiving from client
		reply = ""
		data = conn.recv(1024)
		if (data == "Hey, got anything for me yet?"):
			if (not insnQueue.empty()):
				print("Dispatching new insn.")
				reply = insnQueue.get()
			else:
				reply = "Sorry man; I got nothing."
		if (data[0:10] == "Confirmed:"):
			print("Client confirmed that he last got:%s" %(data[10:]))
		if not data:
			break

		print("Sending reply")
		conn.send(reply)

	print ("Client terminated connection.")
	conn.close()

# Now keep talking with the client
try:
	while 1:
		# Wait to accept a connection
		conn, addr = s.accept()
		print 'Connected with ' + addr[0] + ':' + str(addr[1])

		# Start new thread
		start_new_thread(clientthread, (conn,))
finally:
	print ("Shutting down server.")
	s.close()
