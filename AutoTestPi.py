'''
This is the companion to the AutoTest GUI interface. It runs on the Pi and controls the switching chips to move the input signal from channel to channel and to match the output pins accordingly.

Acts as a Python socket client.

NOTE: NEEDS TO BE RUN AS SUDO; because accessing and changing the GPIO pins is akin to editing memory, even with the WiringPi library.
'''

import socket
import sys
import struct
import time
import wiringpi2 as wiringpi

pin_base1 = 100
pin_base2 = 200

i2c_addr1 = 0x20
i2c_addr2 = 0x21

# Always run when started manually.
if __name__ == "__main__":
	if (len(sys.argv) < 2):
		print "Usage : python AutoTestPi.py [ip of server]"
		sys.exit()
	host = sys.argv[1]
	port = 8888 # Must match the server port

	# Initialize wiringpi
	wiringpi.wiringPiSetup()

	# Establish communications with and assign pins to the 23017s
	wiringpi.mcp23017Setup(pin_base1, i2c_addr1)
	wiringpi.mcp23017Setup(pin_base2, i2c_addr2)	

	# Set pin GPA0 on each 23017 to output
	wiringpi.pinMode(100, 1)
	wiringpi.pinMode(200, 1)

	# Set each pin to LOW output
	wiringpi.digitalWrite(100, 0)
	wiringpi.digitalWrite(200, 0)

# Create an INET, STREAMing socket
try:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
	print 'Failed to create socket'
	sys.exit()

print 'Socket created'

try:
	remote_ip = socket.gethostbyname( host )
	s.connect ((host, port))
except socket.gaierror:
	print 'Hostname could not be resolved. Exiting.'
	sys.exit()

print 'Socket connected to ' + host + ' on ip ' + remote_ip

def recv_timeout(the_socket, timeout=1):
	print "Ready to receive instructions!"
	# Make socket non blocking
	the_socket.setblocking(0)

	# Total data partwise in an array
	total_data = []
	data = ''

	# Beginning time
	begin = time.time()
	while 1:
		# If you got no data at all, continue waiting.
		if time.time() - begin > timeout * 2:
			#print("Breaking after second timeout")
			#break
			print "Awaiting instructions..."
			time.sleep(1)
			the_socket.send("Hey, got anything for me yet?")

		# Give time for the server to craft a response
		time.sleep(1)
		# Received something
		try:
			data = the_socket.recv(8192)
			if data:
				print "Received instruction!"
				total_data.append(data)
				# Change the beginning time of measurement
				begin = time.time()
				message = ''.join(total_data)
				print message
				if (message[0:3] == "ICH"):
					# Got an input channel command
					channel = message[4:6]
					state   = message[7:]
					print ("I am going to turn channel %s %s" 
						%(channel, state.lower()))
					# NOTE: This is currently hardwired.
					if (state.lower() == "on"):
						wiringpi.digitalWrite(100, 1)
					elif (state.lower() == "off"):
						wiringpi.digitalWrite(100, 0)
				elif (message[0:3] == "OCH"):
					# Got an output channel command
					channel = message[4:6]
					state   = message[7:]
					print ("I am going to turn channel %s %s"
						%(channel, state.lower()))
					# NOTE: This is currently hardwired.
					if (state.lower() == "on"):
						wiringpi.digitalWrite(100, 1)
					elif (state.lower() == "off"):
						wiringpi.digitalWrite(100, 0)

				if (data != "Sorry man; I got nothing."):
					# Acknowledge receipt if not empty
					print("Sending confirmation.")
					the_socket.send("Confirmed: " + data)
				total_data = []
		except:
			pass
try:
	recv_timeout(s)
finally:
	s.close()
