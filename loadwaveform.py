#!/usr/bin/python

"""
For this to function correctly I had to change pyvisa's highlevel.py line
644 to: "count = self.write_raw(message)" (Used to be message.encode(enco))
which stopped a unicode decode error from occuring. It allows us to package the
binary information correctly and hand it off, this has been tested (6/12/2014)
to work with the AFG.

THIS FUNCTION NEEDS TO BE PASSED A HANDLE TO THE AFG OR IT WILL EXPLODE
THIS FUNCTION NEEDS TO BE PASSED A DICTIONARY 'imported' WHICH IS FORMATTED
ON THE ASSUMPTION THAT IT IS THE OUTPUT OF readfromfile.py
sweden is an optional flag for setting burst mode on the function generator
set to literally anything else for continuous mode

This function takes data that it assumes is correctly formated and puts it
on the AFG, or whatever it thinks is the AFG, depends on what you hand it
in the AFG argument slot.
Sweden is the indicator for burst or not burst, yes means burst and is the
default value, "Sweden, yes".

wavetype indicates what kind of waveform (sine, arbitrary, etc.) is being
sent. frequency, offset, and vpp should not be set for arbitrary waveforms.
imported and resolution should be set only for arbitrary waveforms

"""
import visa
from struct import pack
import readfromfile as readff
import interpandtransform as interpol

def loadwaveform(AFG, wavetype, freq=None, offset=None, vpp=None,
                 imported=None, sweden='yes', source=1, resolution=10000
                 , tunits='s', vunits='V', invert=False):
    # === Parse units information ===
    # Seconds
    if tunits == 's':
        tscalar = 1.0
    # milliseconds
    elif tunits == 'ms':
        tscalar = 1.0e-3
    # microseconds
    elif tunits == 'us':
        tscalar = 1.0e-6
    # nanoseconds
    elif tunits == 'ns':
        tscalar = 1.0e-9
    # I refuse to believe there are any other units that exist
    else:
        tscalar = 1.0
    
    # Volts
    if vunits == 'V':
        vscalar = 1.0
    # millivolts
    elif vunits == 'mV':
        vscalar = 1.0e-3
    # Those are all of the voltages in existence
    else:
        vscalar = 1.0


    # === Unpack the imported data ===
    #For arbitrary waveform
    if wavetype == 'ememory':
        refform = imported.get('refform')
        frequency = imported.get('frequency')
        rangeref = imported.get('rangeref')
        midref = imported.get('midref')
        
        # Calls the interpolation function, giving us a set of int(resolution) points
        # returns a list that is resolution long
        waveform = interpol.interpoldata(imported, resolution)
    #For other waveforms
    else:
        frequency = freq
        rangeref = vpp
        midref = offset

    # Here we handle the TEK to AGI conversion, a simple shift downward by 8191
    # (the center moves from 8191 to 0, max/mins change correspondingly)
    if AFG.type == "0x0957::0x4108":
        for i in range(len(waveform)):
            waveform[i] = waveform[i] - 8191

    # ==== Send to AFG3000 ===
    if wavetype == 'ememory':
        # Setup the waveform length = number of points for waveform defined above
        AFG.write_arbresolution(resolution)

        # Create a string of arbitrary text that represents the binary form of all
        # the data points, packaged a string that we will write to the AFG
        header = '#' + str(len(str(resolution * 2))) + str(resolution * 2)
        binwaveform = pack('>'+'h'*len(waveform), *waveform)
        AFG.write_waveform(header + binwaveform)
        # The below is the one point at a time method, saving this just in case
        '''
        pindex = 1
        for point in waveform:
            AFG.write('trace:data:value ememory, ' + str(pindex) + ', ' + str(point))
            pindex+=1
        print('Done!')
        '''
    # Set's the AFG to burst mode or continuous mode. May take this out, as it
    # might become a function elsewhere
    if sweden == 'no':
        AFG.write_burstmodetrig(source)
        AFG.write_burstcycles(source, 1)
        AFG.write_burststate(source, 'on')
        AFG.write_burstsequencetimer(1.0e-4)
    else:
        AFG.write_continuousmode(source)
    
    # Configure the channel to play arbitrary waveform
    AFG.write_wavetype(source, wavetype)

    # Write our parameters to the AFG, the frequency, the offset, and the Vpp, all
    # of which were calculated in the function readfromfile
    AFG.write_frequency(source, frequency*(1.0/tscalar))
    AFG.write_voltageoffs(source, midref*vscalar)
    AFG.write_vpp(source, rangeref * vscalar)
    if invert:
        AFG.write_invert(source, 'inverted')
    else:
        AFG.write_invert(source, 'normal')

if __name__ == '__main__':
    rm = visa.ResourceManager()
    AFG = rm.get_instrument('USB0::0x0699::0x0345::C020930::INSTR')
    imported = readff.importdata('C:\\Users\\Alex\\Documents\\Penn\\Code\\LArPosSig.txt')
    loadwaveform(AFG, 'ememory', imported=imported)
