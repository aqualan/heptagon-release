#!/usr/bin/env python

'''
backend.py

Function:           Holds the backend class, which is responsible for
                    running background tasks for the program, including
                    detecting/conecting devices automatically. Additional
                    background tasks may be added to this program later.

Classes:            backend

Notes:              *As of now, the backend assumes that one device of each
                     type can be plugged in. It may be necessary in the future to
                     implement a way of supporting more than one of each device
                     type.

'''

from visa import ResourceManager
from PySide.QtCore import(QThread, Signal)

from time import sleep

from sillyscope import sillyscope
from generator import generator

class backend(QThread):

        '''
        backend is a class that runs background tasks for the program.
        Right now, this consists of detecting/connecting the oscilloscope
        and function generator automatically. backend works by opening up
        a new thread that runs processes separatly for the main program,
        so that these processes can be run without cauing significant processing
        blockage in the main program.

        Functions: run, check_instruments, connect_oscilloscope, connect_AFG,
                   disconnect_oscilloscope, disconnect_AFG
        '''

        oscilloscopeError = Signal()
	AFGError = Signal()
	testQuitError = Signal()

	def __init__(self, parent):

		#---Initializing parent class.---#
		QThread.__init__(self, parent)  #Initializing the properties inherited from QThread
		self.parent = parent   #Storing the location for parent class in a local variable.
		self.running = True #A boolean that indicates whether or not the thread is running.
				    #Set to false when the thread is closed.

		#---Creating class properties.---#
		self.oscName = ''   #Current name of the oscilloscope device.
		self.oscID = None   #Current ID of the oscilloscope device.
		self.oscUSBList = ['0x0699::0x0401', '0x0699::0x040B']    #List of compatible USB oscilloscope devices.

		self.oscConn = False    #Current oscilloscope connection state.
		self.oscilloscopeConnected = False #Long-term oscilloscope connection state.

		self.AFGName = ''   #Current name of the function generator.
		self.AFGID = None   #Current ID of the function generator.
		self.AFGUSBList = ['0x0699::0x0345', '0x0957::0x4108']    #List of compatible USB function generator devices.

		self.AFGConn = False    #Current function generator connection state.
		self.AFGConnected = False #Long-term function generator connection state.

		self.rm = ResourceManager()    #Visa resource manager that is used for creating
						    #visa instruments.

	def run(self):

		'''
		Runs the main event loop of backend. In the event loop,
		the program checks the instruments that are connected using the
		check_instruments function. The following process is then run for
		each instrument (currently an oscilloscope and a function generator).
		If a device is connected and it was previously disconnected,
		the fucntion connect_[device] is run and error check is set to 0.
		If the device has already been connected and is currently
		connected, the error check is set to 0. If the device is
		currently disconnected and was previously disconnected, a disconnect
		message is presented to the user. If the device is disconnected and
		it was previously connected, the error check is increased by 1. Once
		the error check has reached a count of 5, the function diconnect_[device]
		is executed. If both the oscilloscope and function generator are connected,
		functionality for the linearity test tab is enabled. Finally, the
		connect/disconnect state for each device is presented to the user, and the
		event loop briefly pauses to prevent the thread from using up too much
		processing.

		Arguments: None
		'''

		oscFirst = True #A boolean that stores if the oscilloscope has just been
				#initialized. Set to False after the oscilloscope has been
				#connected.
		oscCheck = 0 #Error check for oscilloscope, see note below.
		AFGFirst = True #A boolean that stores if the function generator has just
				#been initialized.
		AFGCheck = 0 #Error check for function generator, see note below.

		#Note regarding error checks: it was discovered that in some instances, the
		#computer looses contact with a USB device for an instance. In order to prevent
		#the program from running the disconnect_[device] function while the device is
		#still connected, an error check has been established that counts how many times
		#the program has failed to contact the device. If the error check reaches 5, the
		#program runs the disconnect_[device] function.

		#Main event loop.
		while self.running:

		    self.check_instruments()
                
		    #Reacting to what the connection state of the oscilloscope is.
		    if self.oscConn and oscFirst:
			self.connect_oscilloscope()
			oscFirst = False
			oscCheck = 0
		    elif self.oscConn and not oscFirst: oscCheck = 0
		    elif not self.oscConn and oscFirst: self.oscName = 'No oscilloscope connected.'
		    else:
			oscCheck += 1
			if oscCheck > 4:
			    self.disconnect_oscilloscope()
			    oscFirst = True

		    #Reacting to what the connection state of the function generator is.
		    if self.AFGConn and AFGFirst:
			self.connect_AFG()
			AFGFirst = False
			AFGCheck = 0
		    elif self.AFGConn and not AFGFirst: AFGCheck = 0
		    elif not self.AFGConn and AFGFirst: self.AFGName = 'No AFG connected.'
		    else:
			AFGCheck += 1
			if AFGCheck > 4:
			    self.disconnect_AFG()
			    AFGFirst = True

		    #Presenting the name/connection state of the devices
		    if self.oscName == 'No oscilloscope connected.': self.parent.oscilloscopeState.setText(self.oscName)
                    else: self.parent.oscilloscopeState.setText('Oscilloscope connected: ' + self.oscName[:18] + '...')
                    
		    if self.AFGName == 'No AFG connected.': self.parent.AFGState.setText(self.AFGName)
		    else: self.parent.AFGState.setText('AFG Connected: ' + self.AFGName[:18] + '...')
		    sleep(0.1) #Taking a break in order to prevent the backend from using too much processing.

                    #Enabling/disabled functionality in the test tabs depending on the connection state of both instruments.
		    if self.oscilloscopeConnected and self.AFGConnected:
                            self.parent.linearityTab.controlButton.setEnabled(1)
                            self.parent.autoTab.singleTestButton.setEnabled(1)
                            self.parent.autoTab.multiTestButton.setEnabled(1)
                            self.parent.autoTab.fullTestButton.setEnabled(1)
                    else:
                            self.parent.linearityTab.controlButton.setDisabled(1)
                            self.parent.autoTab.singleTestButton.setDisabled(1)
                            self.parent.autoTab.multiTestButton.setDisabled(1)
                            self.parent.autoTab.fullTestButton.setDisabled(1)
                            try:
                                #Closes the multi test setup window if it has been opened
                                self.parent.autoTab.multiWindow.close()
                                self.testQuitError.emit()
                            except: pass

		self.quit() #Quitting the thread once the main event loop has finished.

	def check_instruments(self):

		'''
		This function checks to see if any devices are connected to the computer and saves the device name if they are recognized
		an except statement. The function uses pyvisa to list all devices that are
		available to the program. If no devices are present, pyvisa displays an error,
		and check_instruments runs its exception, which sets the connection state for all
		devices to false. If pyvisa does detect devices, the IDs for each instrument is
		stored in a list. This list is run through for each device type and checked
		for its connection type (right now only USB is supported) and checked against the
		list of compatible devices (self.oscUSBList and self.AFGUSBList) for that connection
		type. If a compatible device is detected in the list, the connection state for that
		device type is set equal to true and the devices name is stored. If not, the conncection
		state is set equal to false.

		Arguments: None
		'''

		try: instruments = self.rm.list_resources()
		except Exception, e:
                        self.oscConn = False
                        self.AFGConn = False
                        return
                        
                for inst in instruments:

                        #The following statements check try to determine what kind of instrument has been
                        #connected, if that instrument type has not already been connected.

                        instid = inst.encode('ascii', 'ignore')
                        if 'USB0' in instid:
                                if instid[6:20] in self.oscUSBList:
                                        self.oscID = instid
                                        self.oscConn = True
                                        break
                                else: self.oscConn = False
                        else: self.oscConn = False

                for inst in instruments:

                        instid = inst.encode('ascii', 'ignore')
                        if 'USB0' in instid:
                                if instid[6:20] in self.AFGUSBList:
                                        self.AFGID = instid
                                        self.AFGConn = True
                                        break
                                else: self.AFGConn = False
                        else: self.AFGConn = False
                                

	def connect_oscilloscope(self):

		'''
		This function "connects" the oscilloscope to the computer. A virtual oscilloscope
		instrument is created in the mainApplication parent, and functionality for the
		oscilloscope is enabled in the oscilloscope tab.

		Arguments: None
		'''

                self.oscilloscopeConnected = True

                #Attempting to connect the scope, gives up if the attempt fails several times
                n = 0
                while n < 5:
                        try:
                                self.parent.scope = sillyscope(self.oscID, parent=self.parent) #An instance of a virtual oscilloscope.
                                break
                        except:
                                n += 1
                                if n == 5:
                                        self.oscilloscopeError.emit()
                                        self.oscilloscopeConnected = False
                                        return

		self.oscName = self.parent.scope.communicator.ask('*IDN?').encode('ascii', 'ignore') #Getting the device's name.

		#Turning on functionality for each channel in the oscilloscope.
		for i in range(self.parent.scope.numberChannels):
                        
		    eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.setEnabled(1)')
		    
		    if self.parent.scope.impedanceStore[i] == 'MEG': eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio1M.toggle()')
		    else: eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio50.toggle()')
		    
		    if self.parent.scope.stateStore[i] == 'ON':
			eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.OnOff.toggle()')
			eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio1M.setEnabled(1)')
			eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio50.setEnabled(1)')
			eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.invert.setEnabled(1)')
		    else:
			eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio1M.setDisabled(1)')
			eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio50.setDisabled(1)')
			eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.invert.setDisabled(1)')
			
                    if self.parent.scope.referenceStore[i] == 'ON': eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.referenceOnOff.toggle()')


		self.parent.oscilloscopeTab.chanBoxMath.setEnabled(1)
		
                if self.parent.scope.mathState == 'ON':
                        self.parent.oscilloscopeTab.mathOnOff.toggle()
                        self.parent.oscilloscopeTab.mathexpress.setEnabled(1)
                else: self.parent.oscilloscopeTab.mathexpress.setDisabled(1)
		
		self.parent.oscilloscopeTab.mathexpress.setText(unicode(self.parent.scope.mathExpression))

		#Turning on funtionality for the data management button in the oscilloscope tab.
		self.parent.oscilloscopeTab.setButton.setEnabled(1)
		self.parent.oscilloscopeTab.saveStateButton.setEnabled(1)
		self.parent.oscilloscopeTab.loadStateButton.setEnabled(1)
		self.parent.oscilloscopeTab.grabWaveformButton.setEnabled(1)
		self.parent.oscilloscopeTab.sendRefButton.setEnabled(1)

	def connect_AFG(self):

		'''
		This function "connects" the function generator to the computer. An instance of the virtual
		function generator class is created in the mainApplication parent, and functionality for
		the function generator is turned on in the function generator tab.

		Arguments: None
		'''

                self.AFGConnected = True

                #Attempting to connect to the function generator, gives up if the attempt fails several times.
                n = 0
                while n < 5:
                        try:
                                self.parent.AFG = generator(self.AFGID, self.parent) #An instance of a virtual function generator.
                                break
                        except:
                                n += 1
                                if n == 5:
                                        self.AFGError.emit()
                                        self.AFGConnected = False
                                        return
                                
		self. AFGName = self.parent.AFG.communicator.ask('*IDN?').encode('ascii', 'ignore') #Getting the device's name.

                self.parent.AFGTab.loadConfigButton.setEnabled(1)
                self.parent.AFGTab.saveConfigButton.setEnabled(1)
		self.parent.AFGTab.sendButton.setEnabled(1)
                        

        def disconnect_oscilloscope(self):

		'''
		This function 'disconnects' the oscilloscope from the computer. Any threads that are running are stopped,
		and functionality in the test tabs are disabled. Functionality is turned off for the buttons and controlls
		in the oscilloscope tab, the connection state is set to False, and the location for the virtual oscilloscope
		is set to None. Note that this function is run approximately half a second after the oscilloscope has actually
		been disconnected from the computer.

		Arguments: None
		'''

                #Quitting a linearity test if a thread is still running.
                if self.parent.linearityTab.threadState == 'on':
                        self.parent.linearityTab.quit_test()
                        self.testQuitError.emit()

                #Quitting an auto test if a thread is still running.
                if self.parent.autoTab.threadState == 'on':
                        self.parent.autoTab.quit_test()
                        self.testQuitError.emit()
                        
                self.parent.linearityTab.controlButton.setDisabled(1)

		for i in range(self.parent.scope.numberChannels): eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.setDisabled(1)')

                self.parent.oscilloscopeTab.chanBoxMath.setDisabled(1)
		self.parent.oscilloscopeTab.setButton.setDisabled(1)
		self.parent.oscilloscopeTab.loadStateButton.setDisabled(1)
		self.parent.oscilloscopeTab.saveStateButton.setDisabled(1)
		self.parent.oscilloscopeTab.grabWaveformButton.setDisabled(1)
		self.parent.oscilloscopeTab.sendRefButton.setDisabled(1)
		
		self.oscConn = False
		self.parent.scope = None

		self.oscilloscopeConnected = False

	def disconnect_AFG(self):

		'''
		This function 'disconnects' the function generator from the computer. Any threads that are running are stopped,
		and functionality in the test tabs is disabled. Functionality is turned off for	the send button in each waveform
		tab, the connection state is set to False, and the loaction for the virtual function generator is set to None.
		Note that this function is run approximately half a second after the oscilloscope has actually been disconnected
		from the computer.

		Arguments: None
		'''

		self.parent.AFGTab.saveConfigButton.setDisabled(1)
		self.parent.AFGTab.loadConfigButton.setDisabled(1)
		self.parent.AFGTab.sendButton.setDisabled(1)

		self.AFGConn = False
		self.parent.AFG = None

                #Quitting a linearity test if a thread is still running
		if self.parent.linearityTab.threadState == 'on':
                        self.parent.linearityTab.quit_test()
                        self.testQuitError.emit()

                #Quitting an auto test if a thread is still running.
                if self.parent.autoTab.threadState == 'on':
                        self.parent.autoTab.quit_test()
                        self.testQuitError.emit()
                self.parent.linearityTab.controlButton.setDisabled(1)

                self.AFGConnected = False

