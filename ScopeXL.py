'''
Scope Extra-Large - for extra-large scopes. One size does not fit all.

Actual function: 	Creates an .xlsx file and populates it with the waveform
					data that the user chooses in the GUI

Usage: 	After importing, call only ScopeXL.export(). Arguments as follows:

Arguments: 	filename 	: String 		: 	workbook name specified from the GUI
			title 		: String 		: 	title for the top of the sheet
			data 		: SillyScope	: 	object passed in by the GUI with
											waveform data

NOTE: 	Requires you install the xlsxwriter package - see instructions at 
		https://xlsxwriter.readthedocs.org/getting_started.html
'''

# For the 4034 scope in Mitch's Lab
# instrumentdescriptor = 'USB0::0x0699::0x040B::C021222::INSTR'

# For the 4054 scope in Mitch's Lab
instrumentdescriptor = 'USB0::0x0699::0x0401::C020278::INSTR'

import xlsxwriter
import visa

# Physically write to the .xlsx spreadsheet
def write(i, worksheet, time, amplitude):
	for row in range (3, len(amplitude) + 3):
		# Times (x)
		worksheet.write(row, 0, time[row - 3])
		# Amplitudes (y)
		worksheet.write(row, 1, amplitude[row - 3])

# Main method to call from GUI
def export(filename, title, amplitudes, times, booleans):
	
	# Booleans to figure out whether or not to process a waveform
	for i in range(len(booleans)):
                if not booleans[i]:
                        amplitudes[i] = None
                        times[i] = None

	# Open a new workbook
	workbook = xlsxwriter.Workbook(filename)
	worksheet = workbook.add_worksheet()

	# Put in headers
	worksheet.write("A1", title)

	for column in range (0, 8, 2):
		worksheet.write(1, column, "Channel %d" %(column / 2 + 1))
		worksheet.write(2, column, "Time")
		worksheet.write(2, column+1, "Amplitude")

	# Check and see if a waveform has been asked for
	timecols = ["A4", "C4", "E4", "G4", "I4"]
	ampcols = ["B4", "D4", "F4", "H4", "J4"]

	for i in range(len(timecols)):
		if amplitudes[i] == None:
			continue
		else:
			worksheet.write_column(timecols[i], times[i])
			worksheet.write_column(ampcols[i], amplitudes[i])


	# Finish
	workbook.close()
