#! usr/bin/python
'''
This file is a thread for the linearity, calibration and device tests.

Linearity test is a test of the device's linearity, it outputs data in a .xlsx file
with a defined format, the specifications of this file can be seen by running a simple
linearity test and outputting the results. This excel file is then used as input to step
two, which is calibration test.

Calbiration test is not really a test, it calibrates the AFG to output a value that lines 
up with what we expect, based on the results of linearitytest. As such it takes the .xlsx
file from linearitytest as input, so it knows what results it must calibrate based off of.

Device test is a linearitytest but meant for the actual device you wish to get data for, whereas
linearitytest and calibrationTest are meant for a passive network of resistors/capacitors. Device 
test's output is formatted in the same way as linearitytest and requires a .xlsx file as output 
by calibrationTest for input
'''
import visa
import ZoomOnPeak as zoo
from numpy import array, linspace
from scipy.stats import linregress
import xlsxwriter
import xlrd
import re


from PySide.QtCore import Signal
from kthread import KThread, KException

# Make a class for the type of linearity tests, so they can be initialzed as a separate thread
# This allows us to emit signals, which is output text that doesn't require a terminal (.exe compatible)
class linearityThread(KThread):

    # Two types of signals, results signal indicates that it is finished when it is emitted
    # message signal carries strings to the text display in the GUI
    resultsSignal = Signal()
    messageSignal = Signal(str)

    # Initialize the thread, assign the inputs from the GUI to the correct functions/names
    def __init__(self, whichtest, dictionary, parent):

        # KThread is out own thread customization that allows us to kill threads effectively at anypoint
        # These couple lines initialize the thread
        KThread.__init__(self)
        self.parent = parent
        self.type = whichtest

        # These two arguments are common across all the test types, they are the function generator
        # source and oscilloscope channel to write to/read from
        self.source = dictionary['source']
        self.channel = dictionary['channel']
        self.samples = dictionary['samples']
        self.mathexpression = dictionary['mathexpression']
        # The type of test we are running, basically the test we are calling determines what
        # input we need to pass it, since they don't all have the same input parameters
        if self.type == 'Linearity':
            self.startv = dictionary['startv']
            self.endv = dictionary['endv']
            self.numpoints = dictionary['numpoints']
            self.output = dictionary['output']
            self.advanced = dictionary['advanced']
        elif self.type == 'Calibration':
            self.tolerance = dictionary['tolerance']
            self.filename = dictionary['filename']
        # An else is a devicetest, and it only requires the file
        else: self.filename = dictionary['filename']

    def run(self):
        #Running the thread, which means running the test.
        try:
            # Check to see which function we call based on the text input, we have three options
            if self.type == 'Linearity':
                self.linresults = self.linearitytest(self.parent.parent.AFG, self.parent.parent.scope,
                                                startv=self.startv, endv=self.endv, numpoints=self.numpoints,
                                                outputflag=self.output, advancedin=self.advanced, source=self.source,
                                                channel=self.channel, samples=self.samples, mathexpression=self.mathexpression)
            elif self.type == 'Calibration':
                self.linresults = self.calibrationTest(self.filename, self.tolerance, self.samples, self.source,
                                                  self.channel, self.parent.parent.scope, self.parent.parent.AFG, mathexpression=self.mathexpression)
            else:
                self.linresults = self.deviceTest(self.filename, self.source, self.channel,
                                             self.parent.parent.scope, self.parent.parent.AFG, self.samples, mathexpression=self.mathexpression)
                
            # Pass results out of the thread
            self.parent.linresults = self.linresults

            self.parent.threadState = 'done'

            #Signal to the outside that we are finished.
            self.resultsSignal.emit()
        
            return
        # Handles errors to make sure when we move to a .exe it doesn't crash in weird ways
        except (KException): return

    def calcnewoffs(self, vpp, voffs, vppn):
        vmax = voffs + vpp/2.0
        x = vppn/vpp
        vmaxn = x*vmax
        voffsn = vmaxn - vppn/2
        return voffsn, vppn

    '''
    fetchval takes 9 arguments as follows, it makes one or two calls to zoomonpeak, and returns
    the value you seek (the maximum or minimum, or difference, or gain or advanced)
    self is required, just leave it as is
    scope is a handle to the oscilloscope
    afg is a handle to the function generator
    outputflag is a string detailing what the output will be, it is "max", "min", "max-min"
        "gain" or "advanced", anything else is invalid and will return an error
    currentv is the current voltage the next level up is handling, we pass it in in case we
        need it for gain (a ration of input to output, currentv is input)
    channel is the channel on the oscilloscope, it is an integer
    source is the source on the afg, it is an integer
    calibrationExport is a flag, and should only be set 1 when this function is called
        inside of calibrationTest, it causes this function to return a list that includes
        standard deviation values
    advancedin is the string to be evalutated for advanced input
    '''
    def fetchval(self, scope, afg, outputflag, currentv, channel=1, source=1,
                advancedin="asdf", boogaloo=0, scale=0.2):

        outputlist = []

        # Various possible inputs, we call zoomonpeak depending on what we are looking for
        # and return values based on the flag, max-min returns the max minus the min
        if outputflag == "max-min":
            if boogaloo == 0:
                maxval = zoo.zoomonpeak(scope, afg, feature="max", channel=channel, source=source)
            else:
                maxval = zoo.electricboogaloo(scope, afg, feature="max", channel=channel, source=source, vscale=scale)
            # Fetch the standard deviation by asking it what it is, we use 1 because at the end of the zoomonpeak
            # the relevant measurement is in measurement slot 1
            tempstd1 = scope.ask_stddev(1)
            # Error checking, if we get a string that means it didn't find the value correctly, to prevent this
            # from breaking other things later, we exit early and throw a message
            if type(maxval) is str:
                return maxval
            if boogaloo == 0:
                minval = zoo.zoomonpeak(scope, afg, feature="min", channel=channel, source=source)
            else:
                minval = zoo.electricboogaloo(scope, afg, feature="min", channel=channel, source=source, vscale=scale)
            # Fetch the other standard deviation, same as just before this
            tempstd2 = scope.ask_stddev(1)
            # Error checking
            if type(minval) is str:
                return minval
            # Return values, it's a list if calibration export is 1, otherwise we return just the value
            outputlist.append(maxval-minval)
            outputlist.append(tempstd1)
            outputlist.append(tempstd2)
            return outputlist
        # max returns the max value, min returns the min value, it is important to remember that man/min will
        # not necessarily correspond to the amplitude value, because of offset. (Amplitude = 1.157, max = 1.0)
        elif outputflag == "max" or outputflag == "min":
            if boogaloo == 0:
                peakval = zoo.zoomonpeak(scope, afg, feature=outputflag, channel=channel, source=source)
            else:
                peakval = zoo.electricboogaloo(scope, afg, feature=outputflag, channel=channel, source=source, vscale=scale)
            # Fetch the other standard deviation, same as before (line ~50)
            tempstd1 = scope.ask_stddev(1)
            # Error checking
            if type(peakval) is str:
                return peakval
            # Return values, it's a list if calibration export is 1, otherwise we return just the value
            outputlist.append(peakval)
            outputlist.append(tempstd1)
            return outputlist
        # gain returns the vpp output/vpp input
        elif outputflag == "gain":
            if boogaloo == 0:
                maxval = zoo.zoomonpeak(scope, afg, feature="max", channel=channel, source=source)
            else:
                maxval = zoo.electricboogaloo(scope, afg, feature="max", channel=channel, source=source, vscale=scale)

            tempstd1 = scope.ask_stddev(1)
            # Error checking
            if type(maxval) is str:
                return maxval

            if boogaloo == 0:
                minval = zoo.zoomonpeak(scope, afg, feature="min", channel=channel, source=source)
            else:
                minval = zoo.electricboogaloo(scope, afg, feature="min", channel=channel, source=source, vscale=scale)

            tempstd2 = scope.ask_stddev(1)
            # Error checking
            if type(minval) is str:
                return minval
            outputlist.append((maxval-minval)/currentv)
            outputlist.append(tempstd1)
            outputlist.append(tempstd2)
            return outputlist
        # advanced lets the user determine the value, it will evaluate the string
        # passed to the function, I've been told checking will be done outside this
        # function, so right now it just blindly evaluates what is currently an
        # arbitrary string, if it isn't valid it will crash the program so I'll make
        # sure that checking gets done (6/27/2014)
        # it allows the user to specify, through valid python syntax/operators what
        # the user wants as output from the linearity test, for example, if they want
        # the maxvalue squared minus the absolute value of the minimum value the string
        # will be "(maxval**2)-abs(minval)" which is valid python syntax, and thus will
        # evaluate without issue, the user can only use maxval and minval as variables
        # for now (6/27/2014)
        elif outputflag == "advanced":
            if boogaloo == 0:
                maxval = zoo.zoomonpeak(scope, afg, feature="max", channel=channel, source=source)
            else:
                maxval = zoo.electricboogaloo(scope, afg, feature="max", channel=channel, source=source, vscale=scale)

            tempstd1 = scope.ask_stddev(1)
            # Error checking
            if type(maxval) is str:
                return maxval

            if boogaloo == 0:
                minval = zoo.zoomonpeak(scope, afg, feature="min", channel=channel, source=source)
            else:
                minval = zoo.electricboogaloo(scope, afg, feature="min", channel=channel, source=source, vscale=scale)

            tempstd2 = scope.ask_stddev(1)
            # Error checking
            if type(minval) is str:
                return minval
            outputlist.append(eval(advancedin))
            outputlist.append(tempstd1)
            outputlist.append(tempstd2)
            return outputlist
        # flag was invalid, so we don't know what to do, shouldn't really be possible since
        # only the program calls this function, but to prevent crashing, we have it
        else:
            return "Invalid Output Flag"


    '''
    Inputs:
    self is required for threading to work properly
    afg is a handle to the function generator
    scope is a handle to the oscilloscope
    startv is a double, it's the first voltage that will be tested
    endv is a double, it's the last voltage that will be tested
    numpoints is an integer, it is the number of points you want to test, or how many values
        will be returned
    outputflag is a string of "max", "min", "max-min", "gain" or "advanced" and it marks the
        type of output you will get from linearitytest
    advanced is a string of arbirtrary code, assumed to be correctly formated and to use the
        correct variable names, it allows the user to define a calculation
    channel is the oscilloscope channel
    source is the function generator channel
    '''
    def linearitytest(self, afg, scope, startv = 0.0, endv = 1.0, samples = 1
        , numpoints = 20, outputflag = 'max', advancedin="asdf", channel=1, source=1, mathexpression="asdf"):

        # Create a list to which we will append results
        outputvalues = []
        oscscales = []
        
        if mathexpression != None:
            return self.mathlinearity(afg, scope, startv, endv, samples, numpoints, outputflag, advancedin, channel, source, mathexpression)

        # Get parameters for what we're going to do
        # Vpp of the current signal
        sigrange = afg.ask_vpp(source)
        # Voffs of the current signal
        sigoffs = afg.ask_voltageoffs(source)

        # Create a list of voltage values that we are going to test with
        # np.linspace creates an array of evenly spaced values starting
        # at the first argument, and ending at the second argument
        # the list will be as long as the third argument, .tolist() casts
        # it as a list datatype instead of a numpy array
        vlist = linspace(startv, endv, numpoints).tolist()

        # This comes into play since the AFG doesn't recongize 0 volt amplitude
        if startv < 0.05:
            vlist[0] = 0.05

        # Write to the GUI's text display, so the user has some idea of what's happening
        # otherwise it may appear that the program has frozen when it is actually just processing...
        self.messageSignal.emit("Testing for the values : " + str(vlist))

        # Some settings to stop ridiculous search results from clogging the tubes
        afg.write_burstmodetrig(source)
        afg.write_burstcycles(source, 1)
        afg.write_burststate(source, 'on')
        afg.write_burstsequencetimer('1.0e-4')
        scope.write_horizontalrecord(10000)
        # Make sure the strings we get back are nothing but numbers, no unnecessary text
        scope.write_headoff()

        if samples == 1:
            scope.write_acquiremode('sample')
        else:
            scope.write_acquiremode('average')
            scope.write_numavgsamples(samples)
            # lists that will store the standard deviation for the average. We have two since
            # it is possible to do max-min, and the max/min values will have different standard
            # deviations
        stddev1 = []
        stddev2 = []

        # Now we obtain our datapoints, 
        for currentv in vlist:
            # Call a function to calculate the new offset value, the previous method
            # lost accuracy for a greater number of points it tested, this one 
            # always (to my testing limits) gets back to the same voltage/offset combo you
            # started with
            sigoffs, sigrange = self.calcnewoffs(sigrange, sigoffs, currentv)
            # Send these values to the function generator
            afg.write_vpp(source, str(sigrange))
            afg.write_voltageoffs(source, str(sigoffs))
            # Call the fetchval function above, which will return the output value from zoomonpeak.
            self.messageSignal.emit("Currently fetching output for : " + str(currentv))
            outval = self.fetchval(scope, afg, outputflag, currentv, channel=channel, source=source, advancedin=advancedin)
            oscilloscale = scope.ask_scale(channel)
            self.messageSignal.emit("I have fetched the value : " + str(outval[0]) + "\n")
            # Check to make sure we didn't get an error, if it was a string, we did, so we return that immediatly
            # if not, then it worked fine, so we append the value to our list and carry on
            if type(outval) is str:
                return outval
            else:
                outputvalues.append(outval[0])
                if samples > 1:
                    stddev1.append(outval[1])
                    if len(outval) == 3: 
                        stddev2.append(outval[2])
                    else:
                        stddev2.append(-1)
                else:
                    stddev1.append(-1)
                    stddev2.append(-1)
                oscscales.append(oscilloscale)

        # Put the scope back in a basic state
        scope.write_zoommode('off')
        scope.write_offset(channel, '0.0')
        zoo.onscreenbyscale(scope, channel=channel)

        #Test is done. I spit that
        self.messageSignal.emit("Test finished, calculating regression values\n")

        # outputvalues, inputvalues, package them for a linear regression test
        x = array(vlist)
        y = array(outputvalues)
        # Use scipy linear regression test to calculate how straight the line was
        # we will return all of these values
        slope, intercept, r_value, p_value, std_err = linregress(x,y)

        # Two lists for two different types of error calculation
        percentdiff = []
        fullscale = []
        absout = []
        for value in outputvalues:
            absout.append(abs(value))
        for xval, yval in zip(vlist, outputvalues): 
            # y=mx+b
            regressval = xval*slope + intercept
            # difference by subtraction
            deviation = yval - regressval
            # average of the two values
            average = (yval + regressval)/2.0
            # percent difference: difference/average times 100
            percentdiff.append((deviation / average)*100.0)
            fullscale.append((deviation/max(absout))*100.0)
        # Package everything for returning it as output

        if advancedin == "":
            advancedin = 'default'

        std_deviation = [stddev1, stddev2]

        outputnames = ['slope', 'intercept', 'r_value', 'p_value', 'std_err', 'inputvalues'
        , 'outputvalues', 'outputflag', 'percentdiff', 'advancedin', 'fullscale', 'scalelist'
        , 'samples', 'std_deviation']
        outputvalues = [slope, intercept, r_value, p_value, std_err, vlist
        , outputvalues, outputflag, percentdiff, advancedin, fullscale, oscscales
        , samples, std_deviation]
        # *clap* and ya done
        return dict(zip(outputnames, outputvalues))

    '''
    Calibration test takes 8 arguments
    self is required for threading
    filename is an excel file (.xlsx only) assumed to be of the exportLinearity format
    percenttolerable is a double or integer that is the maximum percent difference that is
        acceptable for this test, the function will make changes to any inputvalue that 
        corresponds to a percent difference greater than this value
    samples is an integer that should be a power of two, since it currently just uses the 
        scopes average, and fetches that standard devition
    scope, afg, source and channel are the ususal
    '''
    def calibrationTest(self, filename, percenttolerable, samples, source, channel, scope, afg, mathexpression="asdf"):
        # Read from excel file of previous linearity test
        # Uses the (now) standardized excel format, which is to say all of these
        # linearity test functions will export through the same function and as such
        # any reading in I do will assume that format outputted by exportLinearity
        workbook = xlrd.open_workbook(filename)
        worksheet = workbook.sheet_by_index(0)

        if mathexpression != None:
            return self.mathcalibration(filename, afg, scope, percenttolerable, samples, channel, source, mathexpression)

        # Lists to store our i/o
        inputvalues = []
        calibratedin = []
        outputvalues = []
        regress = []
        percentdiff = []
        fullscale = []
        scalelist = []
        absout = []

        # Based on the format we start at this row/col to avoid reading in titles
        row = 1
        col = 2

        if mathexpression != None:
            scope.write_mathexpression(mathexpression)

        for i in range(1, len(worksheet.col(2))):
            # All lists should be the same length, but it reads in based on the longest column.
            # Thus, in the case where the list of regression values is longer than all the others
            # It will read in a number of blank cells equal to the difference in length. To prevent
            # that, if we read in a blank, we stop there.
            if worksheet.cell_value(row, col) == '':
                # BREAK THE WHOLE CODE, oh wait just the loop
                break
            # Load in all the relevant parameters we want to operate on
            # Calibratedin is a copy of input values that we can change, we do this so we always
            # have the original value as a reference/for final output
            inputvalues.append(worksheet.cell_value(row, col))
            calibratedin.append(worksheet.cell_value(row, col))
            outputvalues.append(worksheet.cell_value(row, col+1))
            absout.append(abs(worksheet.cell_value(row, col+1)))
            regress.append(worksheet.cell_value(row, col+2))
            percentdiff.append(worksheet.cell_value(row, col+3))
            fullscale.append(worksheet.cell_value(row, col+4))
            scalelist.append(worksheet.cell_value(row, col+5))
            row+=1

        # Read in more values, including information on what the output flag was
        # and linear regression parameters
        outputflag = worksheet.cell_value(0,0)
        slope = worksheet.cell_value(1,1)
        intercept = worksheet.cell_value(2,1)
        r_value = worksheet.cell_value(3,1)
        std_err = worksheet.cell_value(4,1)
        advancedin = worksheet.cell_value(7,1)

        # Set up the scope and AFG, just like in linearity test, just in case
        afg.write_burstmodetrig(source)
        afg.write_burstcycles(source, 1)
        afg.write_burststate(source, 'on')
        afg.write_burstsequencetimer('1.0e-4')
        scope.write_horizontalrecord(10000)

        # Turn off headers so we can easily read numbers from the scope
        scope.write_headoff()
        # Set how many samples per input value we will read out, must be a power of 2
        # (for now), and complete the necessary set up
        if samples == 1:
            scope.write_acquiremode('sample')
        else:
            scope.write_acquiremode('average')
            scope.write_numavgsamples(samples)
            # lists that will store the standard deviation for the average. We have two since
            # it is possible to do max-min, and the max/min values will have different standard
            # deviations
            stddev1 = []
            stddev2 = []

        for i in range(len(calibratedin)):
            # Look at each value in the deviation list, if it's greater than what the
            # user deems is tolerable, we will do something about it (see while loop)
            # otherwise we skip it and move on.
            # Skipping it means it will get std deviations of -1.0, which just means 
            # I don't have a standard deviation to return for that particular point.
            tmpstd1 = -1.0
            tmpstd2 = -1.0
            # The default start value of 50 mv, we shrink this as we hone in on the point.
            # You can make it smaller or larger as you wish, it is in units of VOLTS
            incrementval = 0.05

            # Currently we ignore anything so small as to be dominated by noise.  This is a WIP though
            # We use continue to skip the iteration before getting to the while loop, we append two -1
            # values to indicate we have no relevant standard deviations for these values
            if calibratedin[i] < 0.25:
                errortype = fullscale
                errorflag = "fullscale"
                # Increment up or down, just a 1 or -1 factor to multiply incrementval by
                # We use our known deviation values to give us a start direction
                incrementdir = fullscale[i]/abs(fullscale[i])
            else:
                errortype = percentdiff
                errorflag = "percentdiff"
                # Increment up or down, just a 1 or -1 factor to multiply incrementval by
                # We use our known deviation values to give us a start direction
                incrementdir = percentdiff[i]/abs(percentdiff[i])
            # If deviation is greater than what the user deems is acceptable, we 
            # make corrections until it is, we calculate our objective with regression parameters and
            # the original input value (hence inputvalues and NOT calibratedin, lest our goal constantly
            # change as well)
            objectivevalue = (inputvalues[i]*slope) + intercept
            # A large difference so the first comparison passes just fine without reversing direction
            # if it doesn't pass you have other problems, seriously, 10000% difference? that's ridic.
            previousdiff = 10000.0

            while abs(errortype[i]) > percenttolerable:
                # Emit messages so the user can see it in real time!
                self.messageSignal.emit("Changing from : " + str(calibratedin[i]))
                # Calculate our new input to try! using incrementdirection and value
                calibratedin[i] = calibratedin[i]+(incrementdir*incrementval)

                # If we are trying to write less than 50 mV to the AFG we can't, so we change
                # direction and also set to the minimum we can.
                if calibratedin[i] < 0.05:
                    calibratedin[i] = 0.05
                    incrementdir = 1.0

                # Set parameters for the AFG once we calculate them
                sigrange = afg.ask_vpp(source)
                sigoffs = afg.ask_voltageoffs(source)
                sigoffs, sigrange = self.calcnewoffs(sigrange, sigoffs, calibratedin[i])
                # Send these values to the function generator
                afg.write_vpp(source, str(sigrange))
                afg.write_voltageoffs(source, str(sigoffs))

                '''
                Things I need to know
                1. What to do with values less than 100mv for calibration, maybe
                '''
                # Get our new output value and its standard deviation(s), if it's not a string
                # then we calculate a new deviation and test it again (run the while loop again, etc...)
                self.messageSignal.emit("... to : " + str(calibratedin[i]))
                self.messageSignal.emit("Offset is : " + str(sigoffs))
                # The new voltage value, I get that.
                newout = self.fetchval(scope, afg, outputflag, calibratedin[i], channel=channel, source=source
                    , advancedin=advancedin)
                scalelist[i] = scope.ask_scale(channel)
                # New output value, I write that.
                self.messageSignal.emit("I have fetched the value : " + str(newout[0]))
                # Errors in the function, I handle that.
                if type(newout[0]) is str:
                    return newout
                
                # Calculate the difference between our objective and the new output
                diff = objectivevalue - newout[0]
                # Check, if the difference is greater than before we need to reverse direction
                # If not we skip this (we're going towards the objective not away from it)
                if abs(diff) > abs(previousdiff):
                    if incrementdir < 0:
                        incrementdir = 1.0
                    else:
                        incrementdir = -1.0
                # Save our diff as the previous one for next time, so we can compare against it
                previousdiff = diff

                # If the difference is smaller than our step size, then we need to shrink our step size
                if abs(incrementval) > abs(diff):
                    incrementval = incrementval/10.0
                    if incrementval < 0.0001:
                        # Set a minimum step size, shouldn't REALLY be necessary, because if we are fine
                        # tuning on the level of a millivolt, we are probably within percent tolerance
                        incrementval = 0.0001

                # calculate the deviatons by our usual method
                if errorflag == "percentdiff":
                    average = (newout[0] + objectivevalue)/2.0
                    errortype[i] = (diff / average)*100.00
                else:
                    errortype[i] = (diff / max(absout))*100.0
                # Update the user as yo what we had expected/how close we were this step
                self.messageSignal.emit("I had expected it to be : " + str(regress[i]))
                self.messageSignal.emit("meaning the new error is : " + str(errortype[i]) + "\n")
                # Write our new output, we will always have at least one standard deviation, we might
                # have two (hence the if)
                outputvalues[i] = newout[0]
                tmpstd1 = newout[1]
                if len(newout) == 3: tmpstd2 = newout[2]
                
            # Save the standard deviations, this is outside the while loop so that only the final
            # values are saved, not all of the intermediate ones
            if samples > 1:
                stddev1.append(tmpstd1)
                stddev2.append(tmpstd2)
        # Zip the two lists together to put them in one slot in the dictionary.
        std_deviation = [stddev1, stddev2]
        # Package all the results together in a dictionary for returning
        outputnames = ['slope', 'intercept', 'r_value', 'std_err', 'std_deviation', 'samples'
        , 'inputvalues', 'outputvalues', 'outputflag', 'calibratedin', 'percentdiff', 'advancedin'
        , 'fullscale', 'scalelist']
        outputvalues = [slope, intercept, r_value, std_err, std_deviation, samples
        , inputvalues, outputvalues, outputflag, calibratedin, percentdiff, advancedin
        , fullscale, scalelist]
        # *clap* and ya done
        return dict(zip(outputnames, outputvalues))

    '''
    The following function takes 3 arguments
    filename is an excel file, (.xlsx only) formatted in the way that is output from exportLinearity
        if it is not things will break in weird ways
    scope and afg are the usual, handles to the scope and function generator

    This test is meant for when you have run a linearity test and then calbiration test on a passive 
    device, this is the last hurrah, and is meant to test the actual, active device you want to test

    So, before running this function make sure you are set up to test the device!
    '''
    def deviceTest(self, filename, source, channel, scope, afg, samples, mathexpression="asdf"):
        # Read from excel file of previous calbiration test
        # Uses the (now) standardized excel format, which is to say all of these
        # linearity test functions will export through the same function
        workbook = xlrd.open_workbook(filename)
        worksheet = workbook.sheet_by_index(0)
        # we need to know what type of output we were dealing with
        outputflag = worksheet.cell_value(0,0)

        if mathexpression != None:
            return self.mathdevice(filename, source, channel, scope, afg, samples, mathexpression)

        # Lists to store our i/o
        vlist = []
        outputin = []
        outputvalues = []
        scalestouse = []
        oscscales = []

        if mathexpression != None:
            scope.write_mathexpression(mathexpression)
        # If we had advanced input, we'll need to know what opeartion/s to perform, even if this is
        # a null string it won't matter (unless this is null AND we need to do advanced input operations)
        advancedin = worksheet.cell_value(7,1)
        # Using the standard .xlsx format, we reading in from the excel sheet starting at 1,2
        row = 1
        col = 2
        for i in range(1, len(worksheet.col(2))):
            # Once again, we want to avoid reading in null values so we break once we hit one.
            if worksheet.cell_value(row, col) == '':
                break
            # outputin is the original list of input voltages, we read this in so we a) know what it is
            # and b) 
            outputin.append(worksheet.cell_value(row, col))
            vlist.append(worksheet.cell_value(row, col+1))
            scalestouse.append(worksheet.cell_value(row, col+6))

            row+=1

        afg.write_burstmodetrig(source)
        afg.write_burstcycles(source, 1)
        afg.write_burststate(source, 'on')
        afg.write_burstsequencetimer('1.0e-4')

        scope.write_horizontalrecord(10000)

        scope.write_headoff()

        self.messageSignal.emit("Testing for the values : " + str(vlist))

        if samples == 1:
            scope.write_acquiremode('sample')
        else:
            scope.write_acquiremode('average')
            scope.write_numavgsamples(samples)
            # lists that will store the standard deviation for the average. We have two since
            # it is possible to do max-min, and the max/min values will have different standard
            # deviations
            stddev1 = []
            stddev2 = []

        # Now we obtain our datapoints, 
        for currentv in vlist:
            sigrange = afg.ask_vpp(source)
            # Voffs of the current signal
            sigoffs = afg.ask_voltageoffs(source)

            sigoffs, sigrange = self.calcnewoffs(sigrange, sigoffs, currentv)
            # Send these values to the function generator
            afg.write_vpp(source, str(sigrange))
            afg.write_voltageoffs(source, str(sigoffs))
       
            # Fetch our output value using zoomonpeak magic
            self.messageSignal.emit("Currently fetching output for : " + str(outputin[vlist.index(currentv)]))
            self.messageSignal.emit("Which is calibrated to : " + str(currentv))
            outval = self.fetchval(scope, afg, outputflag, currentv, channel=channel, source=source, advancedin=advancedin
                , boogaloo=1, scale=scalestouse[vlist.index(currentv)])
            self.messageSignal.emit("I have fetched the value : " + str(outval[0]) + "\n")
            oscilloscale = scope.ask_scale(channel)
            # Make sure we actually found a value before we act like we did
            if type(outval) is str:
                return outval
            else:
                outputvalues.append(outval[0])
                if samples > 1:
                    stddev1.append(outval[1])
                    if len(outval) == 3: 
                        stddev2.append(outval[2])
                    else:
                        stddev2.append(-1)
                else:
                    stddev1.append(-1)
                    stddev2.append(-1)
                oscscales.append(oscilloscale)     

        # Put the scope back in a basic state
        scope.write_zoommode('off')
        scope.write_offset(channel, '0.0')
        zoo.onscreenbyscale(scope, channel=channel)

        # outputvalues, inputvalues, package them for a linear regression test
        x = array(outputin)
        y = array(outputvalues)
        # Use scipy linear regression test to calculate how straight the line was
        # we will return all of these values
        slope, intercept, r_value, p_value, std_err = linregress(x,y)

        std_deviation = [stddev1, stddev2]

        percentdiff = []
        fullscale = []
        absout = []
        for value in outputvalues:
            absout.append(abs(value))
        for xval, yval in zip(vlist, outputvalues): 
            regressval = xval*slope + intercept
            deviation = yval - regressval
            average = (yval + regressval)/2.0
            percentdiff.append((deviation / average)*100.0)
            fullscale.append((deviation/max(absout))*100.0)
        # Package everything for returning it as output
        outputnames = ['slope', 'intercept', 'r_value', 'p_value', 'std_err', 'samples'
        , 'inputvalues', 'outputvalues', 'outputflag', 'percentdiff', 'advancedin', 'fullscale'
        , 'std_deviation', 'scalelist']
        outputvalues = [slope, intercept, r_value, p_value, std_err, samples
        , outputin, outputvalues, outputflag, percentdiff, advancedin, fullscale, std_deviation
        , oscscales]
        # *clap* and ya done
        return dict(zip(outputnames, outputvalues))
    '''
    Dear future person that inherits this project, I'm sorry this file is so hacky and has lots of repeated code written 
    if I had had more time I would have made it much more elegant, but alas I am sitting here frantically throwing this 
    together as fast as I can in a manner that actually works.
    '''
    def mathlinearity(self, afg, scope, startv = 0.0, endv = 1.0, samples = 1
        , numpoints = 20, outputflag = 'max', advancedin="asdf", channel=1, source=1, mathexpression="asdf"):
        # Create a list to which we will append results
        outputvalues = []
        oscscales = []

        # Which two channels are we operating with on the oscilloscope?
        # We use a regex to split the string about a math operater, then parse the two parts for their channel numbers
        splitter = re.compile(r'[\+\-\*\-]')
        channels = splitter.split(mathexpression)
        # Cast our channels as integers, since our functions operate with integer values for the channel
        onchannels = [int(channels[0][-1]), int(channels[1][-1])]

        # Set up the scope and AFG, just like in linearity test, just in case
        afg.write_burstmodetrig(source)
        afg.write_burstcycles(source, 1)
        afg.write_burststate(source, 'on')
        afg.write_burstsequencetimer('1.0e-4')
        scope.write_horizontalrecord(10000)

        # Turn off headers so we can easily read numbers from the scope
        scope.write_headoff()

        # Get parameters for what we're going to do
        # Vpp of the current signal
        sigrange = afg.ask_vpp(source)
        # Voffs of the current signal
        sigoffs = afg.ask_voltageoffs(source)

        # Create a list of channels that we aren't using, so we can turn them off, so we don't pull too many
        # waveforms off the scope and slow things down. We do this with a list of all possible channels, and
        # then removing the ones we are using
        offchannels = [1, 2, 3, 4]
        offchannels.remove(onchannels[0])
        offchannels.remove(onchannels[1])
        # Turn off the channels
        for ch in offchannels:
            scope.write_channelstate(ch, 'off')
        for ch in onchannels:
            scope.write_channelstate(ch, 'on')
        # Turn off math as well, better safe than sorry?
        scope.write_channelstate('Math', 'off')

        # Create a list of voltage values that we are going to test with
        # np.linspace creates an array of evenly spaced values starting
        # at the first argument, and ending at the second argument
        # the list will be as long as the third argument, .tolist() casts
        # it as a list datatype instead of a numpy array
        vlist = linspace(startv, endv, numpoints).tolist()

        # This comes into play since the AFG doesn't recongize 0 volt amplitude
        if startv < 0.05:
            vlist[0] = 0.05

        if samples == 1:
            scope.write_acquiremode('sample')
        else:
            scope.write_acquiremode('average')
            scope.write_numavgsamples(samples)

        # BELOW THIS LINE IS STUFF THAT NEEDS TO BE MOVED INTO A LOOP OVER VOLTAGE INPUT VALUES
        for currentv in vlist:
            # Call a function to calculate the new offset value, the previous method
            # lost accuracy for a greater number of points it tested, this one 
            # always (to my testing limits) gets back to the same voltage/offset combo you
            # started with
            sigoffs, sigrange = self.calcnewoffs(sigrange, sigoffs, currentv)
            # Send these values to the function generator
            afg.write_vpp(source, str(sigrange))
            afg.write_voltageoffs(source, str(sigoffs))

            # Put both signals on screen (vertically) before we pull the waveforms
            for ch in onchannels:
                zoo.onscreenbyscale(scope, ch)

            scalea = scope.ask_scale(onchannels[0])
            scaleb = scope.ask_scale(onchannels[1])

            # We do the following so one of the signals doesn't have a greater accuracy than the other
            # this prevents a weird mixing of precisions when we do our mathematical operations
            if scalea > scaleb:
                scope.write_scale(onchannels[1], scalea)
                THEscale = scalea
            elif scaleb > scalea:
                scope.write_scale(onchannels[0], scaleb)
                THEscale = scaleb
            else:
                # at this point they're equal, so it doesn't matter which we use for THEscale
                THEscale = scalea

            #***Here goes the horizontal scaling if I find that it is necessary, testing it without it at first, since
            # I don't think it will be***

            waveformlist, timelist, yunitslist = scope.get_waveforms()
            # Convert our waveform lists to np.arrays, we subtract one from each because python indexs from 0 (CH1 -> 0, etc.)
            # For now I cut the first 6 points since it seems to be the first 6 consistently give weird values (should be 0
            # but reports like -1.5, which is way smaller than the real minimum)
            waveforma = array(waveformlist[onchannels[0]-1][6:])
            waveformb = array(waveformlist[onchannels[1]-1][6:])
            # The 4th charachter in the mathexpression string is the operator : +, -, /, or *
            mathwaveform = eval("waveforma" + mathexpression[4] + "waveformb")

            if outputflag == 'max':
                outputvalues.append(max(mathwaveform))
                oscscales.append(THEscale)
            elif outputflag == 'min':
                outputvalues.append(min(mathwaveform))
                oscscales.append(THEscale)
            elif outputflag == 'max-min':
                outputvalues.append(max(mathwaveform)-min(mathwaveform))
                oscscales.append(THEscale)
            elif outputflag == 'gain':
                outputvalues.append((max(mathwaveform)-min(mathwaveform))/currentv)
                oscscales.append(THEscale)
            elif outputflag == 'advanced':
                # Advancedin at this point MUST operate on max(mathwaveform) and/or min(mathwaveform)
                outputvalues.append(eval(advancedin))
                oscscales.append(THEscale)

        x = array(vlist)
        y = array(outputvalues)
        # Use scipy linear regression test to calculate how straight the line was
        # we will return all of these values
        slope, intercept, r_value, p_value, std_err = linregress(x,y)

        # Two lists for two different types of error calculation
        percentdiff = []
        fullscale = []
        for xval, yval in zip(vlist, outputvalues): 
            # y=mx+b
            regressval = xval*slope + intercept
            # difference by subtraction
            deviation = yval - regressval
            # average of the two values
            average = (yval + regressval)/2.0
            # percent difference: difference/average times 100
            percentdiff.append((deviation / average)*100.0)
            fullscale.append((deviation/max(outputvalues))*100.0)
        # Package everything for returning it as output

        if advancedin == "":
            advancedin = 'default'

        outputnames = ['slope', 'intercept', 'r_value', 'p_value', 'std_err', 'inputvalues'
        , 'outputvalues', 'outputflag', 'percentdiff', 'advancedin', 'fullscale', 'scalelist'
        , 'samples', 'mathflag']
        outputvalues = [slope, intercept, r_value, p_value, std_err, vlist
        , outputvalues, outputflag, percentdiff, advancedin, fullscale, oscscales
        , samples, 1]
        # *clap* and ya done
        return dict(zip(outputnames, outputvalues))

    def mathcalibration(self, filename, afg, scope, percenttolerable, samples, channel, source, mathexpression):
        # Sorry, but I am out of time and I can't get these to working condition by my last day. If you want to
        # implement these, follow the example of math linearity/calibrationtest.
        return "Sorry but this version of Heptagon does not yet have a math calibration test implemented."

    def mathdevice(self, filename, source, channel, scope, afg, samples, mathexpression):
        # Sorry, but I am out of time and I can't get these to working condition by my last day. If you want to
        # implement these, follow the example of math linearity/devicetest.
        return "Sorry but this version of Heptagon does not yet have a math device test implemented."


'''
Exports the data that linearity test returns to an excel file

filename is a string to be file's name, needs to end with .xlsx
datadict is a dictionary of the form exactly output by linearitytest

It exports the data to an excel file with 1 worksheet
The columns are all labeled, it also generates two charts, one of the 
input/output values vs the input/linear regression values the other is
percent difference of each output value from the linear regression values
'''
def exportLinearity(filename, datadict, calibrationExport=0):
    # Create the excel file by creating a workbook class, and giving it a worksheet
    workbook = xlsxwriter.Workbook(filename)
    worksheet = workbook.add_worksheet()
    # I don't know what this does exactly, I just don't want this to break in whatever weird
    # way it did before, and it works with this, so this stays.
    bold = workbook.add_format({'bold': 1})
    # calibrationExport is for when we are exporting the results of calibration test, it includes
    # added columns for standard deviation, any subsequent case of this type of if statement is
    # doing just that.
    if datadict.get('mathflag') == 1:
        headings = ['Regression Values', 'Input', 'Output', 'Regression Points','Percent Diff'
        , 'Full Scale', 'Scale Used']
    else:
        if calibrationExport == 0:
            headings = ['Regression Values', 'Input', 'Output', 'Regression Points','Percent Diff'
            , 'Full Scale', 'Scale Used', 'Standard Deviation']
        else:
            headings = ['Regression Values', 'Input', 'Calibrated Input', 'Output', 'Regression Points'
            , 'Percent Diff', 'Full Scale', 'Scale Used', 'Standard Deviation']
    # A list of two lists, where each sublist is a list of the values we get as input (output from
    # linearity test)
    datapairs = zip(datadict.get('inputvalues'), datadict.get('outputvalues'))
    # deviation list, calculates how different each output value is from what the linear regression
    # predicts it should be, by percent difference which is (difference of two vals/average of two vals)
    percentdiff = datadict.get('percentdiff')
    fullscale = datadict.get('fullscale')
    oscscales = datadict.get('scalelist')
    # A list of values that we think the yval should be, based on our linear regression, we generate
    # this so we can plot it in excel.
    regresslist = []
    for xval, yval in datapairs:
        regresslist.append(xval*datadict.get('slope') + datadict.get('intercept'))
    # All of the data we are writing, saved in one list to write with.
    if datadict.get('mathflag') == 1:
        data = [
            datadict.get('inputvalues'),
            datadict.get('outputvalues'),
            regresslist,
            percentdiff,
            fullscale,
            oscscales
        ]
    else:
        if calibrationExport == 0:
            data = [
                datadict.get('inputvalues'),
                datadict.get('outputvalues'),
                regresslist,
                percentdiff,
                fullscale,
                oscscales,
                datadict.get('std_deviation')[0],
                datadict.get('std_deviation')[1]
            ]
        else:
            data = [
                datadict.get('inputvalues'),
                datadict.get('calibratedin'),
                datadict.get('outputvalues'),
                regresslist,
                percentdiff,
                fullscale,
                oscscales,
                datadict.get('std_deviation')[0],
                datadict.get('std_deviation')[1]
            ]  
    # The first column in the excel file, labels for the regression parameters
    firstcol = [
        "Slope",
        "Intercept",
        "R Value",
        "R Squared",
        "Standard Error",
        "Samples",
        "Advanced In"
    ]
    # The second column in the excel file, the actual values for what's listed above
    seccol = [
        datadict.get('slope'),
        datadict.get('intercept'),
        datadict.get('r_value'),
        datadict.get('r_value')**2,
        datadict.get('std_err'),
        datadict.get('samples'),
        datadict.get('advancedin')
    ]
    # Write to the worksheet, the first argument is the cell it starts from, the second
    # is the sequence of things for it to write
    worksheet.write(0, 0, datadict.get('outputflag'))
    worksheet.write_row('B1', headings, bold)
    worksheet.write_column('A2', firstcol)
    worksheet.write_column('B2', seccol)
    worksheet.write_column('C2', data[0])
    worksheet.write_column('D2', data[1])
    worksheet.write_column('E2', data[2])
    worksheet.write_column('F2', data[3])
    worksheet.write_column('G2', data[4])
    worksheet.write_column('H2', data[5])
    if datadict.get('mathflag') == None:
        worksheet.write_column('I2', data[6])
        worksheet.write_column('J2', data[7])
    if calibrationExport == 1:
        worksheet.write_column('K2', data[8])

    # Add a chart, give it a type/subtype
    chart1 = workbook.add_chart({'type': 'scatter',
                             'subtype': 'straight_with_markers'})

    if calibrationExport == 0:
        # Configure the first series. (set of points)
        chart1.add_series({
            'name':       '=Sheet1!$D$1',
            'categories': ['Sheet1', 1, 2, len(datadict.get('inputvalues')), 2],
            'values':     ['Sheet1', 1, 3, len(datadict.get('inputvalues')), 3],
        })
        # Configure the second series
        chart1.add_series({
            'name':       '=Sheet1!$B$1',
            'categories': ['Sheet1', 1, 2, len(datadict.get('inputvalues')), 2],
            'values':     ['Sheet1', 1, 4, len(datadict.get('inputvalues')), 4],
        })
    else:
        # Configure the first series. (set of points)
        chart1.add_series({
            'name':       '=Sheet1!$E$1',
            'categories': ['Sheet1', 1, 2, len(datadict.get('inputvalues')), 2],
            'values':     ['Sheet1', 1, 4, len(datadict.get('inputvalues')), 4],
        })
        # Configure the second series
        chart1.add_series({
            'name':       '=Sheet1!$B$1',
            'categories': ['Sheet1', 1, 2, len(datadict.get('inputvalues')), 2],
            'values':     ['Sheet1', 1, 5, len(datadict.get('inputvalues')), 5],
        })
        

    # Add a chart title and some axis labels.
    chart1.set_title ({'name': 'Input/Output vs Linear Regression'})
    chart1.set_x_axis({'name': 'Input Voltage (V)'})
    chart1.set_y_axis({'name': 'Output Voltage (V)'})

    # Set an Excel chart style.
    chart1.set_style(12)

    # Insert the chart into the worksheet (with an offset).
    worksheet.insert_chart('L1', chart1, {'x_offset': 25, 'y_offset': 10})
    # Repeat, this time instead of plotting the input/output and regression line we will plot
    # the Percent difference of the output value from the regression value, at each input value
    chart2 = workbook.add_chart({'type': 'scatter'})

    if calibrationExport == 0:
        # Configure the first series.
        chart2.add_series({
            'name': '=Sheet1!$F$1',
            'categories': ['Sheet1', 1, 2, len(datadict.get('inputvalues')), 2],
            'values':     ['Sheet1', 1, 5, len(datadict.get('inputvalues')), 5],    })
    else:
        # Configure the first series.
        chart2.add_series({
            'name': '=Sheet1!$G$1',
            'categories': ['Sheet1', 1, 2, len(datadict.get('inputvalues')), 2],
            'values':     ['Sheet1', 1, 6, len(datadict.get('inputvalues')), 6],    })

    # Add a chart title and some axis labels.
    chart2.set_title ({'name': 'Percent Diff from Regression'})
    chart2.set_x_axis({'name': 'Input Voltage (V)'})
    chart2.set_y_axis({'name': 'Difference (%)'})

    # Set an Excel chart style.
    chart2.set_style(11)

    # Insert the chart into the worksheet (with an offset).
    worksheet.insert_chart('L16', chart2, {'x_offset': 25, 'y_offset': 10})

    chart3 = workbook.add_chart({'type': 'scatter'})

    if calibrationExport == 0:
        # Configure the first series.
        chart3.add_series({
            'name': '=Sheet1!$F$1',
            'categories': ['Sheet1', 1, 2, len(datadict.get('inputvalues')), 2],
            'values':     ['Sheet1', 1, 6, len(datadict.get('inputvalues')), 6],    })
    else:
        # Configure the first series.
        chart3.add_series({
            'name': '=Sheet1!$G$1',
            'categories': ['Sheet1', 1, 2, len(datadict.get('inputvalues')), 2],
            'values':     ['Sheet1', 1, 7, len(datadict.get('inputvalues')), 7],    })

    # Add a chart title and some axis labels.
    chart3.set_title ({'name': 'Full Scale Error'})
    chart3.set_x_axis({'name': 'Input Voltage (V)'})
    chart3.set_y_axis({'name': 'Difference (%)'})

    # Set an Excel chart style.
    chart3.set_style(11)

    # Insert the chart into the worksheet (with an offset).
    worksheet.insert_chart('L30', chart3, {'x_offset': 25, 'y_offset': 10})

    workbook.close()
