#include <stdio.h>
#include <string.h>

#include "VISA/visa.h"

#define MAX_CNT 256

int main(void) {
	ViStatus	status; 			// For checking errors
	ViSession 	defaultRM, instr; 	// Communication channels
	ViUInt32 	retCount; 			// Return count for string I/O
	ViChar 		buffer[MAX_CNT]; 	// Buffer for string I/O

	// Initialize the system
	// Check for failure after each command; included only once for brevity
	status = viOpenDefaultRM(&defaultRM);
	if (status < VI_SUCCESS) {
		return -1;
	}

	// Open communication with GPIB Device at Primary Address 1
	status = viOpen(defaultRM, "GPIB::1::INSTR", VI_NULL, VI_NULL, &instr);

	// Set the timeout for message-based communication
	status = viSetAttribute(instr, VI_ATTR_TMO_VALUE, 5000);

	// Ask the device for identification
	status = viWrite(instr, "*IDN?\n", 6, &retCount);
	status = viRead(instr, buffer, MAX_CNT, &retCount);

	// Call other functions here
	// TODO: Try asking user for a string
	char message[64];
	strcpy (message, "Let's do some science.");

	char command[94];
	strcpy (command, "DISPlay:WINDow:TEXT:DATA ");
	strcat (command, message);

	status = viWrite(instr, command, strlen(command), &retCount);

	// Close system
	status = viClose(instr);
	status = viClose(defaultRM);

	printf("Text complete\n");
	return 0;
}
