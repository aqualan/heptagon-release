#include <stdio.h>

#include "VISA/visa.h"

#define MAX_CNT 256

int main(void)
{
	ViSession 		rm, vi;
	ViStatus 		status;
	ViChar 			desc[MAX_CNT], id[MAX_CNT], buffer[MAX_CNT];
	ViUInt32 		retCnt, itemCnt;
	ViFindList 		list;
	ViUInt32 		i;

	// Open a default session
	status = viOpenDefaultRM(&rm);
	if (status < VI_SUCCESS) goto error;

	// Find all GPIB devices
	status = viFindRsrc(rm, "GPIB?*INSTR", &list, &itemCnt, desc);
	if (status < VI_SUCCESS) goto error;
	
	for (i = 0; i < itemCnt; i++) {
		// Open resource found in rsrc list
		status = viOpen(rm, desc, VI_NULL, VI_NULL, &vi);
		if (status < VI_SUCCESS) goto error;

		// Send ID query
		status = viWrite(vi, (ViBuf) "*IDN?", 5, &retCnt);
		if (status < VI_SUCCESS) goto error;

		// Clear the buffer and read the response
		status = viRead(vi, (ViBuf) id, sizeof(id), &retCnt);
		id[retCnt] = '\0'; // Null terminator
		if (status < VI_SUCCESS) goto error;

		// Print the response
		printf("id: %s: %s\n", desc, id);

		// Close device, as we are now done with it.
		viClose(vi);

		// Get the next item.
		viFindNext(list, desc);
	}

	// Clean up
	viClose(rm);

	error:
		// Report and clean up
	viStatusDesc(vi, status, buffer);
	fprintf(stderr, "failure: %s\n", buffer);
	if (rm != VI_NULL) {
		viClose(rm);
	}
	return 1;
}
