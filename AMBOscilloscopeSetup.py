'''
Automatically sets up an MSO4000 oscilloscope for AMB testing.

Usage: 	Call setup().

Arguments:	descriptor	: String 	: Instrument descriptor for the scope
			numavg 		: Int 		: # of measurements to take for averaging

'''

import visa

def setup(descriptor, numavg):
	# Setup
	rm = visa.ResourceManager()
	scope = rm.get_instrument(descriptor)

	# Turn off the ones that we don't need.
	scope.write('select:bus1 off')
	scope.write('select:bus2 off')
	scope.write('select:bus3 off')
	scope.write('select:bus4 off')
	scope.write('select:ch4 off')

	# Turn on the correct channels
	scope.write('select:ch1 on')
	scope.write('select:ch2 on')
	scope.write('select:ch3 on')
	scope.write('select:ref1 on')
	scope.write('select:ref2 on')
	scope.write('select:ref3 on')
	scope.write('select:ref4 on')
	scope.write('select:math on')

	# Set acquisition mode to average
	scope.write('acquire:mode average')
	scope.write('acquire:numavg ' + str(numavg))

	# Set up math to combine differential signals 1 and 2
	scope.write('math:define "ch2-ch1"')

	# Set the correct zooms# 500mV for 1,2,3; 200mV for math
	scope.write("ch1:scale 500E-3")
	scope.write("ch2:scale 500E-3")
	scope.write("ch3:scale 500E-3")
	scope.write("math:vertical:scale 200E-3")

	# Set 1,2,3 impedances
	scope.write("ch1:termination meg")
	scope.write("ch2:termination meg")
	scope.write("ch3:termination fifty")

	# Turn off all measurements.
	for i in range (1, 9):
		scope.write("measurement:meas%d:state off" %(i))

