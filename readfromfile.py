'''
Load waveform data from a file, simplified as of 6/13/2014, it reads in data from
the file you feed to the function. Outputs the see below.


Returns a dictionary with the following things packed inside:
frequency : one over the max time value listed in the timevals, parameter for AFG
maxref : maximum voltage in the signal
minref : minimum voltage in the signal
rangeref : difference between maxref and minref, it is the Vpp parameter for AFG
midref : maxref - (rangeref / 2), it is the offset parameter for AFG
refform : a list of the values found in the text file, for reference
timevals : a list of the time values found in the text file
'''
import re

def importdata(inputname):
    # Open the file, and read in voltages/times to a list
    datafile = open(inputname, 'r')

    # Create lists that will be zipped into a dict for return
    outputnames = []
    outputvals = []

    # Use regular expressions for arbritrary precision of input file
    # It matches any number of numeric characters. + signs, - signs and .
    searcher = re.compile(r'[0-9.+-]+')

    # Lists to store information that we read in from the file
    refform = []
    timevals = []

    # Assumes correctly formatted file, which is the format generated as LTSpice output
    # This output format is ####E+###   ####E-### where there can be any number of digits
    # for any set of #'s, the numbers are scientific notation of arbitrary precision
    # they are organized such that it is one time value and one voltage value per line
    # the time value comes first, then voltage, and the values are seperated by a character
    # that does NOT match the above regex, which is whitespace, letters, symbols, etc...
    # the first line in the file is a title line, and thus ignored
    for line in datafile.readlines()[1:]:
        if len(line.strip()):
            # returns a tuple of the matches, two digits, two exponents
            results = searcher.findall(line)
            # the first pair of results is time
            # the time digit, the value multiplied by 10^x
            timed = float(results[0])
            # x in the above comment
            timee = float(results[1])
            timevals.append(timed*(10**timee))
            # same format for time, the second pair is voltage though
            digit = float(results[2])
            exponent = float(results[3])
            refform.append(digit * (10**exponent))

    # Close the file since we are done with it
    datafile.close()

    # Append these new lists to our output lists
    outputnames.extend(['refform', 'timevals'])
    outputvals.extend([refform, timevals])

    # Calculate a few useful parameters and package them with our output
    # The max value in the waveform
    maxref = max(refform)
    # The minimum value in the waveform
    minref = min(refform)
    # The range of values, the Vpp
    rangeref = maxref - minref
    # The vertical offset the AFG will use
    midref = maxref - (rangeref/2.0)
    # The frequency the AFG will use
    frequency = 1.0/max(timevals)

    # Append these parameters to our outputlists
    outputnames.extend(['maxref', 'minref', 'rangeref', 'midref', 'frequency'])
    outputvals.extend([maxref, minref, rangeref, midref, frequency])

    # Zip the two lists into a dictionary, and return the dictionary
    thing = zip(outputnames, outputvals)
    output = dict(thing)
    return output
