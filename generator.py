#!/usr/bin/env python

'''
generator - Keeps the lights on.

Actual function:    Holds the generator class, which is the virtual function
                    generator class.

Classes:            generator

Notes:              For now, the generator class is only
                    compatible with Tektronix function generators. However,
                    it may be changed later to work with other kinds of
                    function generators.
'''

from visa import instrument

class generator(object):

    '''
    A virtual function generator. This class holds the properties that are
    grabbed from the function generator, and is used to communicate with the
    function generator.

    Functions: getGeneratorAtt
    '''

    def __init__(self, generator_name, parent=None):

        #---Initializing function generator communicator. The communicator is used
        #to read and write messages to the function generator.---#
        self.parent = parent
        self.communicator = instrument(generator_name)

        #---Initializing attributes of the scope.---#

        self.type = generator_name[6:20]

        #self.properties is a dictionary that holds the properties of each channel
        #in the function generator (2). Note that the key for each property is named
        #after the command that is used to obtain that property using Tektronix syntax.

        self.properties = {'vvp': 'float', 'voltageoffs': 'float',
                         'freq': 'float', 'burstmode': 'string',
                         'resolution': 'float', 'timeu': 'float',
                         'voltageunit': 'string', 'type': 'string'}

        #Each property in the dictionary is changed to a list of length 3. The first
        #element of each list indicates the data type of the property, the second
        #element gives the property value for channel one, and the third element
        #gives the property value for channel two.
        
        for i in self.properties:
            if self.properties[i] == 'float':
                self.properties[i] = ['float', None, None]
            elif self.properties[i] == 'string':
                self.properties[i] = ['string', None, None]

    #Below are the commands that are used to communicate with the function generator.
    #Be warned all ye who enter.
    #
    #Note that commands that begin with the word "write" use visa's write command, while
    #commands that begin with the word "ask" use visa's ask command. An improved version
    #of visa's read, write, and ask commands have been included, which prevent the program
    #from experiencing an error if it tries to write a command after a device has been disconnected.

    def ask(self, message):
        while self.parent.backend.AFGConnected:
            try: return self.communicator.ask(message)
            except: pass
        
    def ask_burstmode(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            return str(self.ask('sour' + str(channel) + ':BURST:MODE?').strip())
        elif self.type == '0x0957::0x4108':
            return str(self.ask('burst' + str(channel) + ':mode?').strip())

    def ask_freq(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            return float(self.ask('sour' + str(channel) + ':FREQ?').strip())
        elif self.type == '0x0957::0x4108':
            return float(self.ask('freq' + str(channel) + '?').strip())

    def ask_invert(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            state = self.ask('output' + str(channel) + ':polarity?')
            if state == None: return
            else: return state.strip().encode('ascii', 'ignore')
        elif self.type == '0x0957::0x4108':
            state = self.ask('output' + str(channel) + ':polarity?')
            if state == None: return
            else: return state.strip().encode('ascii', 'ignore')            

    def ask_leadedgetime(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            time = self.ask('sour' + str(channel) + ':puls:tran:lead?')
            if time == None: return
            else: return float(time.strip())
        elif self.type == '0x0957::0x4108':
            time = self.ask('source:fuction' + str(channel) + ':pulse:tran:leading?')
            if time == None: return
            else: return float(time.strip())

    def ask_outputstate(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            state = self.ask('output' + str(channel) + ':state?')
            if state == None: return
            else: return state.strip().encode('ascii', 'ignore')
        elif self.type == '0x0957::0x4108':
            state = self.ask('out' + str(channel) + '?')
            if state == None: return
            else: return state.strip().encode('ascii', 'ignore')

    def ask_voltageoffs(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            return float(self.ask('sour' + str(channel) + ':VOLT:OFFS?').strip())
        elif self.type == '0x0957::0x4108':
            return float(self.ask('voltage' + str(channel) + ':offset?').strip())

    def ask_voltageunit(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            return str(self.ask('sour' + str(channel) + ':VOLT:UNIT?').strip())
        elif self.type == '0x0957::0x4108':
            return str(self.ask('voltage' + str(channel) + ':unit?').strip())

    def ask_vpp(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            return float(self.ask('sour' + str(channel) + ':VOLT?').strip())
        elif self.type == '0x0957::0x4108':
            return float(self.ask('voltage' + str(channel) + '?').strip())

    def getGeneratorAtt(self, num):
        '''
        This function obtains properties from the function generator.

        Arguments: 1
            num: The channel number that properties are being obtained from.
        '''
        
        self.write_headoff
        for i in self.properties:
            if i != 'resolution' and i != 'timeu' and i != 'type':
                self.properties[i][num] = eval('self.ask_' + i + '(num)')
                                                               
    def read(self):
        while self.parent.backend.AFGConnected:
            try: return self.communicator.read().strip()
            except: pass

    def write(self, message):
        while self.parent.backend.AFGConnected:
            try: return self.communicator.write(message)
            except: pass

    def write_arbresolution(self, resolution):
        
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('trace:define ememory, ' + str(resolution))
        elif self.type == '0x0957::0x4108':
            return

    def write_burstcycles(self, channel, ncycles):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('sour' + str(channel) + ':burst:ncycles ' + str(ncycles))
        elif self.type == '0x0957::0x4108':
            self.write('burst' + str(channel) + ':ncycles ' + str(ncycles))

    def write_burstmodegate(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('source' + str(channel) + ':burst:mode gate')
        elif self.type == '0x0957::0x4108':
            self.write('burst' + str(channel) + ':mode gate')

    def write_burstmodetrig(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('source' + str(channel) + ':burst:mode trig')
        elif self.type == '0x0957::0x4108':
            self.write('burst' + str(channel) + ':mode trig')

    def write_burstsequencetimer(self, time):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('trigger:sequence:timer ' + str(time))
        elif self.type == '0x0957::0x4108':
            self.write('arm:per1 ' + str(time))
            return

    def write_burststate(self, channel, state):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('sour' + str(channel) + ':burst:state ' + state)
        elif self.type == '0x0957::0x4108':
            self.write('burst' + str(channel) + 'state ' + str(state))

    def write_continuousmode(self, channel):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('source' + str(channel) + ':frequency:mode cw')
        elif self.type == '0x0957::0x4108':
            self.write('arm:source' + str(channel) + ' immediate')

    def write_frequency(self, channel, frequency):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('source' + str(channel) + ':frequency ' + str(frequency))
        elif self.type == '0x0957::0x4108':
            self.write('freq' + str(channel) + ' ' + str(frequency))

    def write_headoff(self):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('HEAD OFF')
        elif self.type == '0x0957::0x4108':
            return

    def write_headon(self):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('HEAD ON')
        elif self.type == '0x0957::0x4108':
            return    

    def write_invert(self, channel, state):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('output' + str(channel) + ':polarity ' + str(state))
        elif self.type == '0x0957::0x4108':
            self.write('output' + str(channel) + ':polarity ' + str(state))

    def write_outputstate(self, channel, state):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('output' + str(channel) + ':state ' + str(state))
        elif self.type == '0x0957::0x4108':
            self.write('out' + str(channel) + ' ' + str(state))

    def write_voltageoffs(self, channel, offset):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('source' + str(channel) + ':volt:lev:offs ' + str(offset) + 'V')
        elif self.type == '0x0957::0x4108':
            self.write('voltage' + str(channel) + ':offset ' + str(offset))

    def write_vpp(self, channel, vpp):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('source' + str(channel) + ':volt ' + str(vpp))
        elif self.type == '0x0957::0x4108':
            self.write('voltage' + str(channel) + ' ' + str(vpp))

    def write_waveform(self, waveform):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('trace ememory, ' + waveform)
        elif self.type == '0x0957::0x4108':
            self.write('data:dac VOLATILE, ' + waveform)
            return
    '''
    def write_wavepoint(self, pindex, point):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.communicator.write('trace:data:value ememory, ' + str(pindex) + ', ' + str(point))
        elif self.type == '0x0957::0x4108':
            # Move waveform supplying channel1 to temporary memory
            self.communicator.write('data1') 
            # Move said waveform to full memory
            self.communicator.write('data1:copy')
            return
    '''
      
    def write_wavetype(self, channel, wavetype):
        if (self.type == '0x0699::0x0345') or (self.type == '0x0699::0x040B'):
            self.write('source' + str(channel) + ':function ' + wavetype)
        elif self.type == '0x0957::0x4108':
            if wavetype == 'ememory':
                wavetype = 'user'
            self.write('apply' + str(channel) + ':' + str(wavetype))
    
