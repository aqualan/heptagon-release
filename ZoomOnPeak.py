#! usr/bin/python
'''
Functionizing: needs scope input, which feature your looking at, which will
default to max, can bet set to min as well, that's all for now (6/17)

Needs the scope, the function generator, the list of timevalues and voltage
values, and optional input of the feature we are looking for (max, min, 
max-min, and others that are WIP)

TIME VALUES MUST HAVE UNITS OF SECONDS
VOLTAGE VALUES MUST HAVE UNITS OF VOLTS

'''
import visa
from time import sleep

'''
Onscreenparams takes two arguments, a handle to the oscilloscope called scope
and an integer indiciating which channel it is supposed to act on.

The function is supposed to set up the scope in a basic state, in preparation
for onscreenbyscale, it exists as a separate function since it's a lot of commands
and they don't need to be called repeatedly. (onscreenbyscale is a recursive function)
'''
def onscreenparams(scope, channel=1):
    # Turns headers from the scope off, meaning we only get number values
    # when we query measurements, instead of values and labels, this makes
    # it easier to cast the values as floats
    scope.write_headoff()

    # Set measurements so we have the max/min measurements available for querying
    scope.write_meastype(1, 'max')
    scope.write_meastype(2, 'mini')
    scope.write_measstate(1, 'on')
    scope.write_meassource(1, str(channel))
    scope.write_measstate(2, 'on')
    scope.write_meassource(2, str(channel))

    # Finally, set the position and voltage offset of the scope to 0, so
    # we are starting frm square one, meaning less checking work and stuff
    scope.write_position(str(channel), '0.0')
    scope.write_offset(str(channel), '0.0')
    return


'''
onscreenbyscale takes three arguments, a handle to the oscilloscope, an integer
for which channel it is acting on, and an integer flag for if it is being called
for the first time.

firstcall=1 means it is being called for the first time, which means it will call
onscreenparams, otherwise it will not.

It sets the scale, checks to see if the signal is clipping, if it is we set a larger
scale, and repeat until it's not clipping.
'''
def onscreenbyscale(scope, channel=1):
    # If this is the first call, then we need to assume the scope is not ready
    # based on the assumptions we make later on, so we call this to set it up
    # also we start at the smallest scale and work our way up, checking each
    # one along the way to find the smallest one that works (most detail)
    onscreenparams(scope, channel=channel)
    scope.write_scale(str(channel), '1.0')

    # Query what it thinks the max/min values are, after giving it time to acquire
    sleep(0.5)
    maxval = scope.ask_measval(1)
    minval = scope.ask_measval(2)

    # A huge switch statement, it used to be a calculation, but this turns out to be much
    # faster, basically preprocessed all of the choices and wrote them out in this form
    # The if statements are made based on what the maximum value is for the screen at each
    # scale, and then modified slightly to account for noise casuing it to jump off screen
    # All scales are in units of volts
    if maxval > 5.0 and abs(minval) > 5.0:
        # A flag of not 1 will cause the code to resort to extreme measures
        return 0
    elif maxval < 0.0045 and abs(minval) < 0.0045:
        scope.write_scale(str(channel), '0.001')
        return 1
    elif maxval < 0.0095 and abs(minval) < 0.0095:
        scope.write_scale(str(channel), '0.002')
        return 1
    elif maxval < 0.0245 and abs(minval) < 0.0245:
        scope.write_scale(str(channel), '0.005')
        return 1
    elif maxval < 0.0495 and abs(minval) < 0.0495:
        scope.write_scale(str(channel), '0.01')
        return 1
    elif maxval < 0.095 and abs(minval) < 0.095:
        scope.write_scale(str(channel), '0.02')
        return 1
    elif maxval < 0.249 and abs(minval) < 0.249:
        scope.write_scale(str(channel), '0.05')
        return 1
    elif maxval < 0.49 and abs(minval) < 0.49:
        scope.write_scale(str(channel), '0.1')
        return 1
    elif maxval < 0.925 and abs(minval) < 0.925:
        scope.write_scale(str(channel), '0.2')
        return 1
    elif maxval < 2.45 and abs(minval) < 2.45:
        scope.write_scale(str(channel), '0.5')
        return 1
    elif maxval < 5.0 and abs(minval) < 5.0:
        return 1
    else:
        # Flag of not one: resort to extreme measures, if those don't work then you're SoL
        return 2


'''
onscreenbyshift2 takes 3 arguments, 2 are optional
scope is a handle to the oscilloscope
channel is an integer indicating which channel it shall act on
maxormin is the feature it is looking for.

This function currently exists only to check the first onscreenbyshift
basically it sets an offset based on what it thinks should be the correct
offset, then calls this to make sure.

This checks to see if the signal is clipping, if it is it moves up or down
(based on which way it is clipping) until it is no longer clipping. If necessary
it will change the scale of the scope. It only does this if the scale it's at does
not allow for the necessary offset
'''
def onscreenbyshift2(scope, channel=1, maxormin="max"):

    # Query the current scale, so we can find what the max value is
    currentscale = scope.ask_scale(str(channel))
    screenlimit = currentscale*5.0

    currentoffs = scope.ask_offs(str(channel))

    # This is voltage at the top/bottom of the screen
    # used to test for clipping
    topscreen = currentoffs+screenlimit
    botscreen = currentoffs-screenlimit

    # Query what it thinks the max/min values are
    if maxormin == "max":
        sleep(0.5)
        seekval = scope.ask_measval('1')
    elif maxormin == "min":
        sleep(0.5)
        seekval = scope.ask_measval('2')

    # A list of vertscales to use, in case we need to change to a larger one
    # we use our currentscale to find where we are in the list, then increment
    # that index later on if necessary
    vertscales = [0.001, 0.002, 0.005, 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1.0]
    if vertscales.count(currentscale):
        ourindex = vertscales.index(currentscale)
    else:
        ourindex = 0
    # If we are reading the value as larger than the top of the screen
    # we need to seek upwards, so we increase offset by enough to not
    # skip anything (one half screen worth of volts) if that offset
    # is beyond the limit of the current scale, we change the scale
    # instead
    if seekval > topscreen:
        # Adjust our offset value by half the total screen
        currentoffs += screenlimit
        # Write the new value to the scope
        scope.write_offset(str(channel), str(currentoffs))
        # Ask what it says the offset is, we check this because if it is equal to what it was before
        # then that means we hit our limit at that scale, and we need to try a new one.
        # NOTE: This doesn't use == because == is unreliable with floats in python
        writeoffs = scope.ask_offs(str(channel))
        if writeoffs < (currentoffs-0.001):
            # If our index is 9, we are at the end of the list and if we increment by one the program
            # will crash because of the out of bounds index, it also means that we have nothing else to
            # try so we return an error.
            if ourindex == 9:
                return "The necessary scale/offset combination is not attainable due to the limits of the scope."
            # Write our new scale to the scope if we have a new one to write with, and try again (recursion!)
            scope.write_scale(str(channel), str(vertscales[ourindex + 1]))
        onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
    # Samething as above, but we move down instead of up, in case the feature
    # is below the current screen instead of above (negative clipping)
    elif seekval < botscreen:
        currentoffs = currentoffs - screenlimit
        scope.write_offset(str(channel), str(currentoffs))
        writeoffs = scope.ask_offs(str(channel))
        if writeoffs < (currentoffs-0.001):
            if ourindex == 9:
                return "The necessary scale/offset combination is not attainable due to the limits of the scope."
            scope.write_scale(str(channel), str(vertscales[ourindex + 1]))
        onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
    # If it's neither, then that means we are on screen, and we must be done
    else:
        return 0


'''
onscreenbyshift takes 4 arguments, 3 optional
scope is a handle to the oscilloscope
channel is the channel it is to act on, an integer, defaults to 1
maxormin is the feature it is looking for, max or min, defaults to max
onscreen should only be set to 1 if you know for a fact that the signal
    will actualyl be on screen, it will seriously mess up if not.

The way it works is it puts the signal on screen with onscreenbyscale, if it
isn't already there, and then reads the maxormin value, and uses a switch statement
to determine what the offset/scale will be to put just the feature on screen, at the
smallest scale possible. If the signal is greater than 5 V then you're boned. But not really
it will just be really slow.
'''
def onscreenbyshift(scope, channel=1, maxormin="max", onscreen=0):
    # If the signal isn't onscreen, then we need to put it on screen for this to work
    if onscreen == 0:
        onscreenbyscale(scope, channel=channel)

    # It queries twice because for some unknown reason the first query doesn't work
    # I've tried everything I could think of, but this works so if it ain't broke don't fix it
    if maxormin == "max":
        # Ask what the maximum value is
        seekval = scope.ask_measval('1')
        # If it lied to us and said it was 9.91e37, then we ask again, this generally works
        if seekval > 9.9e37:
            sleep(0.5)
            seekval = scope.ask_measval('1')
    elif maxormin == "min":
        seekval = scope.ask_measval('2')
        if seekval > 9.9e37: 
            sleep(0.5)
            seekval = scope.ask_measval('2')

    # Now here is the long switch statement, these cover a number of cases for the correct
    # offset/scale combination to put the peak on screen with the minimum possible scale
    # in order to get the most accurate reading we can.
    # In all cases we call a function to ensure that it actually worked (checks for clipping)
    # This first case, if the signal is less than 1V in amplitude, then 1mv/div scale and
    # it's amplitude is the offset we require (1mv/div is limited to 1V offset)
    if abs(seekval) < 1.0045:
        scope.write_offset(str(channel), str(seekval))
        scope.write_scale(str(channel), '0.001')
        successflag = onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
        return successflag
    # This case is the first one that needs a larger resolution, each step from here is the
    # next larger resoltuion, the value we check in the elif statements corresponds to the 
    # most extreme value we can see on the screen clearly at that resolution/offset
    elif abs(seekval) < 1.0085:
        scope.write_offset(str(channel), str(1.0*(seekval/abs(seekval))))
        scope.write_scale(str(channel), '0.002')
        successflag = onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
        return successflag
    elif abs(seekval) < 1.0235:
        scope.write_offset(str(channel), str(1.0*(seekval/abs(seekval))))
        scope.write_scale(str(channel), '0.005')
        successflag = onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
        return successflag
    elif abs(seekval) < 1.0495:
        scope.write_offset(str(channel), str(1.0*(seekval/abs(seekval))))
        scope.write_scale(str(channel), '0.01')
        successflag =  onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
        return successflag
    elif abs(seekval) < 1.095:
        scope.write_offset(str(channel), str(1.0*(seekval/abs(seekval))))
        scope.write_scale(str(channel), '0.02')
        successflag = onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
        return successflag
    elif abs(seekval) < 1.245:
        scope.write_offset(str(channel), str(1.0*(seekval/abs(seekval))))
        scope.write_scale(str(channel), '0.05')
        successflag = onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
        return successflag
    elif abs(seekval) < 5.495:
        scope.write_offset(str(channel), str(seekval))
        scope.write_scale(str(channel), '0.1')
        successflag = onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
        return successflag
    # If nothing worked at this point, we have to do the slow method, which is to increment
    # offset from square 1.
    else:
        scope.write_scale(str(channel), '0.2')
        successflag = onscreenbyshift2(scope, channel=channel, maxormin=maxormin)
        return successflag

'''
These are the extrememeasures I have mentioned in above comments, basically this is a function that will definetely
work as long as it is possible in the first place, it's just really slow. It takes 3 arguments, the scope handle
the feature it's looking for, and channel it's looking on
'''
def extrememeasures(scope, maxormin, channel):
    scope.write_headoff()

    # Set measurements so we have the max/min measurements available for querying
    scope.write_meastype(1, 'max')
    scope.write_meastype(2, 'mini')
    scope.write_measstate(1, 'on')
    scope.write_meassource(1, str(channel))
    scope.write_measstate(2, 'on')
    scope.write_meassource(2, str(channel))

    # Call the slow method that will force it to work if possible
    successflag = onscreenbyshift2(scope, channel=channel, maxormin=maxormin)

    # Check for an error, if it's a string we got an error so we return that
    if type(successflag) is str:
        return successflag
    # Otherwise we ask for the value and return the value we found
    if maxormin == "max":
        sleep(0.5)
        seekval = scope.ask_measval('1')
    elif maxormin == "min":
        sleep(0.5)
        seekval = scope.ask_measval('2')
    return seekval



'''
zoomonpeak takes 5 arguments
scope is a handle to the oscilloscope
afg is a handle to the function generator
feature is the feature it is looking for, max or min
channel is the oscilloscope channel
source is the function generator channel

zoomonpeak zooms in on the feature, in order to obtain the most accurate
possible reading for the value of the max or min value.
'''
def zoomonpeak(scope, afg, feature='max', channel=1, source=1):
    # Make sure that all we get back are numbers when we query things, to make things easier
    scope.write_headoff()
    # Turn off zoom state in case it is on (it probably isn't, but this ensures our assumptions
    # are okay)
    scope.write_zoomstate('1', 'off')

    # Nuke all the marks, because they will get in the way otherwise
    scope.write_deletemarks('all')

    # So the AFG has a delay it likes to add to the signal its putting in
    # Here we ask what it is, so we can account for it
    delay = afg.ask_leadedgetime(str(source))

    # Use the frequency to calculate the duration of the signal
    period = 1.0/afg.ask_freq(str(source))

    horzscales = [1.0e-9, 2.0e-9, 4.0e-9, 1.0e-8, 2.0e-8, 4.0e-8, 8.0e-8
                  , 2.0e-7, 4.0e-7, 1.0e-6, 2.0e-6, 4.0e-6, 1.0e-5, 2.0e-5]
    shiftbyh = 0.0
    # Test each horizontal scale to find what works the best for fitting the signal on screen
    # horizontally speaking
    for scale in horzscales:
        # If this passes, the signal will fit on the screen, with/out some shifting
        if period/scale < 10.0:
            horzscale = scale
            # If this passes, then the signal will fit on the screen, but we will have to
            # horizontally shift it in order to do so we ask ourselves, what is that shift?
            if period/scale > 5.0:
                # This comes from a reduction of the equation (period/scale - 5.0)*scale
                # I don't know why they are equivalent, but clearly they are
                # math: not even once.
                shiftbyh = period - 5.0*scale
                shiftbyh = shiftbyh + delay
            break
        else:
            # Assume a 'max' there are larger scales on some models, but for universality's sake
            horzscale = 2.0e-5
    # Write our newfound values to the scope!
    scope.write_horizontalscale(str(horzscale))
    scope.write_horizontaldelaytime(str(shiftbyh))

    # Call the onscreenbyscale function (see above) which puts the signal on screen
    onscreenflag = onscreenbyscale(scope, channel=channel)
    # If it returns a flag of non-1 it was unsuccessful, so we resort to extrememeasures
    if onscreenflag != 1:
        return extrememeasures(scope, channel=channel, maxormin=feature)

    # Set up depending on which feature we are interested in (max or min)
    if feature == 'max':
        scope.write_triggerriseorfall('rise')
        scope.write_meastype('1', 'max')
    elif feature == 'min':
        scope.write_triggerriseorfall('fall')
        scope.write_meastype('1', 'mini')
    # Turn the measurement on and set it read from the channel we are operating on
    scope.write_measstate('1', 'on')
    scope.write_meassource('1', str(channel))

    sleep(0.5)
    # Ask what it says the value is after letting it acquire
    peakval = scope.ask_measval('1')

    # === Search ===
    # Set search type to edge
    scope.write_triggertype('edge')
    # Set the search to trigger off of the channel we are operating on
    scope.write_triggeredgesource(str(channel))

    # Calculate the upper and lower thresholds, the search will return anything that lies
    # between these two values
    # Old method is shown below, new method is to do 90%, you can try either, but the one there works fine
    if peakval < 0.05:
        upperthres = peakval + (0.005*(peakval/abs(peakval)))
        lowerthres = peakval - (0.005*(peakval/abs(peakval)))
    elif peakval < 0.1:
        upperthres = peakval + (0.015*(peakval/abs(peakval)))
        lowerthres = peakval - (0.015*(peakval/abs(peakval)))
    else:
        upperthres = peakval*0.9
        lowerthres = peakval*0.9

    # Write these thresholds to the scope
    scope.write_threshold('upp', str(channel), str(upperthres))
    scope.write_threshold('low', str(channel), str(lowerthres))    

    # Activate Search and see how many we found
    scope.write_searchstate('1')
    # Give it time to find things, 0.1 is tested to work with one peak, 1 is arbitrarily longer
    sleep(1.0)
    totalmarks = scope.ask_searchtotal()

    scope.write_zoommode('on')

    # *** Not sure what to do here for now, going to assume it's always the first peak ***
    if int(totalmarks) > 1:
        # Focus in on the first mark, then we use it as a 
        # reference to find the one the user wishes to focus on
        for i in range(0, int(totalmarks)):
            scope.write_selectmark('prev')
        #***Currently this assumes the first peak is the most important***
        focusp = 1
        for j in range(0, int(focusp)-1):
            scope.write_selectmark('next')
    elif int(totalmarks) == 0:
        scope.write_zoommode('off')
        return 'Search did not return results'
    else:
        # We do both to guarantee we are on the mark
        scope.write_selectmark('next')
        scope.write_selectmark('prev')

    # Set the offset/scale correctly to get the most accurate value we can
    successflag = onscreenbyshift(scope, maxormin=feature, onscreen=1, channel=channel)
    # Check for an error
    if type(successflag) is str:
        return successflag
    # Set up the measurement according to what type we are looking for
    if feature == "max":
        scope.write_meastype('1', 'max')
    elif feature == "min":
        scope.write_meastype('1', 'mini')
    # Allow the scope time to acquire
    sleep(1.0)
    # Query for the value we are seeking
    # *clap* And ya done.
    return scope.ask_measval('1')

'''
ZOOM ON PEAK 2: ELECTRIC BOOGALOO
takes all the same arugments as zoomonpeak EXCEPT it adds vscale, and AWESOME way to 
force the program to try and use the same scale that worked before.
vscale will be stored in the .xlsx files under the scales column, and will be passed to this
function by reading it in from there
'''
def electricboogaloo(scope, afg, feature='max', channel=1, source=1, vscale=0.2):
    # Make sure that all we get back are numbers when we query things, to make things easier
    scope.write_headoff()
    # Turn off zoom state in case it is on (it probably isn't, but this ensures our assumptions
    # are okay)
    scope.write_zoomstate('1', 'off')

    # Nuke all the marks, because they will get in the way otherwise
    scope.write_deletemarks('all')

    # So the AFG has a delay it likes to add to the signal its putting in
    # Here we ask what it is, so we can account for it
    delay = afg.ask_leadedgetime(str(source))

    # Use the frequency to calculate the duration of the signal
    period = 1.0/afg.ask_freq(str(source))

    horzscales = [1.0e-9, 2.0e-9, 4.0e-9, 1.0e-8, 2.0e-8, 4.0e-8, 8.0e-8
                  , 2.0e-7, 4.0e-7, 1.0e-6, 2.0e-6, 4.0e-6, 1.0e-5, 2.0e-5]
    shiftbyh = 0.0
    # Test each horizontal scale to find what works the best for fitting the signal on screen
    # horizontally speaking
    for scale in horzscales:
        # If this passes, the signal will fit on the screen, with/out some shifting
        if period/scale < 10.0:
            horzscale = scale
            # If this passes, then the signal will fit on the screen, but we will have to
            # horizontally shift it in order to do so we ask ourselves, what is that shift?
            if period/scale > 5.0:
                # This comes from a reduction of the equation (period/scale - 5.0)*scale
                # I don't know why they are equivalent, but clearly they are
                # math: not even once.
                shiftbyh = period - 5.0*scale
                shiftbyh = shiftbyh + delay
            break
        else:
            # Assume a 'max' there are larger scales on some models, but for universality's sake
            horzscale = 2.0e-5
    scope.write_horizontalscale(str(horzscale))
    scope.write_horizontaldelaytime(str(shiftbyh))

    # Call the onscreenbyscale function (see above) which puts the signal on screen
    onscreenflag = onscreenbyscale(scope, channel=channel)
    if onscreenflag != 1:
        return extrememeasures(scope, channel=channel, maxormin=feature)

    # Set up depending on which feature we are interested in (max or min)
    if feature == 'max':
        scope.write_triggerriseorfall('rise')
        scope.write_meastype('1', 'max')
    elif feature == 'min':
        scope.write_triggerriseorfall('fall')
        scope.write_meastype('1', 'mini')
    # Turn the measurement on and set it ch1, which we currently assume is the channel to use
    scope.write_measstate('1', 'on')
    scope.write_meassource('1', str(channel))

    sleep(0.5)
    peakval = scope.ask_measval('1')

    # === Search ===
    # Set search type to edge
    scope.write_triggertype('edge')
    scope.write_triggeredgesource(str(channel))

    # Calculate the upper and lower thresholds, the search will return anything that lies
    # between these two values
    upperthres = peakval*0.9
    lowerthres = peakval*0.9

    # Write these thresholds to the scope
    scope.write_threshold('upp', str(channel), str(upperthres))
    scope.write_threshold('low', str(channel), str(lowerthres))

    # Activate Search and see how many we found
    scope.write_searchstate('1')
    # Give it time to find things, 0.1 is tested to work with one peak, 1 is arbitrarily longer
    sleep(1.0)
    totalmarks = scope.ask_searchtotal()

    scope.write_zoommode('on')

    # *** Not sure what to do here for now, going to assume it's always the first peak ***
    if int(totalmarks) > 1:
        # Focus in on the first mark, then we use it as a 
        # reference to find the one the user wishes to focus on
        for i in range(0, int(totalmarks)):
            scope.write_selectmark('prev')
        #***Currently this assumes the first peak is the most important***
        focusp = 1
        for j in range(0, int(focusp)-1):
            scope.write_selectmark('next')
    elif int(totalmarks) == 0:
        scope.write_zoommode('off')
        return 'Search did not return results'
    else:
        # We do both to guarantee we are on the mark
        scope.write_selectmark('next')
        scope.write_selectmark('prev')

    scope.write_scale(str(channel), str(vscale))

    # At this scale the offset is anything less than 1.0 V, so we just set it right to the signal amplitude
    if vscale == 0.001:
        scope.write_offset(str(channel), str(peakval))
    # Because of the different handling for scales here, we compress can compress the switch statement
    # Here there is a range where all we do is set the offset to maximum possible in one direction or the other
    elif vscale > 0.001 and vscale < 0.1:
        scope.write_offset(str(channel), str(1.0*(peakval/abs(peakval))))
    # Another tier where the scope has different limits to offset, after this we have to go by the slower method
    elif vscale == 0.1:
        scope.write_offset(str(channel), str(peakval))
    # If nothing worked at this point, we have to do the slow method, which is to increment
    # offset from square 1.
 
    successflag = onscreenbyshift2(scope, channel=channel, maxormin=feature)

    if type(successflag) is str:
        return successflag

    if feature == "max":
        scope.write_meastype('1', 'max')
    elif feature == "min":
        scope.write_meastype('1', 'mini')
    # Allow the scope time to acquire
    sleep(1.0)
    # Query for the value we are seeking
    # *clap* And ya done.
    return scope.ask_measval('1')

