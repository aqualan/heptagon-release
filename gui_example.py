import sys, visa
from PySide.QtCore import *
from PySide.QtGui import *
from export_excel_data import write_file as wxlf
import numpy as np
from struct import unpack

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4']='PySide'
import pylab
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from readfromfile import importdata

from settings import settingsTab

#initializing the application and resource manager
qt_app = QApplication(sys.argv)
rm = visa.ResourceManager()

class sillyscope:
    '''A virtual oscilloscope.'''

    def __init__(self, scope_name):

        #initializing scope
        self.communicator = rm.get_instrument(scope_name)
        self.communicator.write('HEAD OFF')

        #attributes of the scope
        self.communicator.write('CH1:TER?')
        self.impedance = self.communicator.read().encode('ascii', 'ignore')
        if int(self.impedance[0]) == 1:
            self.impedance = 'MEG'
        else:
            self.impedance = '50.0E+0'

        self.communicator.write('CH2:TER?')
        self.impedance2 = self.communicator.read().encode('ascii', 'ignore')
        if int(self.impedance2[0]) == 1:
            self.impedance2 = 'MEG'
        else:
            self.impedance2 = '50.0E+0'

        self.communicator.write('CH3:TER?')
        self.impedance3 = self.communicator.read().encode('ascii', 'ignore')
        if int(self.impedance3[0]) == 1:
            self.impedance3 = 'MEG'
        else:
            self.impedance3 = '50.0E+0'

        self.communicator.write('CH4:TER?')
        self.impedance4 = self.communicator.read().encode('ascii', 'ignore')
        if int(self.impedance4[0]) == 1:
            self.impedance4 = 'MEG'
        else:
            self.impedance4 = '50.0E+0'
        

        self.current_waveform = []
        self.waveform_time = []

        self.communicator.write('SELECT:CH1?')
        if int(self.communicator.read()) == 1:
            self.ch1_state = 'ON'
        else:
            self.ch1_state = 'OFF'

        self.communicator.write('SELECT:CH2?')
        if int(self.communicator.read()) == 1:
            self.ch2_state = 'ON'
        else:
            self.ch2_state = 'OFF'

        self.communicator.write('SELECT:CH3?')
        if int(self.communicator.read()) == 1:
            self.ch3_state = 'ON'
        else:
            self.ch3_state = 'OFF'

        self.communicator.write('SELECT:CH4?')
        if int(self.communicator.read()) == 1:
            self.ch4_state = 'ON'
        else:
            self.ch4_state = 'OFF'

        #variables for adjusting attributes of the scope
        self.imp_store = self.impedance
        self.imp_store2 = self.impedance2
        self.imp_store3 = self.impedance3
        self.imp_store4 = self.impedance4
        self.ch1_state_store = self.ch1_state
        self.ch2_state_store = self.ch2_state
        self.ch3_state_store = self.ch3_state
        self.ch4_state_store = self.ch4_state

class OscilloscopeController(QWidget):
    '''A controller for a Tektronix Oscilloscope.'''

    def __init__(self):

        #initializing the application
        QWidget.__init__(self)
        self.setWindowTitle("Tektronix Oscilloscope Controller")
        self.setMinimumSize(QSize(1200, 680))

        #---FIRST TAB: Oscilloscope Control---#
        
        #writing title at top of tab
        self.title = QLabel('Tekronix Oscilloscope Controller')

        #group box for holding device select
        self.deviceGroupBox = QGroupBox('Select Device')
        self.deviceLayout = QFormLayout()

        #button for updating entries of combo_box
        self.update_devices = QPushButton('Update device list...')
        self.update_devices.clicked.connect(self.up_devices)

        #combo box for choosing device
        self.choose_device = QComboBox(self)

        #button for connecting/disconnecting device
        self.silly_connect = QPushButton('Connect')
        self.silly_connect.clicked.connect(self.connect_oscilloscope)
        self.silly_connect.setDisabled(1)
        self.silly_disconnect = QPushButton('Disconnect')
        self.silly_disconnect.clicked.connect(self.disconnect_oscilloscope)

        #adding stuff
        self.deviceLayout.addRow(self.update_devices)
        self.deviceLayout.addRow('Choose Oscilloscope: ', self.choose_device)
        self.deviceLayout.addRow(self.silly_connect, self.silly_disconnect)
        self.deviceGroupBox.setLayout(self.deviceLayout)
        
        #group box for holding channel 1 impedance switch
        self.groupBox = QGroupBox('Set Channel 1 Impedance', self)
        self.groupBox.resize(150, 75)

        #radio buttons placed in group box
        self.radio1 = QRadioButton('50 Ohms', self)
        self.radio1.toggled.connect(self.set_imp_50)
        self.radio2 = QRadioButton('1M Ohms', self)
        self.radio2.toggled.connect(self.set_imp_1M)

        #button for turning channel 1 on/off
        self.ch1_on_off = QCheckBox('Channel 1 On/Off', self)
        self.ch1_on_off.stateChanged.connect(self.channel1_on_button)

        #adding/formatting radio buttons into group box
        self.vbox = QVBoxLayout()
        self.vbox.addWidget(self.ch1_on_off)
        self.vbox.addWidget(self.radio1)
        self.vbox.addWidget(self.radio2)
        self.vbox.addStretch(1)
        self.groupBox.setLayout(self.vbox)

        #group box number 2
        self.groupBox2 = QGroupBox('Set Channel 2 Impedance', self)
        self.groupBox.resize(150, 75)

        #radio buttons placed in second group box
        self.radio2_50 = QRadioButton('50 Ohms', self)
        self.radio2_50.toggled.connect(self.set_imp_2_50)
        self.radio2_MEG = QRadioButton('1M Ohms', self)
        self.radio2_MEG.toggled.connect(self.set_imp_2_MEG)

        #button for turning channel 2 on/off
        self.ch2_on_off = QCheckBox('Channel 2 On/Off', self)
        self.ch2_on_off.stateChanged.connect(self.channel2_on_button)

        #adding/formatting radio buttons into group box 2
        self.vbox2 = QVBoxLayout()
        self.vbox2.addWidget(self.ch2_on_off)
        self.vbox2.addWidget(self.radio2_50)
        self.vbox2.addWidget(self.radio2_MEG)
        self.vbox2.addStretch(1)
        self.groupBox2.setLayout(self.vbox2)

        #group box number 3
        self.groupBox3 = QGroupBox('Set Channel 3 Impedance', self)
        self.groupBox.resize(150, 75)

        #radio buttons placed in third group box
        self.radio3_50 = QRadioButton('50 Ohms', self)
        self.radio3_50.toggled.connect(self.set_imp_3_50)
        self.radio3_MEG = QRadioButton('1M Ohms', self)
        self.radio3_MEG.toggled.connect(self.set_imp_3_MEG)

        #button for turning channel 3 on/off
        self.ch3_on_off = QCheckBox('Channel 3 On/Off', self)
        self.ch3_on_off.stateChanged.connect(self.channel3_on_button)

        #adding/formatting radio buttons into group box 3
        self.vbox3 = QVBoxLayout()
        self.vbox3.addWidget(self.ch3_on_off)
        self.vbox3.addWidget(self.radio3_50)
        self.vbox3.addWidget(self.radio3_MEG)
        self.vbox3.addStretch(1)
        self.groupBox3.setLayout(self.vbox3)

        #group box number 4
        self.groupBox4 = QGroupBox('Set Channel 4 Impedance', self)
        self.groupBox.resize(150, 75)

        #radio buttons placed in fourth group box
        self.radio4_50 = QRadioButton('50 Ohms', self)
        self.radio4_50.toggled.connect(self.set_imp_4_50)
        self.radio4_MEG = QRadioButton('1M Ohms', self)
        self.radio4_MEG.toggled.connect(self.set_imp_4_MEG)

        #button for turning channel 4 on/off
        self.ch4_on_off = QCheckBox('Channel 4 On/Off', self)
        self.ch4_on_off.stateChanged.connect(self.channel4_on_button)

        #adding/formatting radio buttons into group box 4
        self.vbox4 = QVBoxLayout()
        self.vbox4.addWidget(self.ch4_on_off)
        self.vbox4.addWidget(self.radio4_50)
        self.vbox4.addWidget(self.radio4_MEG)
        self.vbox4.addStretch(1)
        self.groupBox4.setLayout(self.vbox4)

        #button for setting the scope to the user input
        self.set_button = QPushButton('Set Scope', self)
        self.set_button.clicked.connect(self.set_scope)

        #formatting the setup button so it can be added
        self.button_box = QHBoxLayout()
        self.button_box.addStretch(1)
        self.button_box.addWidget(self.set_button)

        #disabling functionality until a device has been connected
        self.disconnect_oscilloscope()

        #---SECOND TAB: Waveform Generator Tab--#

        #buttons for loading data sets
        self.load_button_1 = QPushButton('Load Waveform 1')
        self.load_button_1.clicked.connect(self.load_wave_1)
        self.load_button_2 = QPushButton('Load Waveform 2')
        #self.load_button_2.clicked.connect(self.load_wave_2)

        #tabs for holding data sets
        self.wave_tab1 = QTabWidget()
        self.wave_tab1.setDisabled(1)
        self.wave_tab2 = QTabWidget()
        self.wave_tab2.setDisabled(1)
        
        #table for holding data points
        self.data_table1 = QTableWidget()
        self.data_table1.setRowCount(10)
        self.data_table1.setColumnCount(2)
        self.data_table1.setHorizontalHeaderLabels(('Time (s)', 'Voltage'))
        self.data_table1.resizeColumnsToContents()

        self.data_table2 = QTableWidget()
        self.data_table2.setRowCount(10)
        self.data_table2.setColumnCount(2)
        self.data_table2.setHorizontalHeaderLabels(('Time (s)', 'Voltage'))
        self.data_table2.resizeColumnsToContents()

        #plots for displaying waveforms
        self.wave1Frame = QWidget()
        self.wave1Frame.resize(0.1,0.1)
        self.wave1Figure = Figure(figsize=(12, 3), dpi = 100, facecolor='white')
        self.wave1Canvas = FigureCanvas(self.wave1Figure)
        self.wave1Canvas.setGeometry(QRect(500, 500, 1200, 300))
        self.wave1Canvas.setParent(self.wave1Frame)
        self.wave1Axes = self.wave1Figure.add_subplot(111, axis_bgcolor='black')
        self.wave1Axes.plot([])
        self.wave1Figure.tight_layout()
        self.wave1Axes.grid(True, color='green', linewidth=2)
        self.wave1Vbox = QVBoxLayout()
        self.wave1Vbox.addWidget(self.wave1Canvas)
        self.wave1Frame.setLayout(self.wave1Vbox)
                
        self.wave2Frame = QWidget()
        self.wave2Figure = Figure(figsize=(0.1, 0.1))
        self.wave2Canvas = FigureCanvas(self.wave2Figure)
        self.wave2Canvas.setParent(self.wave2Frame)
        self.wave2Axes = self.wave2Figure.add_subplot(111, axis_bgcolor='black')
        self.wave2Axes.plot([])
        self.wave2Axes.grid(True, color='green', linewidth=2)
        self.wave2Vbox = QVBoxLayout()
        self.wave2Vbox.addWidget(self.wave2Canvas)
        self.wave2Frame.setLayout(self.wave2Vbox)

        #buttons for sending waveforms to generator
        self.send_waveform_button_1 = QPushButton('Send Waveform 1')
        #self.send_waveform_button_1.clicked.connect(self.send_wave_1)
        self.send_waveform_button_2 = QPushButton('Send Waveform 2')
        #self.send_waveform_button_2.clicked.connect(self.send_wave_2)
        
        #--THIRD TAB: Export Data Tab---#
        
        #writing title at top of tab
        self.data_label = QLabel('Data')

        #button for running data collection
        self.data_button = QPushButton('Retrieve Data', self)
        self.data_button.clicked.connect(self.collect_data)

        #export title
        self.export_title = QLabel('Export Data to Excel')

        #create layout for setting export title
        self.export_layout = QFormLayout()

        #box for naming file
        self.excel_title = QLineEdit(self)
        self.export_layout.addRow('Title: ', self.excel_title)
        
        #button for exporting the data
        self.export_button = QPushButton('Export Data...', self)
        self.export_button.clicked.connect(self.export_data)

        #---ADDING TABS TO APPLICATION---#

        #tab for oscilloscope
        self.silly_tab = QWidget()
        self.silly_layout = QVBoxLayout()
        self.silly_layout.addStretch(1)
        self.silly_layout.addWidget(self.title)
        self.silly_layout.addWidget(self.deviceGroupBox)
        self.silly_layout.addWidget(self.groupBox)
        self.silly_layout.addWidget(self.groupBox2)
        self.silly_layout.addWidget(self.groupBox3)
        self.silly_layout.addWidget(self.groupBox4)
        self.silly_layout.addLayout(self.button_box)
        self.silly_tab.setLayout(self.silly_layout)

        #tab for data
        self.data_tab = QWidget()
        self.data_layout = QVBoxLayout()
        self.data_layout.addStretch(1)
        self.data_layout.addWidget(self.data_label)
        self.data_layout.addWidget(self.data_button)
        self.data_layout.addLayout(self.export_layout)
        self.data_layout.addWidget(self.export_button)
        self.data_tab.setLayout(self.data_layout)

        #tab for settings
        self.settingsTab = settingsTab()

        #tab for waveform generator
        self.generator_tab = QWidget()
        self.generator_layout = QVBoxLayout()
        
        self.generator_load_layout = QHBoxLayout()
        self.generator_load_layout.addWidget(self.load_button_1)
        self.generator_load_layout.addWidget(self.load_button_2)
        self.generator_layout.addLayout(self.generator_load_layout)

        self.generator_tab_bar = QTabWidget()
        self.wave_tab1_layout = QHBoxLayout()
        self.wave_tab1_layout.addWidget(self.data_table1)
        self.wave_tab1_layout.addWidget(self.wave1Frame)
        self.wave_tab1.setLayout(self.wave_tab1_layout)
        self.generator_tab_bar.addTab(self.wave_tab1, 'Waveform 1')

        self.wave_tab2_layout = QHBoxLayout()
        self.wave_tab2_layout.addWidget(self.data_table2)
        self.wave_tab2_layout.addWidget(self.wave2Frame)
        self.wave_tab2.setLayout(self.wave_tab2_layout)
        self.generator_tab_bar.addTab(self.wave_tab2, 'Waveform 2')
        
        self.generator_layout.addWidget(self.generator_tab_bar)
        self.generator_tab.setLayout(self.generator_layout)

        #tab bar for switching between devices/stuff
        self.tabBar = QTabWidget()
        self.tabBar.addTab(self.silly_tab, 'Oscilloscope')
        self.tabBar.addTab(self.generator_tab, 'Waveform Generator')
        self.tabBar.addTab(self.data_tab, 'Data')
        self.tabBar.addTab(self.settingsTab, 'Settings')

        #adding tab bar
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tabBar)
        self.setLayout(self.layout)
        
        
    #functions
    @Slot()
    def up_devices(self):

        self.choose_device.clear()

        try:
            insts = visa.get_instruments_list()
            self.instrument_list = []
            self.instrument_names = []

            for i in range(len(insts)):
                self.instrument_list.append(insts[i].encode('ascii', 'ignore'))
                rm.get_instrument(self.instrument_list[i]).write('*IDN?')
                name = rm.get_instrument(self.instrument_list[i]).read()
                self.instrument_names.append(name[:18])

            self.choose_device.addItems(self.instrument_names)
            self.silly_connect.setEnabled(1)

        except:
            self.silly_connect.setDisabled(1)
            self.io_error_window = QErrorMessage()
            self.io_error_window.showMessage('There are no devices connected to the ' + 
                                             'computer. Please make sure the oscilloscope ' + 
                                             'is connected and try again.')

    @Slot()
    def connect_oscilloscope(self):
        #building oscilloscope
        self.silly_name = self.instrument_list[self.choose_device.currentIndex()]
        self.scope = sillyscope(self.silly_name)

        #enabling functionality
        self.groupBox.setEnabled(1)
        self.groupBox2.setEnabled(1)
        self.groupBox3.setEnabled(1)
        self.groupBox4.setEnabled(1)
        self.set_button.setEnabled(1)
        self.silly_disconnect.setEnabled(1)

        #setting channel 1 settings
        if self.scope.impedance == 'MEG':
            self.radio2.toggle()
        else:
            self.radio1.toggle()
        if self.scope.ch1_state == 'ON':
            self.ch1_on_off.toggle()
            self.radio1.setEnabled(1)
            self.radio2.setEnabled(1)
        else:
            self.radio1.setDisabled(1)
            self.radio2.setDisabled(1)

        #setting channel 2 settings
        if self.scope.impedance2 == 'MEG':
            self.radio2_MEG.toggle()
        else:
            self.radio2_50.toggle()

        if self.scope.ch2_state == 'ON':
            self.ch2_on_off.toggle()
            self.radio2_50.setEnabled(1)
            self.radio2_MEG.setEnabled(1)
        else:
            self.radio2_50.setDisabled(1)
            self.radio2_MEG.setDisabled(1)

        #setting channel 3 settings
        if self.scope.impedance3 == 'MEG':
            self.radio3_MEG.toggle()
        else:
            self.radio3_50.toggle()

        if self.scope.ch3_state == 'ON':
            self.ch3_on_off.toggle()
            self.radio3_50.setEnabled(1)
            self.radio3_MEG.setEnabled(1)
        else:
            self.radio3_50.setDisabled(1)
            self.radio3_MEG.setDisabled(1)
            
        #setting channel 4 settings
        if self.scope.impedance4 == 'MEG':
            self.radio4_MEG.toggle()
        else:
            self.radio4_50.toggle()

        if self.scope.ch4_state == 'ON':
            self.ch4_on_off.toggle()
            self.radio4_50.setEnabled(1)
            self.radio4_MEG.setEnabled(1)
        else:
            self.radio4_50.setDisabled(1)
            self.radio4_MEG.setDisabled(1)

        while True:
            QApplication.processEvents()
            try: self.connect_state = visa.get_instruments_list().index(self.silly_name) + 1
            except: self.connect_state = 0
            if not self.connect_state:
                break

        self.disconnect_oscilloscope()
        self.up_devices()

    @Slot()
    def disconnect_oscilloscope(self):
        self.groupBox.setDisabled(1)
        self.groupBox2.setDisabled(1)
        self.groupBox3.setDisabled(1)
        self.groupBox4.setDisabled(1)
        self.silly_disconnect.setDisabled(1)
        self.set_button.setDisabled(1)

    @Slot()
    def set_scope(self):
        self.scope.communicator.write('CH1:TER ' + self.scope.imp_store)
        self.scope.impedance = self.scope.communicator.write('CH1:TER?')
        self.scope.communicator.write('CH2:TER ' + self.scope.imp_store2)
        self.scope.impedance2 = self.scope.communicator.write('CH2:TER?')
        self.scope.communicator.write('CH3:TER ' + self.scope.imp_store3)
        self.scope.impedance3 = self.scope.communicator.write('CH3:TER?')
        self.scope.communicator.write('CH4:TER ' + self.scope.imp_store4)
        self.scope.impedance4 = self.scope.communicator.write('CH4:TER?')

        self.scope.ch1_state = self.scope.communicator.write(
            'SELECT:CH1 ' + self.scope.ch1_state_store)
        self.scope.ch2_state = self.scope.communicator.write(
            'SELECT:CH2 ' + self.scope.ch2_state_store)
        self.scope.ch3_state = self.scope.communicator.write(
            'SELECT:CH3 ' + self.scope.ch3_state_store)
        self.scope.ch4_state = self.scope.communicator.write(
            'SELECT:CH4 ' + self.scope.ch4_state_store)

    @Slot()
    def set_imp_50(self):
        self.scope.imp_store = '50.0E+0'

    @Slot()
    def set_imp_1M(self):
        self.scope.imp_store = 'MEG'

    @Slot()
    def set_imp_2_50(self):
        self.scope.imp_store2 = '50.0E+0'

    @Slot()
    def set_imp_2_MEG(self):
        self.scope.imp_store2 = 'MEG'

    @Slot()
    def set_imp_3_50(self):
        self.scope.imp_store3 = '50.0E+0'

    @Slot()
    def set_imp_3_MEG(self):
        self.scope.imp_store3 = 'MEG'

    @Slot()
    def set_imp_4_50(self):
        self.scope.imp_store4 = '50.0E+0'

    @Slot()
    def set_imp_4_MEG(self):
        self.scope.imp_store4 = 'MEG'

    @Slot()
    def channel1_on_button(self, state):
        if state == Qt.Checked:
            self.scope.ch1_state_store = 'ON'
            self.radio1.setEnabled(1)
            self.radio2.setEnabled(1)
        else:
            self.scope.ch1_state_store = 'OFF'
            self.radio1.setDisabled(1)
            self.radio2.setDisabled(1)

    @Slot()
    def channel2_on_button(self, state):
        if state == Qt.Checked:
            self.scope.ch2_state_store = 'ON'
            self.radio2_50.setEnabled(1)
            self.radio2_MEG.setEnabled(1)
        else:
            self.scope.ch2_state_store = 'OFF'
            self.radio2_50.setDisabled(1)
            self.radio2_MEG.setDisabled(1)

    @Slot()
    def channel3_on_button(self, state):
        if state == Qt.Checked:
            self.scope.ch3_state_store = 'ON'
            self.radio3_50.setEnabled(1)
            self.radio3_MEG.setEnabled(1)
        else:
            self.scope.ch3_state_store = 'OFF'
            self.radio3_50.setDisabled(1)
            self.radio3_MEG.setDisabled(1)

    @Slot()
    def channel4_on_button(self, state):
        if state == Qt.Checked:
            self.scope.ch4_state_store = 'ON'
            self.radio4_50.setEnabled(1)
            self.radio4_MEG.setEnabled(1)
        else:
            self.scope.ch4_state_store = 'OFF'
            self.radio4_50.setDisabled(1)
            self.radio4_MEG.setDisabled(1)

    @Slot()
    def load_wave_1(self):
        self.choose_load_wave_1 = QFileDialog()
        self.choose_load_wave_1.setDefaultSuffix(unicode('txt'))
        if self.choose_load_wave_1.exec_():
            self.wave_tab1.setEnabled(1)
            self.load_wave1_name = self.choose_load_wave_1.selectedFiles()
            self.load_wave1_name = self.load_wave1_name[0]
            self.load_wave1_name.encode('ascii', 'ignore')
            self.waveform1_data = importdata(self.load_wave1_name)
            self.data_table1.clearContents()
            self.data_table1.setRowCount(len(self.waveform1_data['refform']))
            for i in range(len(self.waveform1_data['refform'])):
                table_item = QTableWidgetItem(str(self.waveform1_data['timevals'][i]))
                self.data_table1.setItem(i, 0, table_item)
                table_item = QTableWidgetItem(str(self.waveform1_data['refform'][i]))
                self.data_table1.setItem(i, 1, table_item)
            self.wave1Axes.clear()
            self.wave1Axes.plot(self.waveform1_data['timevals'], self.waveform1_data['refform'], 'r')
            self.wave1Axes.grid(True, color='green', linewidth=2)
            self.wave1Canvas.draw()
            self.data_table1.resizeColumnsToContents()


    @Slot()
    def collect_data(self):
        self.scope.communicator.write('DATA:SOU CH1')
        self.scope.communicator.write('DATA:WIDTH 1')
        self.scope.communicator.write('DATA:ENC RPB')

        ymult = float(self.scope.communicator.ask('WFMPRE:YMULT?'))
        yzero = float(self.scope.communicator.ask('WFMPRE:YZERO?'))
        yoff = float(self.scope.communicator.ask('WFMPRE:YOFF?'))
        xincr = float(self.scope.communicator.ask('WFMPRE:XINCR?'))
        self.scope.communicator.write('HEAD ON')

        self.scope.communicator.write('CURVE?')
        data = self.scope.communicator.read_raw()
        headerlen = 2 + int(data[7])
        header = data[:headerlen]
        ADC_wave = data[headerlen:-1]

        ADC_wave = np.array(unpack('%sB' % len(ADC_wave),ADC_wave))

        self.scope.current_waveform = (ADC_wave - yoff) * ymult  + yzero
        self.scope.waveform_time = np.arange(0, xincr * len(self.scope.current_waveform), xincr)
        
    @Slot()
    def export_data(self):
        self.choose_save = QFileDialog()
        self.choose_save.setAcceptMode(QFileDialog.AcceptSave)
        self.choose_save.setDefaultSuffix(unicode('xls'))
        if self.choose_save.exec_():
            self.excel_filename = self.choose_save.selectedFiles()
            self.excel_filename = self.excel_filename[0]
            self.excel_filename.encode('ascii', 'ignore')
            wxlf(self.scope.current_waveform, self.scope.waveform_time,
                 self.excel_title.text(), self.excel_filename)
    
    def run(self):
        self.show()
        qt_app.exec_()


#running the application
app = OscilloscopeController()
app.run()
