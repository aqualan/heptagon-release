'''
Tests one grouping from the analog mezzanine board.

The oscilloscope must be configured as the following:
Channel 1 - negative ADC differential output from AMB testing board
Channel 2 - positive ADC differential output from AMB testing board
Channel 3 - summing output from AMB testing board

The following measurement channels are used:
1: Analog Channel 1, input
2: Analog Channel 2, input
3: Analog Channel 3, input
4: Analog Channel 4, input
5: Reference Channel 1, input
6: Reference Channel 2, input
7: Reference Channel 3, input
8: Reference Channel 4, input

Usage: 	Execute directly from command line with `python AMBTest.py`. Make sure 
		that you are plugged into the oscilloscope via USB, and that the
		scopedescriptor constant is correct

Arguments: 	None

'''

import visa
import collections
import AMBOscilloscopeSetup as init
import time

scope = None

# New data type to return on measurement checks
waveform = collections.namedtuple("Waveform", ["riseTime", "peakAmp", "phase"])

### ----- HELPER METHODS ----- ###

# Load references from ASCII to the oscilloscope
def loadRefs(scope, refnum, data):
	# Specify which reference to load to
	scope.set_datadestination(self, refnum)

	# Specify length of waveform in # of points
	nr_pts = 0 #TODO
	scope.set_recordlength(self, nr_pts)

	# Specify data format (ASCII/Binary)
	format = "Something" #TODO
	scope.set_dataformat(self, format)

	# Specify number of bytes per data point
	bytes_per_point = 0 #TODO
	scope.set_numberofbytes(self, bytes_per_point)

	# Specify starting point
	starting_point = 0 #TODO
	scope.set_startingpoint(self, starting_point)

	# Transfer
	scope.transferdata(self, data)

# Asks whether or not the current references are the right ones to use.
# NOTE: Irrelevant with the GUI, as the user will decide what is correct.
def checkRefs():
	while(True):
		# TODO : STILL CLI; CONVERT TO GUIz
		userinput = raw_input("Take a look at the reference waveforms on the" + 
							  " scope. Are they correct? (Y\N)\n>>> ").lower()
		if (userinput == 'y' or userinput == "yes"):
			break
		elif (userinput == 'n' or userinput == "no"):
			raw_input("Please set the references accordingly, and then press" +
					  " any key to continue. ")
			break
		else:
			print("Input not recognized. Please try again.")

# Gets pertinent wave information - rise time, amplitude, and phase
def measureWave(scope, channel, source, DELTAREF=10e-3, DELAY=1):
	print("\nMeasuring parameters for Meas%d, waveform %s." %(channel, source))
	# Set up the scope
	scope.set_gatingscreen(self)
	scope.set_measurementstate(self, channel, "on")
	scope.set_measurementtype(self, channel, "rise")
	scope.set_measurementsource(self, channel, source)

	# Get minimum value
	abslow = scope.ask_measurementvalue(self, channel, "mini", DELAY)
	print("Got a minimum value on %s of %s" %(source, abslow))
	
	# Get maximum value
	abshigh = scope.ask_measurementvalue(self, channel, "max", DELAY)
	print("Got a maximum value on %s of %s" %(source, abshigh))
	
	# Add leeway to each
	abshigh -= DELTAREF
	abslow  += DELTAREF
	print("After adding deltas, max and min are %s and %s, respectively." 
		  %(abshigh, abslow))

	# Set reference levels
	scope.set_reflevels(self, abslow, abshigh, DELAY)

	risetime = scope.ask_measurementvalue(self, channel, "rise", DELAY)
	amplitude = scope.ask_measurementvalue(self, channel, "amplitude", DELAY)

	if (channel == 5 or channel == 6 or channel == 7 or channel == 8):
		# Reference waveforms have no phase to compare against
		w = waveform(risetime, amplitude, 0)
	elif (channel == 1 or channel == 2 or channel == 3 or channel == 4):
		# Whereas the input waveforms can compare against reference; 
		# all channels are 4 less than their corresponding waveform 
		phase = ask_measurementvalue(self, channel, "phase", DELAY)
		w = waveform(risetime, amplitude, phase)

	# Print out values to see on GUI
	print("Risetime: %s // Amplitude: %s" 
		  %(risetime, amplitude))
	return w

# Checks an input wave's measurements against its reference
def checkParams(scope, chID, channel, reference, AMPTOLERANCE=0.05,
	RISETOLERANCE=0.05, PHASETOLDERANCE=5e-3):
	upperBoundRiseTime = reference.riseTime * (1 + RISETOLERANCE)
	lowerBoundRiseTime = reference.riseTime * (1 - RISETOLERANCE)
	upperBoundPeakAmplitude = reference.peakAmp * (1 + AMPTOLERANCE)
	lowerBoundPeakAmplitude = reference.peakAmp * (1 - AMPTOLERANCE)
	upperBoundPhase = 360 * PHASETOLERANCE * 1
	lowerBoundPhase = 360 * PHASETOLERANCE * -1

	# Conditional booleans
	riseGood = False
	ampGood = False
	phaseGood = False

	while (not riseGood or not ampGood or not phaseGood):
		# Rise time checks
		if (channel.riseTime >= upperBoundRiseTime):
			pass
		elif (channel.riseTime <= lowerBoundRiseTime):
			pass
		else:
			print("Ch%d Rise Time : %f/%f" 
				  %(chID, channel.riseTime, reference.riseTime))
			riseGood = True

		# Amplitude checks
		if (channel.peakAmp >= upperBoundPeakAmplitude):
			pass
		elif (channel.peakAmp <= lowerBoundPeakAmplitude):
			pass
		else:
			print("Ch%d Amplitude : %f/%f" 
				  %(chID, channel.peakAmp, reference.peakAmp))
			ampGood = True

		# Phase checks
		if (channel.phase >= upperBoundPhase):
			pass
		elif (channel.phase <= lowerBoundPhase):
			pass
		else:
			print("Ch%d Phase Difference : %f" 
				  %(chID, channel.phase))
			phaseGood = True

		# Notify user of any failures
		if (not riseGood or not ampGood or not phaseGood):
			print("\nTesting failed on the following attributes for channel %d:" 
				  %(chID))
			if (not riseGood):
				print("Rise time; was %s but expected %s +/- %s" 
					  %(channel.riseTime, reference.riseTime, 
					  	RISETOLERANCE*reference.riseTime))
			if (not ampGood):
				print("Peak amplitude; was %s but expected %s +/- %s" 
					  %(channel.peakAmp, reference.peakAmp, 
					  	AMPTOLERANCE*reference.peakAmp))
			if (not phaseGood):
				print("Phase; was %s but expected %s +/- %s" 
					  %(channel.phase, reference.phase, 
					  	PHASETOLERANCE*reference.phase))
			
			shouldRetest = respondToFailure(chID)
			# If the following statement is not satisfied, then we will stay in 
			# the while loop and retest the channel parameters
			if (shouldRetest == False):
				break
		else:
			print("All attributes on this channel check out! Continuing onwards.")

# Prompt the user for input after a channel fails
def respondToFailure(chID):
	while (True):
		failureResponse = raw_input("Please verify what you see on the screen." + 
									" Enter 's' to enter scope mode for manual" + 
									" control, or enter 'O' to override. \n>>> ")
		if (failureResponse == "s"):
			scopeMode(chID)
			return True
		elif (failureResponse == "O"):
			print("\nChannel %d discrepancy overlooked; continuing test.\n" %(chID))
			return False
		else:
			print("Invalid response; please try again.")

# Detect if a user input signifies a quit
def quitOn(userinput):
	if (userinput == 'q' or userinput == 'Q'):
		return True
	else:
		return False

# Turn off all measurements and re-enable the scope so that the user can manually debug
def scopeMode(chID):
	print("\nRelinquishing control to user.\n")
	# Turn off all measurements
	for channel in range (1, 9):
		scope.set_measurementstate(self, channel, "off")
	# Turn Channel 4 (presumed probe) on
	scope.write_channelstate(self, 4, "on")

	# TODO: GUI-IFY
	userinput = ""
	while (not quitOn(userinput)):
		userinput = raw_input("Do your thing. Enter any key to quit. ")
		# Do things until user presses a key

	# Turn off Channel 4
	scope.set_channelstate(self, 4, "off")
	# Return automatic control
	print("\nAssuming DIRECT control. Automatically retesting problem channel.\n")
	# Turn all measurements back on.
	for channel in range (1, 9):
		scope.write_measurementstate(self, channel, "on")
		