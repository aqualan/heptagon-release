#!/use/bin/env python
'''
autoTab.py

Function:           Holds the tab that runs the auto test.

Classes:            autoTab, multiGroupBox
'''

from PySide.QtCore import(Qt, Slot)
from PySide.QtGui import(QWidget, QGroupBox, QFormLayout, QCheckBox, QComboBox, QLineEdit, QTextEdit, QPushButton, QHBoxLayout, QVBoxLayout, QTextCursor, QColor, QLabel)

from waveform_preview import waveformPreview
from AutoTest import testThread

class autoTab(QWidget):

    '''
    autoTab is the class that generates the auto test tab.

    Functions: enable_disable_ref, check_setup, multi_test, run_multi_test, new_message, enable_test, quit_test
    '''
    kind = 'test'

    def __init__(self, parent=None):

        QWidget.__init__(self, parent)

        #Generating the setup box
        
        self.setupBox = QGroupBox('Test Setup')
        self.setupLayout = QFormLayout()

        #Channel selection boxes
        
        self.selectCh1 = QCheckBox()
        self.selectCh1.chNum = '1'
        self.selectCh1.toggled.connect(self.enable_disable_ref)
        self.selectCh2 = QCheckBox()
        self.selectCh2.chNum = '2'
        self.selectCh2.toggled.connect(self.enable_disable_ref)
        self.selectCh3 = QCheckBox()
        self.selectCh3.chNum = '3'
        self.selectCh3.toggled.connect(self.enable_disable_ref)
        self.selectCh4 = QCheckBox()
        self.selectCh4.chNum = '4'
        self.selectCh4.toggled.connect(self.enable_disable_ref)
        self.selectChMath = QCheckBox()
        self.selectChMath.chNum = 'Math'
        self.selectChMath.toggled.connect(self.enable_disable_ref)

        #Boxes for selecting the reference wave that each channel is compared to
        reflist = ['Reference 1', 'Reference 2', 'Reference 3', 'Reference 4']

        self.selectCh1Ref = QComboBox()
        self.selectCh1Ref.addItems(reflist)
        self.selectCh1Ref.setFixedWidth(90)
        self.selectCh1Ref.setDisabled(1)
        self.selectCh2Ref = QComboBox()
        self.selectCh2Ref.addItems(reflist)
        self.selectCh2Ref.setFixedWidth(90)
        self.selectCh2Ref.setDisabled(1)
        self.selectCh3Ref = QComboBox()
        self.selectCh3Ref.addItems(reflist)
        self.selectCh3Ref.setFixedWidth(90)
        self.selectCh3Ref.setDisabled(1)
        self.selectCh4Ref = QComboBox()
        self.selectCh4Ref.addItems(reflist)
        self.selectCh4Ref.setFixedWidth(90)
        self.selectCh4Ref.setDisabled(1)
        self.selectChMathRef = QComboBox()
        self.selectChMathRef.addItems(reflist)
        self.selectChMathRef.setFixedWidth(90)
        self.selectChMathRef.setDisabled(1)

        #Boxes for setting the test parameters
        self.riseBox = QLineEdit()
        self.riseBox.setFixedWidth(50)
        self.riseBox.setText('3')
        self.ampBox = QLineEdit()
        self.ampBox.setFixedWidth(50)
        self.ampBox.setText('5')
        self.phaseBox = QLineEdit()
        self.phaseBox.setFixedWidth(50)
        self.phaseBox.setText('0.5')

        #Formatting the setup box
        self.setupLayout.addRow('Channel 1: ', self.selectCh1)
        self.setupLayout.addRow('           ', self.selectCh1Ref)
        self.setupLayout.addRow('Channel 2: ', self.selectCh2)
        self.setupLayout.addRow('           ', self.selectCh2Ref)
        self.setupLayout.addRow('Channel 3: ', self.selectCh3)
        self.setupLayout.addRow('           ', self.selectCh3Ref)
        self.setupLayout.addRow('Channel 4: ', self.selectCh4)
        self.setupLayout.addRow('           ', self.selectCh4Ref)
        self.setupLayout.addRow('Math: ', self.selectChMath)
        self.setupLayout.addRow('           ', self.selectChMathRef)
        self.setupLayout.addRow('% Risetime: ', self.riseBox)
        self.setupLayout.addRow('% Amplitude: ', self.ampBox)
        self.setupLayout.addRow('% Phase: ', self.phaseBox)
        self.setupBox.setLayout(self.setupLayout)
        self.setupBox.setFixedWidth(300)

        #Box that reports the test status to the user.
        self.statusBox = QTextEdit()
        self.statusBox.setReadOnly(1)
        self.statusBox.setStyleSheet("QTextEdit { background-color: rgb(0, 0, 0) }")
        self.statusBox.setTextColor(QColor(0, 200, 0))
        self.statustext = '>>>Auto Test Status Box'
        self.statusBox.setText(self.statustext)

        #Test buttons
        self.singleTestButton = QPushButton('Single Channel Test')
        self.singleTestButton.setFixedHeight(50)
        self.singleTestButton.setDisabled(1)
        self.singleTestButton.type = 'single'
        self.singleTestButton.clicked.connect(self.check_setup)
        self.multiTestButton = QPushButton('Multi Channel Test')
        self.multiTestButton.setFixedHeight(50)
        self.multiTestButton.clicked.connect(self.check_setup)
        self.multiTestButton.setDisabled(1)
        self.multiTestButton.type = 'multi'
        self.fullTestButton = QPushButton('Full Board Test')
        self.fullTestButton.setFixedHeight(50)
        self.fullTestButton.setDisabled(1)
        self.fullTestButton.type = 'full'
        self.fullTestButton.clicked.connect(self.check_setup)
        self.quitTestButton = QPushButton('Quit Test')
        self.quitTestButton.clicked.connect(self.quit_test)
        self.quitTestButton.setFixedHeight(50)
        self.quitTestButton.setDisabled(1)

        #Formatting the tab.
        self.topLayout = QHBoxLayout()
        self.topLayout.addWidget(self.statusBox)
        self.topLayout.addWidget(self.setupBox)

        self.bottomLayout = QHBoxLayout()
        self.bottomLayout.addWidget(self.singleTestButton)
        self.bottomLayout.addWidget(self.multiTestButton)
        self.bottomLayout.addWidget(self.fullTestButton)
        self.bottomLayout.addWidget(self.quitTestButton)

        self.Layout = QVBoxLayout()
        self.Layout.addLayout(self.topLayout)
        self.Layout.addLayout(self.bottomLayout)
        self.setLayout(self.Layout)
        self.setFixedWidth(825)
        self.setFixedHeight(520)

        self.threadState = 'off' #Current state of the tab's thread

        self.parent=parent

    @Slot()
    def enable_disable_ref(self, state):

        '''
        This function enables or disables the reference waveform box based on whether or not the related channel box has been turned on.
        It checks to see what the chNum attribute of the sender object is, and enables/disables the related reference box.
        '''
        sender = self.sender().chNum
        if state:
            eval('self.selectCh' + sender + 'Ref.setEnabled(1)')
        else:
            eval('self.selectCh' + sender + 'Ref.setDisabled(1)')

    def check_setup(self):

        '''
        This function checks the auto test parameters. If the test parameters are filled out correctly,
        it launches the selected test. For the mutli test, the multi test setup window is opened.
        '''
        
        boxlist = [self.selectCh1, self.selectCh2, self.selectCh3, self.selectCh4, self.selectChMath]
        refboxlist = [self.selectCh1Ref, self.selectCh2Ref, self.selectCh3Ref,
                   self.selectCh4Ref, self.selectChMathRef]

        #Determining which channels have been requested for testing and what the related reference wave is
        #for each channel.
        channellist = []
        reflist = []
        for i in range(len(boxlist)):
            if boxlist[i].isChecked():
                channellist.append(True)
                reflist.append(int(refboxlist[i].currentText().strip()[-1]))
            else:
                channellist.append(False)
                reflist.append(0)

        #An auto test can only run up to 4 channels. This statement ensures that no more than 4 channels have been selected for running
        if channellist.count(True) == 5:
            self.parent.auto_channelerror()
            return

        #Making sure that all other test parameters make sense.
        try: risetime = float(self.riseBox.text().encode('ascii', 'ignore').strip())
        except:
            self.parent.auto_riseerror()
            return

        if risetime > 100.0 or risetime < 0.0:
            self.parent.auto_riseerror()
            return

        try: amplitude = float(self.ampBox.text().encode('ascii', 'ignore').strip())
        except:
            self.parent.auto_amplitudeerror()
            return

        if amplitude > 100.0 or amplitude < 0.0:
            self.parent.auto_amplitudeerror()
            return

        try: phase = float(self.phaseBox.text().encode('ascii', 'ignore').strip())
        except:
            self.parent.auto_phaseerror()

        if phase > 100.0 or phase < 0.0:
            self.parent.auto_phaseerror()
            return

        #Dictionary of parameters that is passed to the test thread.
        dictionary = {'channel': channellist, 'reference': reflist, 'risetime': risetime,
                      'amplitude': amplitude, 'phase': phase}

        #Launching the test thread for the selected test. The threads have currently been commented out, since the auto test has not been implemented yet.
        if self.sender().type == 'single':
            #self.enable_test()
            #self.thread = testThread(self.parent.scope, 'Single', dictionary, self)
            #self.thread.resultsSignal.connect(self.new_message)
            #self.thread.confirmSignal.connect(self.test_confirmation)
            #self.thread.endSignal.connect(self.parent.new_auto_results)
            self.new_message('Beginning new single channel test.\n')
            #self.threadState = 'on'
            #self.thread.start()
        elif self.sender().type == 'multi':
            self.multi_test(dictionary) #Opening a multi test window.
        else:
            #self.enable_test()
            #self.thread = testThread(self.parent.scope, 'Full', dictionary, self)
            #self.thread.resultsSignal.connect(self.new_message)
            #self.thread.confirmSignal.connect(self.test_confirmation)
            #self.thread.endSignal.connect(self.parent.new_auto_results)
            self.new_message('Beginning new full board test.\n')
            #self.threadState = 'on'
            #self.thread.start()

    def multi_test(self, dictionary):

        '''
        This function opens up the window for settting up the multi test. It uses the multiGroupBox class
        to generate a box of channels for each channel group on the AMB board.
        '''

        #Generating/initializing window
        self.multiWindow = QWidget()
        self.multiWindow.dictionary = dictionary

        #Generating channel group boxes
        self.multiWindow.grouplist = []
        for i in range(10):
            group = multiGroupBox(i, self.multiWindow)
            self.multiWindow.grouplist.append(group)

        #First line of channel group boxes
        windowLine1Layout = QHBoxLayout()
        for i in range(5): windowLine1Layout.addWidget(self.multiWindow.grouplist[i])

        #Second line of channel group boxes
        windowLine2Layout = QHBoxLayout()
        for i in range(5, 10): windowLine2Layout.addWidget(self.multiWindow.grouplist[i])

        #Run test button
        testButton = QPushButton('Run Test')
        testButton.clicked.connect(self.run_multi_test)
        testButton.setFixedWidth(100)
        testButton.setFixedHeight(50)

        #Generating the window layout
        windowLayout = QVBoxLayout()
        windowLayout.addWidget(QLabel('Select Channels for Testing:'), alignment=Qt.AlignCenter)
        windowLayout.addSpacing(20)
        windowLayout.addLayout(windowLine1Layout)
        windowLayout.addLayout(windowLine2Layout)
        windowLayout.addSpacing(20)
        windowLayout.addWidget(testButton, alignment=Qt.AlignRight)

        #Formatting the window
        self.multiWindow.setLayout(windowLayout)
        self.multiWindow.setWindowModality(Qt.ApplicationModal)
        self.multiWindow.setWindowTitle('Select Channels')
        self.multiWindow.setFixedWidth(600)
        self.multiWindow.setFixedHeight(400)
        self.multiWindow.show()

    @Slot()
    def run_multi_test(self):

        '''
        This function runs the multi test thread using the channels that have been indicated in
        multiWindow. Functionality has been commented out since the auto test is currently incomplete.
        '''
        
        #self.enable_test()
        #self.thread = testThread(self.parent.scope, 'Multi', self.multiWindow.dictionary, self)
        #self.thread.resultsSignal.connect(self.new_message)
        #self.thread.confirmSignal.connect(self.test_confirmation)
        #self.thread.endSignal.connect(self.parent.new_auto_results)
        self.new_message('Beginning new multi channel test.\n')
        #self.threadState = 'on'
        #self.thread.start()
        
        self.multiWindow.close()

    @Slot()
    def new_message(self, message):

        '''
        This function writes a new message to the status box in the tab.
        '''
        
        self.statustext += '\n>>>' + message
        self.statusBox.setText(self.statustext)
        self.statusBox.moveCursor(QTextCursor.End)

    def enable_test(self):

        '''
        This function sets testing conditions onto the program so that users
        can't run other instrument functions during the test.
        '''

        self.singleTestButton.setDisabled(1)
        self.multiTestButon.setDisabled(1)
        self.fullTestButton.setDisabled(1)
        self.threadState = 'on'
        self.parent.AFGTab.sendButton.setDisabled(1)
        self.parent.AFGTab.saveConfigButton.setDisabled(1)
        self.parent.AFGTab.loadConfigButton.setDisabled(1)
        self.parent.oscilloscopeTab.setButton.setDisabled(1)
        self.parent.oscilloscopeTab.setButton.setDisabled(1)
        self.parent.oscilloscopeTab.saveStateButton.setDisabled(1)
        self.parent.oscilloscopeTab.loadStateButton.setDisabled(1)
        self.parent.oscilloscopeTab.grabWaveformButton.setDisabled(1)
        self.parent.oscilloscopeTab.sendRefButton.setDisabled(1)
        self.parent.oscilloscopeTab.chanBox1.setDisabled(1)
        self.parent.oscilloscopeTab.chanBox2.setDisabled(1)
        self.parent.oscilloscopeTab.chanBox3.setDisabled(1)
        self.parent.oscilloscopeTab.chanBox4.setDisabled(1)
        self.parent.linearityTab.controlButton.setDisabled(1)
        self.quitTestButton.setEnabled(1)

    @Slot()
    def quit_test(self):

        '''
        This function quits a test that is currently running and disables testing conditions
        from the program.
        '''

        self.quitTestButton.setDisabled(1)
        self.thread.kill()
        self.thread.exit()
        self.threadState = 'off'
        self.parent.AFGTab.sendButton.setEnabled(1)
        self.parent.AFGTab.saveConfigButton.setEnabled(1)
        self.parent.AFGTab.loadConfigButton.setEnabled(1)
        self.parent.oscilloscopeTab.setButton.setEnabled(1)
        self.parent.oscilloscopeTab.saveStateButton.setEnabled(1)
        self.parent.oscilloscopeTab.loadStateButton.setEnabled(1)
        self.parent.oscilloscopeTab.grabWaveformButton.setEnabled(1)
        self.parent.oscilloscopeTab.sendRefButton.setEnabled(1)
        self.parent.oscilloscopeTab.chanBox1.setEnabled(1)
        self.parent.oscilloscopeTab.chanBox2.setEnabled(1)
        self.parent.oscilloscopeTab.chanBox3.setEnabled(1)
        self.parent.oscilloscopeTab.chanBox4.setEnabled(1)
        self.parent.linearityTab.controlButton.setEnabled(1)
        self.singleTestButton.setEnabled(1)
        self.multiTestButton.setEnabled(1)
        self.fullTestButton.setEnabled(1)
        self.new_message('Test has concluded.\n')


class multiGroupBox(QWidget):

    '''
    multiGroupBox is the base class for the channel group box objects that are formed in autTab's
    multi test window.

    Parameters: groupnum (the channel group number)

    Functions: on_off
    '''
    
    def __init__(self, groupnum, parent=None):

        QWidget.__init__(self, parent)

        self.layout = QVBoxLayout(self)

        self.groupBox = QGroupBox('        Group %i' %(groupnum+1), self)
        self.groupBoxLayout = QVBoxLayout(self.groupBox)

        self.channellist = []

        #Placing the checkboxes for each channel into the box.
        for i in range(1, 5):
            channelnum  = i + 4*groupnum
            checkbox = QCheckBox('Channel ' + str(channelnum), self.groupBox)
            self.channellist.append(checkbox)
            self.groupBoxLayout.addWidget(checkbox)

        self.groupBox.setLayout(self.groupBoxLayout)
        self.layout.addWidget(self.groupBox)
        self.setLayout(self.layout)

        #The "master" checkbox for the groupbox
        self.checkBox = QCheckBox(self)
        self.checkBox.move(25, 3)
        self.checkBox.toggled.connect(self.on_off)

    @Slot()
    def on_off(self):

        '''
        This function checks/unchecks all of the channel checkboxes in the group box when the "master"
        check box has been toggled.
        '''
        
        if self.checkBox.isChecked():
            for i in self.channellist: i.setCheckState(Qt.Checked)
        else:
            for i in self.channellist: i.setCheckState(Qt.Unchecked)

if __name__ == '__main__':
    from PySide.QtGui import QApplication
    import sys

    qtApp = QApplication(argv)
    autoWindow = autoTab()
    autoWindow.show()
    exit(qtApp.exec_())
