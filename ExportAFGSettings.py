'''
Exports essential settings from the Arbitrary Function Generator into a 
txt file

Usage: 	Execute directly with `python ExportAFGSettings.py`; the program will
		take care of the rest.

Arguments: 	None

NOTE: 	State settings have been moved to the end of the list so that they are 
		the last to be set when imported, and won't be overwritten by other 
		commands.
'''

# For the 3252 in Mitch's Lab
DESC3252 = "USB0::0x0699::0x0345::C020930::INSTR"

# For the Agilent 81150A
DESC81150 = "USB0::0x0957::0x4108::DE47C00187::INSTR"

import visa
import csv
import StringIO

# Based on a user answer, pick which instrument descriptor to use.
def chooseInstrument():
	print("Please select a function generator to use:")
	print("1) Tektronix 3252")
	print("2) Agilent 81150A")

	while(True):
		userInput = raw_input(">>> ")
		if (userInput == "1" or userInput == "1)"):
			return DESC3252
		elif (userInput == "2" or userInput == "2)"):
			return DESC81150
		else:
			print("\nInput invalid.\n")


# Ask the instrument for a set of parameters, returning them in a String array
def inquire(afg, output, parameter):
	value = afg.ask(parameter)
	command = parameter[:-1] # Take off the question mark for reuse
	output.write("%s %s" %(command, value))

# For all the following:
# 1. Add a number if applicable for ImportSettings.py to find later
# 2. Ask the instrument for settings, passing the result through inquire(afg, output, )
# 3. After finishing a command group, add a new line character.

# martychangwhatdoido = the file to write to
def do(martychangwhatdoido, afginstance):
	# Pass the filename for writing
	# filename = raw_input("Please enter a file name to save to.\n>>> ")
	filename = martychangwhatdoido
	output = open(filename, 'w')
	# Add identifier to top of settings
	output.write("AFG\n")

	# Connect to the instrument
	rm = visa.ResourceManager()
	'''
	If this ever becomes more universal, uncomment the following.
	'''
	#instrumentdescriptor = chooseInstrument()
	instrumentdescriptor = DESC3252
	#afg = rm.get_instrument(instrumentdescriptor)
	afg = afginstance

	if (instrumentdescriptor == DESC3252):
		output.write("===Output Settings===\n")
		output.write("1.\n")
		inquire(afg, output, "output1:impedance?")
		inquire(afg, output, "output1:polarity?")
		inquire(afg, output, "output1:state?")
		inquire(afg, output, "output2:impedance?")
		inquire(afg, output, "output2:polarity?")
		inquire(afg, output, "output2:state?")
		inquire(afg, output, "output:trigger:mode?")
		output.write("\n")

		output.write("===Source1 Settings===\n")
		output.write("2.\n")
		inquire(afg, output, "source:roscillator:source?")
		inquire(afg, output, "source1:am:internal:frequency?")
		inquire(afg, output, "source1:am:internal:function?")
		inquire(afg, output, "source1:am:internal:function:efile?")
		inquire(afg, output, "source1:am:source?")
		inquire(afg, output, "source1:am:depth?")
		inquire(afg, output, "source1:burst:mode?")
		inquire(afg, output, "source1:burst:ncycles?")
		inquire(afg, output, "source1:burst:tdelay?")
		inquire(afg, output, "source1:combine:feed?")
		inquire(afg, output, "source1:fm:internal:frequency?")
		inquire(afg, output, "source1:fm:internal:function?")
		inquire(afg, output, "source1:fm:internal:function:efile?")
		inquire(afg, output, "source1:fm:source?")
		inquire(afg, output, "source1:fm:deviation?")
		inquire(afg, output, "source1:frequency:center?")
		inquire(afg, output, "source1:frequency:concurrent:state?")
		inquire(afg, output, "source1:frequency:mode?")
		inquire(afg, output, "source1:frequency:span?")
		inquire(afg, output, "source1:frequency:start?")
		inquire(afg, output, "source1:frequency:stop?")
		inquire(afg, output, "source1:frequency:cw?")
		inquire(afg, output, "source1:frequency:fixed?")
		inquire(afg, output, "source1:fskey:internal:rate?")
		inquire(afg, output, "source1:fskey:source?")
		inquire(afg, output, "source1:fskey:frequency?")
		inquire(afg, output, "source1:function:ramp:symmetry?")
		inquire(afg, output, "source1:function:shape?")
		inquire(afg, output, "source1:function:efile?")
		inquire(afg, output, "source1:phase:adjust?")
		inquire(afg, output, "source1:pm:internal:frequency?")
		inquire(afg, output, "source1:pm:internal:function?")
		inquire(afg, output, "source1:pm:source?")
		inquire(afg, output, "source1:pm:deviation?")
		inquire(afg, output, "source1:pulse:dcycle?")
		inquire(afg, output, "source1:pulse:delay?")
		inquire(afg, output, "source1:pulse:hold?")
		inquire(afg, output, "source1:pulse:period?")
		inquire(afg, output, "source1:pulse:transition:leading?")
		inquire(afg, output, "source1:pulse:transition:trailing?")
		inquire(afg, output, "source1:pulse:width?")
		inquire(afg, output, "source1:pwm:internal:frequency?")
		inquire(afg, output, "source1:pwm:internal:function?")
		inquire(afg, output, "source1:pwm:internal:function:efile?")
		inquire(afg, output, "source1:pwm:source?")
		inquire(afg, output, "source1:pwm:deviation:dcycle?")
		inquire(afg, output, "source1:sweep:htime?")
		inquire(afg, output, "source1:sweep:mode?")
		inquire(afg, output, "source1:sweep:rtime?")
		inquire(afg, output, "source1:sweep:spacing?")
		inquire(afg, output, "source1:sweep:time?")
		inquire(afg, output, "source1:voltage:concurrent:state?")
		inquire(afg, output, "source1:voltage:limit:high?")
		inquire(afg, output, "source1:voltage:limit:low?")
		inquire(afg, output, "source1:voltage:unit?")
		inquire(afg, output, "source1:voltage:level:immediate:high?")
		inquire(afg, output, "source1:voltage:level:immediate:low?")
		inquire(afg, output, "source1:voltage:level:immediate:offset?")
		inquire(afg, output, "source1:voltage:level:immediate:amplitude?")
		inquire(afg, output, "source1:pm:state?")
		inquire(afg, output, "source1:pwm:state?")
		inquire(afg, output, "source1:am:state?")
		inquire(afg, output, "source1:fm:state?")
		inquire(afg, output, "source1:burst:state?")
		inquire(afg, output, "source1:fskey:state?")
		output.write("\n")

		output.write("===Source2 Settings===\n")
		output.write("3.\n")

		inquire(afg, output, "source:roscillator:source?")
		inquire(afg, output, "source2:am:internal:frequency?")
		inquire(afg, output, "source2:am:internal:function?")
		inquire(afg, output, "source2:am:internal:function:efile?")
		inquire(afg, output, "source2:am:source?")
		inquire(afg, output, "source2:am:depth?")
		inquire(afg, output, "source2:burst:mode?")
		inquire(afg, output, "source2:burst:ncycles?")
		inquire(afg, output, "source2:burst:tdelay?")
		inquire(afg, output, "source2:combine:feed?")
		inquire(afg, output, "source2:fm:internal:frequency?")
		inquire(afg, output, "source2:fm:internal:function?")
		inquire(afg, output, "source2:fm:internal:function:efile?")
		inquire(afg, output, "source2:fm:source?")
		inquire(afg, output, "source2:fm:deviation?")
		inquire(afg, output, "source2:frequency:center?")
		inquire(afg, output, "source2:frequency:concurrent:state?")
		inquire(afg, output, "source2:frequency:mode?")
		inquire(afg, output, "source2:frequency:span?")
		inquire(afg, output, "source2:frequency:start?")
		inquire(afg, output, "source2:frequency:stop?")
		inquire(afg, output, "source2:frequency:cw?")
		inquire(afg, output, "source2:frequency:fixed?")
		inquire(afg, output, "source2:fskey:internal:rate?")
		inquire(afg, output, "source2:fskey:source?")
		inquire(afg, output, "source2:fskey:frequency?")
		inquire(afg, output, "source2:function:ramp:symmetry?")
		inquire(afg, output, "source2:function:shape?")
		inquire(afg, output, "source2:function:efile?")
		inquire(afg, output, "source2:phase:adjust?")
		inquire(afg, output, "source2:pm:internal:frequency?")
		inquire(afg, output, "source2:pm:internal:function?")
		inquire(afg, output, "source2:pm:source?")
		inquire(afg, output, "source2:pm:deviation?")
		inquire(afg, output, "source2:pulse:dcycle?")
		inquire(afg, output, "source2:pulse:delay?")
		inquire(afg, output, "source2:pulse:hold?")
		inquire(afg, output, "source2:pulse:period?")
		inquire(afg, output, "source2:pulse:transition:leading?")
		inquire(afg, output, "source2:pulse:transition:trailing?")
		inquire(afg, output, "source2:pulse:width?")
		inquire(afg, output, "source2:pwm:internal:frequency?")
		inquire(afg, output, "source2:pwm:internal:function?")
		inquire(afg, output, "source2:pwm:internal:function:efile?")
		inquire(afg, output, "source2:pwm:source?")
		inquire(afg, output, "source2:pwm:deviation:dcycle?")
		inquire(afg, output, "source2:sweep:htime?")
		inquire(afg, output, "source2:sweep:mode?")
		inquire(afg, output, "source2:sweep:rtime?")
		inquire(afg, output, "source2:sweep:spacing?")
		inquire(afg, output, "source2:sweep:time?")
		inquire(afg, output, "source2:voltage:concurrent:state?")
		inquire(afg, output, "source2:voltage:limit:high?")
		inquire(afg, output, "source2:voltage:limit:low?")
		inquire(afg, output, "source2:voltage:unit?")
		inquire(afg, output, "source2:voltage:level:immediate:high?")
		inquire(afg, output, "source2:voltage:level:immediate:low?")
		inquire(afg, output, "source2:voltage:level:immediate:offset?")
		inquire(afg, output, "source2:voltage:level:immediate:amplitude?")
		inquire(afg, output, "source2:pm:state?")
		inquire(afg, output, "source2:pwm:state?")
		inquire(afg, output, "source2:am:state?")
		inquire(afg, output, "source2:fm:state?")
		inquire(afg, output, "source2:burst:state?")
		inquire(afg, output, "source2:fskey:state?")
		output.write("\n")

	elif (instrumentdescriptor == DESC81150):
		output.write("===Apply Settings1===\n")
		output.write("1.\n")
		inquire(afg, output, "apply1?")
		output.write("\n")

		output.write("===Apply Settings2===\n")
		output.write("2.\n")
		inquire(afg, output, "apply2?")
		output.write("\n")

		output.write("===Arbitrary Commands 1===\n")
		output.write("3.\n")
		inquire(afg, output, "data1:attribute:average?")
		inquire(afg, output, "data1:attribute:cfactor?")
		inquire(afg, output, "data1:attribute:points?")
		inquire(afg, output, "data1:attribute:ptpeak?")
		inquire(afg, output, "function1:modulation:user?")
		output.write("\n")

		output.write("===Arbitrary Commands 2===\n")
		output.write("4.\n")
		inquire(afg, output, "data2:attribute:average?")
		inquire(afg, output, "data2:attribute:cfactor?")
		inquire(afg, output, "data2:attribute:points?")
		inquire(afg, output, "data2:attribute:ptpeak?")
		inquire(afg, output, "function2:modulation:user?")
		output.write("\n")

		output.write("===Arbitrary Commands Both===\n")
		output.write("5.\n")
		inquire(afg, output, "format:border?")
		output.write("\n")

		output.write("===Burst Commands 1===\n")
		output.write("6.\n")
		inquire(afg, output, "burst1:internal:period?")
		inquire(afg, output, "burst1:mode?")
		inquire(afg, output, "burst1:ncycles?")
		inquire(afg, output, "burst1:phase?")
		inquire(afg, output, "burst1:state?")
		inquire(afg, output, "trigger1:count?")
		inquire(afg, output, "unit1:angle?")
		output.write("\n")

		output.write("===Burst Commands 2===\n")
		output.write("7.\n")
		inquire(afg, output, "burst2:internal:period?")
		inquire(afg, output, "burst2:mode?")
		inquire(afg, output, "burst2:ncycles?")
		inquire(afg, output, "burst2:phase?")
		inquire(afg, output, "burst2:state?")
		inquire(afg, output, "trigger2:count?")
		inquire(afg, output, "unit2:angle?")
		output.write("\n")

		output.write("===Burst Commands Both===\n")
		output.write("8.\n")
		inquire(afg, output, "burst:gate:polarity?")
		output.write("\n")

		output.write("===Level Commands 1===\n")
		output.write("9.\n")
		inquire(afg, output, "voltage1:high?")
		inquire(afg, output, "voltage1:low?")
		inquire(afg, output, "voltage1:unit?")
		inquire(afg, output, "voltage1:limit:high?")
		inquire(afg, output, "voltage1:limit:low?")
		inquire(afg, output, "voltage1:limit:state?")
		inquire(afg, output, "voltage1:offset?")
		inquire(afg, output, "voltage1:range:auto?")
		output.write("\n")

		output.write("===Level Commands 2===\n")
		output.write("10.\n")
		inquire(afg, output, "voltage2:high?")
		inquire(afg, output, "voltage2:low?")
		inquire(afg, output, "voltage2:unit?")
		inquire(afg, output, "voltage2:limit:high?")
		inquire(afg, output, "voltage2:limit:low?")
		inquire(afg, output, "voltage2:limit:state?")
		inquire(afg, output, "voltage2:offset?")
		inquire(afg, output, "voltage2:range:auto?")
		output.write("\n")

		output.write("===Amplitude Modulation Commands 1===\n")
		output.write("11.\n")
		inquire(afg, output, "am1:depth?")
		inquire(afg, output, "am1:dsscarrier?")
		inquire(afg, output, "am1:external:impedance?")
		inquire(afg, output, "am1:external:range?")
		inquire(afg, output, "am1:internal:frequency?")
		inquire(afg, output, "am1:internal:function?")
		inquire(afg, output, "am1:source?")
		inquire(afg, output, "am1:state?")
		output.write("\n")

		output.write("===Amplitude Modulation Commands 2===\n")
		output.write("12.\n")
		inquire(afg, output, "am2:depth?")
		inquire(afg, output, "am2:dsscarrier?")
		inquire(afg, output, "am2:external:impedance?")
		inquire(afg, output, "am2:external:range?")
		inquire(afg, output, "am2:internal:frequency?")
		inquire(afg, output, "am2:internal:function?")
		inquire(afg, output, "am2:source?")
		inquire(afg, output, "am2:state?")
		output.write("\n")

		output.write("===Frequency Modulation Commands 1===\n")
		output.write("13.\n")
		inquire(afg, output, "fm1:deviation?")
		inquire(afg, output, "fm1:external:impedance?")
		inquire(afg, output, "fm1:external:range?")
		inquire(afg, output, "fm1:internal:frequency?")
		inquire(afg, output, "fm1:internal:function?")
		inquire(afg, output, "fm1:source?")
		inquire(afg, output, "fm1:state?")
		output.write("\n")

		output.write("===Frequency Modulation Commands 2===\n")
		output.write("14.\n")
		inquire(afg, output, "fm2:deviation?")
		inquire(afg, output, "fm2:external:impedance?")
		inquire(afg, output, "fm2:external:range?")
		inquire(afg, output, "fm2:internal:frequency?")
		inquire(afg, output, "fm2:internal:function?")
		inquire(afg, output, "fm2:source?")
		inquire(afg, output, "fm2:state?")
		output.write("\n")

		output.write("===Frequency Shift-Keying Modulation Commands 1===\n")
		output.write("15.\n")
		inquire(afg, output, "fskey1:external:impedance?")
		inquire(afg, output, "fskey1:external:level?")
		inquire(afg, output, "fskey1:external:range?")
		inquire(afg, output, "fskey1:frequency?")
		inquire(afg, output, "fskey1:internal:rate?")
		inquire(afg, output, "fskey1:source?")
		inquire(afg, output, "fskey1:state?")
		output.write("\n")

		output.write("===Frequency Shift-Keying Modulation Commands 2===\n")
		output.write("16.\n")
		inquire(afg, output, "fskey2:external:impedance?")
		inquire(afg, output, "fskey2:external:level?")
		inquire(afg, output, "fskey2:external:range?")
		inquire(afg, output, "fskey2:frequency?")
		inquire(afg, output, "fskey2:internal:rate?")
		inquire(afg, output, "fskey2:source?")
		inquire(afg, output, "fskey2:state?")
		output.write("\n")

		output.write("===Phase Modulation Commands 1===\n")
		output.write("17.\n")
		inquire(afg, output, "pm1:deviation?")
		inquire(afg, output, "pm1:external:impedance?")
		inquire(afg, output, "pm1:external:range?")
		inquire(afg, output, "pm1:internal:frequency?")
		inquire(afg, output, "pm1:internal:function?")
		inquire(afg, output, "pm1:source?")
		inquire(afg, output, "pm1:state?")
		output.write("\n")

		output.write("===Phase Modulation Commands 2===\n")
		output.write("18.\n")
		inquire(afg, output, "pm2:deviation?")
		inquire(afg, output, "pm2:external:impedance?")
		inquire(afg, output, "pm2:external:range?")
		inquire(afg, output, "pm2:internal:frequency?")
		inquire(afg, output, "pm2:internal:function?")
		inquire(afg, output, "pm2:source?")
		inquire(afg, output, "pm2:state?")
		output.write("\n")

		output.write("===Pulse Width Modulation Commands 1===\n")
		output.write("19.\n")
		inquire(afg, output, "pwm1:deviation?")
		inquire(afg, output, "pwm1:deviation:dcycle?")
		inquire(afg, output, "pwm1:external:impedance?")
		inquire(afg, output, "pwm1:external:range?")
		inquire(afg, output, "pwm1:internal:frequency?")
		inquire(afg, output, "pwm1:internal:function?")
		inquire(afg, output, "pwm1:source?")
		inquire(afg, output, "pwm1:state?")
		output.write("\n")

		output.write("===Pulse Width Modulation Commands 2===\n")
		output.write("20.\n")
		inquire(afg, output, "pwm2:deviation?")
		inquire(afg, output, "pwm2:deviation:dcycle?")
		inquire(afg, output, "pwm2:external:impedance?")
		inquire(afg, output, "pwm2:external:range?")
		inquire(afg, output, "pwm2:internal:frequency?")
		inquire(afg, output, "pwm2:internal:function?")
		inquire(afg, output, "pwm2:source?")
		inquire(afg, output, "pwm2:state?")
		output.write("\n")

		output.write("===Channel Commands===\n")
		output.write("21.\n")
		inquire(afg, output, "channel:math?")
		output.write("\n")

		output.write("===Output Commands 1===\n")
		output.write("22.\n")
		inquire(afg, output, "output1?")
		inquire(afg, output, "output1:complement?")
		inquire(afg, output, "output1:impedance?")
		inquire(afg, output, "output1:load?")
		inquire(afg, output, "output1:polarity?")
		inquire(afg, output, "output1:route?")
		inquire(afg, output, "output1:strobe:voltage?")
		inquire(afg, output, "output1:trigger:voltage?")
		#inquire(afg, output, "output1:strobe:route?")
		#inquire(afg, output, "output1:trigger:route?")
		inquire(afg, output, "track:channel1?")
		output.write("\n")

		output.write("===Output Commands 2===\n")
		output.write("23.\n")
		inquire(afg, output, "output2?")
		inquire(afg, output, "output2:complement?")
		inquire(afg, output, "output2:impedance?")
		inquire(afg, output, "output2:load?")
		inquire(afg, output, "output2:polarity?")
		inquire(afg, output, "output2:route?")
		inquire(afg, output, "output2:strobe:voltage?")
		inquire(afg, output, "output2:trigger:voltage?")
		#inquire(afg, output, "output2:strobe:route?")
		#inquire(afg, output, "output2:trigger:route?")
		inquire(afg, output, "track:channel2?")
		output.write("\n")

		output.write("===Output Commands Both===\n")
		output.write("24.\n")
		inquire(afg, output, "track:frequency?")
		inquire(afg, output, "track:frequency:divider?")
		inquire(afg, output, "track:frequency:multiplier?")
		output.write("\n")

		output.write("===Output Function Commands 1===\n")
		output.write("25.\n")
		inquire(afg, output, "frequency1?")
		inquire(afg, output, "function1?")
		inquire(afg, output, "function1:noise:pdfunction?")
		inquire(afg, output, "function1:pulse:dcycle?")
		inquire(afg, output, "function1:pulse:delay:hold?")
		inquire(afg, output, "function1:pulse:delay:unit?")
		inquire(afg, output, "function1:pulse:hold?")
		inquire(afg, output, "function1:pulse:tdelay?")
		inquire(afg, output, "function1:pulse:transition?")
		inquire(afg, output, "function1:pulse:transition:hold?")
		inquire(afg, output, "function1:pulse:transition:trailing?")
		inquire(afg, output, "function1:pulse:transition:trailing:auto?")
		inquire(afg, output, "function1:pulse:transition:unit?")
		inquire(afg, output, "function1:pulse:width?")
		inquire(afg, output, "function1:ramp:symmetry?")
		inquire(afg, output, "function1:square:dcycle?")
		inquire(afg, output, "pulse:dcycle1?")
		inquire(afg, output, "pulse:delay1?")
		inquire(afg, output, "pulse:delay1:hold?")
		inquire(afg, output, "pulse:delay1:unit?")
		inquire(afg, output, "pulse:frequency1?")
		inquire(afg, output, "pulse:hold1?")
		inquire(afg, output, "pulse:period1?")
		inquire(afg, output, "pulse:tdelay1?")
		inquire(afg, output, "pulse:transition1?")
		inquire(afg, output, "pulse:transition1:hold?")
		inquire(afg, output, "pulse:transition1:trailing?")
		inquire(afg, output, "pulse:transition1:trailing:auto?")
		inquire(afg, output, "pulse:transition1:unit?")
		inquire(afg, output, "pulse:width1?")
		output.write("\n")

		output.write("===Output Function Commands 2===\n")
		output.write("26.\n")
		inquire(afg, output, "frequency2?")
		inquire(afg, output, "function2?")
		inquire(afg, output, "function2:noise:pdfunction?")
		inquire(afg, output, "function2:pulse:dcycle?")
		inquire(afg, output, "function2:pulse:delay:hold?")
		inquire(afg, output, "function2:pulse:delay:unit?")
		inquire(afg, output, "function2:pulse:hold?")
		inquire(afg, output, "function2:pulse:tdelay?")
		inquire(afg, output, "function2:pulse:transition?")
		inquire(afg, output, "function2:pulse:transition:hold?")
		inquire(afg, output, "function2:pulse:transition:trailing?")
		inquire(afg, output, "function2:pulse:transition:trailing:auto?")
		inquire(afg, output, "function2:pulse:transition:unit?")
		inquire(afg, output, "function2:pulse:width?")
		inquire(afg, output, "function2:ramp:symmetry?")
		inquire(afg, output, "function2:square:dcycle?")
		inquire(afg, output, "pulse:dcycle2?")
		inquire(afg, output, "pulse:delay2?")
		inquire(afg, output, "pulse:delay2:hold?")
		inquire(afg, output, "pulse:delay2:unit?")
		inquire(afg, output, "pulse:frequency2?")
		inquire(afg, output, "pulse:hold2?")
		inquire(afg, output, "pulse:period2?")
		inquire(afg, output, "pulse:tdelay2?")
		inquire(afg, output, "pulse:transition2?")
		inquire(afg, output, "pulse:transition2:hold?")
		inquire(afg, output, "pulse:transition2:trailing?")
		inquire(afg, output, "pulse:transition2:trailing:auto?")
		inquire(afg, output, "pulse:transition2:unit?")
		inquire(afg, output, "pulse:width2?")
		output.write("\n")

		output.write("===Reference Clock Commands===\n")
		output.write("27.\n")
		inquire(afg, output, "roscillator:source?")
		inquire(afg, output, "roscillator:source:auto?")
		output.write("\n")

		output.write("===Sweep Commands 1===\n")
		output.write("28.\n")
		inquire(afg, output, "frequency1:center?")
		inquire(afg, output, "frequency1:span?")
		inquire(afg, output, "frequency1:start?")
		inquire(afg, output, "frequency1:stop?")
		inquire(afg, output, "marker1:state?")
		inquire(afg, output, "marker1:frequency?")
		inquire(afg, output, "sweep1:idle?")
		inquire(afg, output, "sweep1:spacing?")
		inquire(afg, output, "sweep1:state?")
		inquire(afg, output, "sweep1:time?")
		output.write("\n")

		output.write("===Sweep Commands 2===\n")
		output.write("29.\n")
		inquire(afg, output, "frequency2:center?")
		inquire(afg, output, "frequency2:span?")
		inquire(afg, output, "frequency2:start?")
		inquire(afg, output, "frequency2:stop?")
		inquire(afg, output, "marker2:state?")
		inquire(afg, output, "marker2:frequency?")
		inquire(afg, output, "sweep2:idle?")
		inquire(afg, output, "sweep2:spacing?")
		inquire(afg, output, "sweep2:state?")
		inquire(afg, output, "sweep2:time?")
		output.write("\n")

		output.write("===Triggering Commands 1===\n")
		output.write("30.\n")
		inquire(afg, output, "arm:frequency1?")
		inquire(afg, output, "arm:period1?")
		inquire(afg, output, "arm:sense1?")
		inquire(afg, output, "arm:slope1?")
		inquire(afg, output, "arm:source1?")
		inquire(afg, output, "trigger:source1?")
		output.write("\n")

		output.write("===Triggering Commands 2===\n")
		output.write("31.\n")
		inquire(afg, output, "arm:frequency2?")
		inquire(afg, output, "arm:period2?")
		inquire(afg, output, "arm:sense2?")
		inquire(afg, output, "arm:slope2?")
		inquire(afg, output, "arm:source2?")
		#inquire(afg, output, "trigger:source2?")
		output.write("\n")

		output.write("===Triggering Commands Both===\n")
		output.write("32.\n")
		#inquire(afg, output, "arm:efrequency?")
		#inquire(afg, output, "arm:hysteresis?")
		inquire(afg, output, "arm:impedance?")
		inquire(afg, output, "arm:level?")
		output.write("\n")

		output.write("===Pattern Related Commands 1===\n")
		output.write("33.\n")
		inquire(afg, output, "digital1?")
		output.write("\n")

		output.write("===Pattern Related Commands 2===\n")
		output.write("34.\n")
		inquire(afg, output, "digital2?")
		output.write("\n")

		output.write("===Pattern Related Commands Both===\n")
		output.write("35.\n")
		inquire(afg, output, "digital:signal:format?")
		output.write("\n")

	
