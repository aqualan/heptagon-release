import sys, visa
from PySide.QtCore import *
from PySide.QtGui import(QApplication, QWidget, QLabel, QVBoxLayout, QPushButton, QGroupBox,
                         QFormLayout, QComboBox, QHBoxLayout, QErrorMessage)

from sillyscope import sillyscope
from generator import generator

import time

class settingsTab(QWidget):
    '''The settings tab for the GUI.'''

    def __init__(self, parent=None):

        #---Initializing the widget.---#
        QWidget.__init__(self, parent)

        #---Generating widget contents.---#
        self.settingsLabel = QLabel('Settings')

        self.deviceBox = QGroupBox('Device Control')
        self.deviceLayout = QVBoxLayout()

        self.oscilloscopeDeviceBox = deviceBox('Oscilloscope Device')
        self.oscilloscopeDeviceBox.connectButton.clicked.connect(self.connect_oscilloscope)
        self.oscilloscopeDeviceBox.disconnectButton.clicked.connect(self.disconnect_oscilloscope)

        self.AFGDeviceBox = deviceBox('AFG Device')
        self.AFGDeviceBox.connectButton.clicked.connect(self.connect_AFG)
        self.AFGDeviceBox.disconnectButton.clicked.connect(self.disconnect_AFG)
        
        self.deviceList = QHBoxLayout()
        self.deviceList.addWidget(self.oscilloscopeDeviceBox)
        self.deviceList.addWidget(self.AFGDeviceBox)

        self.updateDevicesButton = QPushButton('Update Devices...', self)
        self.updateDevicesButton.clicked.connect(self.up_devices)

        self.deviceLayout.addWidget(self.updateDevicesButton)
        self.deviceLayout.addLayout(self.deviceList)

        self.deviceBox.setLayout(self.deviceLayout)

        self.AFGConnected = False

        #---Organizing widget layout.---#
        self.settingsLayout = QVBoxLayout()
        self.settingsLayout.addWidget(self.settingsLabel)
        self.settingsLayout.addWidget(self.deviceBox)
        self.setLayout(self.settingsLayout)

        self.parent = self.parent()

    #---Functions.---#
    @Slot()
    def up_devices(self):
        
        self.oscilloscopeDeviceBox.chooseList.clear()
        self.AFGDeviceBox.chooseList.clear()

        try:
            insts = visa.get_instruments_list()
            self.instrument_list = []
            self.instrument_names = []

            for i in range(len(insts)):
                self.instrument_list.append(insts[i].encode('ascii', 'ignore'))
                if self.parent != None:
                    self.parent.rm.get_instrument(self.instrument_list[i]).write('*IDN?')
                    name = self.parent.rm.get_instrument(self.instrument_list[i]).read()
                else:
                    visa.ResourceManager().get_instrument(self.instrument_list[i]).write('*IDN?')
                    name = visa.ResourceManager().get_instrument(self.instrument_list[i]).read()
                self.instrument_names.append(name[:18])

            self.oscilloscopeDeviceBox.chooseList.addItems(self.instrument_names)
            self.oscilloscopeDeviceBox.connectButton.setEnabled(1)
            self.AFGDeviceBox.chooseList.addItems(self.instrument_names)
            self.AFGDeviceBox.connectButton.setEnabled(1)

        except:
            self.oscilloscopeDeviceBox.connectButton.setDisabled(1)
            self.AFGDeviceBox.connectButton.setDisabled(1)
            self.io_error_window = QErrorMessage()
            self.io_error_window.showMessage('There are no devices connected to the ' + 
                                             'computer. Please make sure the oscilloscope ' + 
                                             'is connected and try again.')

    @Slot()
    def connect_oscilloscope(self):
        #building oscilloscope
        self.silly_name = self.instrument_list[self.oscilloscopeDeviceBox.chooseList.currentIndex()]
        self.parent.scope = sillyscope(self.silly_name, self.parent)

        self.oscilloscopeDeviceBox.disconnectButton.setEnabled(1)

        #enabling functionality
        for i in range(self.parent.scope.numberChannels):
            eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.setEnabled(1)')
            if self.parent.scope.impedanceStore[i] == 'MEG':
                eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio1M.toggle()')
            else:
                eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio50.toggle()')
            if self.parent.scope.stateStore[i] == 'ON':
                eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.OnOff.toggle()')
                eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio1M.setEnabled(1)')
                eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio50.setEnabled(1)')
            else:
                eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio1M.setDisabled(1)')
                eval('self.parent.oscilloscopeTab.chanBox' + str(i + 1) + '.radio50.setDisabled(1)')

        self.parent.oscilloscopeTab.setButton.setEnabled(1)
                
        while True:
            QApplication.processEvents()
            try: connect_state = visa.get_instruments_list().index(self.silly_name) + 1
            except: connect_state = 0
            if not connect_state:
                break

        self.disconnect_oscilloscope()
        self.up_devices()

    def connect_AFG(self):
        #building AFG
        self.AFGName = self.instrument_list[self.AFGDeviceBox.chooseList.currentIndex()]
        self.parent.AFG = generator(self.AFGName, self.parent)

        #enabling functionality
        self.AFGDeviceBox.disconnectButton.setEnabled(1)
        self.parent.AFGTab.waveformTab1.sendButton1.setEnabled(1)
        self.parent.AFGTab.waveformTab1.sendButton2.setEnabled(1)
        for i in self.parent.AFGTab.waveformDict:
            if self.parent.AFGTab.waveformDict[i] != None:
                self.parent.AFGTab.waveformDict[i].sendButton1.setEnabled(1)
                self.parent.AFGTab.waveformDict[i].sendButton2.setEnabled(1)

        self.AFGConnected = True

        print visa.get_instruments_list().index(self.AFGName)

##        counter = 0
##        while True:
##            QApplication.processEvents()
##            try:
##                connect_state = visa.get_instruments_list().index(self.AFGName) + 1
##                counter = 0
##            except:
##                connect_state = 0
##                counter += 1
##            if not connect_state and counter > 5:
##                break
##            time.sleep(1)
##
##        self.disconnect_AFG()
##        self.up_devices()

    @Slot()
    def disconnect_oscilloscope(self):
        for i in range(self.parent.scope.numberChannels):
            eval('self.parent.oscilloscopeTab.chanBox' + str(i) + '.setDisabled(1)')
        self.silly_disconnect.setDisabled(1)
        self.set_button.setDisabled(1)

    @Slot()
    def disconnect_AFG(self):
        self.AFGConnected = False
        self.AFGDeviceBox.disconnectButton.setDisabled(1)
        self.parent.AFGTab.waveformTab1.sendButton1.setDisabled(1)
        self.parent.AFGTab.waveformTab1.sendButton2.setDisabled(1)
        for i in self.parent.AFGTab.waveformDict:
            if self.parent.AFGTab.waveformDict[i] != None:
                self.parent.AFGTab.waveformDict[i].sendButton1.setDisabled(1)
                self.parent.AFGTab.waveformDict[i].sendButton2.setDisabled(1)
        

class deviceBox(QGroupBox):

    '''A widget that contains a choose device list, connect button,
        and disconnect button for a device.'''

    def __init__(self, device_name, parent=None):

        #Initializing widget.
        QGroupBox.__init__(self, device_name, parent)
        self.Layout = QFormLayout()
        self.chooseList = QComboBox(self)
        self.connectButton = QPushButton('Connect', self)
        self.connectButton.setDisabled(1)
        self.disconnectButton = QPushButton('Disconnect', self)
        self.disconnectButton.setDisabled(1)

        #Formatting widget.
        self.Layout.addRow('Choose Device: ', self.chooseList)
        self.Layout.addRow(self.connectButton, self.disconnectButton)
        self.setLayout(self.Layout)
     

if __name__ == "__main__":
    qtApp = QApplication(sys.argv)
    settingsWindow = settingsTab()
    settingsWindow.show()
    qtApp.exec_()
