# Example script for socket client
# Usage : python example-client.py [ip_address of server]

import socket
import sys
import struct
import time

# Main function
if __name__ == "__main__":
	if (len(sys.argv) < 2):
		print 'Usage : python client.py hostname'
		sys.exit()

	host = sys.argv[1]
	port = 8888

# Create an INET, STREAMing socket
try:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
	print 'Failed to create socket'
	sys.exit()

print 'Socket created'

try:
	remote_ip = socket.gethostbyname( host )
	s.connect ((host, port))
except socket.gaierror:
	print 'Hostname could not be resolved. Exiting.'
	sys.exit()

print 'Socket connected to ' + host + ' on ip ' + remote_ip
'''
# Send some data to remote server
message = "Test"

try:
	# Set the whole string
	#while True:
	s.send(message)
	print 'Sending message...'
except socket.error:
	# Send Failed
	print 'Send failed'
	sys.exit()
'''

def recv_timeout(the_socket, timeout=1):
	print "Ready to receive instructions!"
	# Make socket non blocking
	the_socket.setblocking(0)

	# Total data partwise in an array
	total_data = []
	data = ''

	# Beginning time
	begin = time.time()
	while 1:
		# If you got no data at all, continue waiting.
		if time.time() - begin > timeout * 2:
			#print("Breaking after second timeout")
			#break
			print "Awaiting instructions..."
			time.sleep(1)
			the_socket.send("Hey, got anything for me yet?")

		# Give time for the server to craft a response
		time.sleep(1)
		# Received something
		try:
			data = the_socket.recv(8192)
			if data:
				print "Received instruction!"
				total_data.append(data)
				# Change the beginning time of measurement
				begin = time.time()
				print ''.join(total_data)

				if (data != "Sorry man; I got nothing."):
					# Acknowledge receipt if not empty
					print("Sending confirmation.")
					the_socket.send("Confirmed: " + data)
				total_data = []
		except:
			pass

# Get reply and print
recv_timeout(s)

s.close()
