#!/usr/bin/env python

'''
linearityTab.py

Function:   Holds the linearityTab class, which holds and formats the widgets
            used for running the linearity test.

Classes:    linearityTab, testBox, linearityBox, calibrationBox, deviceBox

Functions:  isPower

Notes:      linearityTab is only activated if backend.py detects that both
            an oscilloscope and a function generator have been connected to
            the computer.
'''

import re
import sys
from math import log
from PySide.QtCore import(Qt, Slot)
from PySide.QtGui import(QWidget, QHBoxLayout, QVBoxLayout, QTextEdit, QColor, QPushButton, QGroupBox, QApplication,
                         QRadioButton, QLineEdit, QLabel, QComboBox, QFileDialog, QButtonGroup, QTextCursor)

from loadwaveform import loadwaveform
from waveform_preview import waveformPreview
from readfromfile import importdata
from linearitytest import linearityThread

class linearityTab(QWidget):
    
    '''
    A class that holds the widgets that are used to run the
    linearity test.

    Functions: run_test, linearity_test, calibration_test, device_test, enable_test, quit_test, change_button_name, new_message
    '''

    kind = 'test'

    def __init__(self, parent=None):

        #---Initializing the widget.---#
        QWidget.__init__(self, parent)

        #---Building test control tab---#
        self.Layout = QHBoxLayout()
        
        #Making widgets
        self.linearityBox = linearityBox(self)
        self.calibrationBox = calibrationBox(self)
        self.deviceBox = deviceBox('Device Test Setup')
        self.deviceBox.setFixedWidth(425)

        #Collecting radio buttons
        self.testSelectGroup = QButtonGroup()
        self.testSelectGroup.addButton(self.linearityBox.button)
        self.testSelectGroup.addButton(self.calibrationBox.button)
        self.testSelectGroup.addButton(self.deviceBox.button)
        self.testSelectGroup.buttonClicked.connect(self.change_button_name)

        #Right panel of the test tab
        self.rightLayout = QVBoxLayout()

        #Preview screen for load waveform
        self.statusBox = QTextEdit()
        self.statusBox.setReadOnly(1)
        self.statusBox.setStyleSheet("QTextEdit { background-color: rgb(0, 0, 0) }")
        self.statusBox.setTextColor(QColor(0, 200, 0))
        self.statustext = '>>>Linearity Test Status Box'
        self.statusBox.setText(self.statustext)

        #Button for running/stopping test
        self.controlButton = QPushButton('Run Selected Test', self)
        self.controlButton.clicked.connect(self.run_test)
        self.controlButton.setFixedWidth(325)
        self.controlButton.setFixedHeight(50)
        self.controlButton.setDisabled(1)

        #Formatting right panel of the test tab
        self.rightLayout.addWidget(self.statusBox)
        self.rightLayout.addWidget(self.controlButton)

        #Formatting left panel of the test tab
        self.leftLayout = QVBoxLayout()
        self.leftLayout.addWidget(self.linearityBox)
        self.leftLayout.addWidget(self.calibrationBox)
        self.leftLayout.addWidget(self.deviceBox)

        #Formatting test panel
        self.Layout.addLayout(self.leftLayout)
        self.Layout.addLayout(self.rightLayout)
        self.setLayout(self.Layout)
        self.setFixedWidth(825)
        self.setFixedHeight(520)

        self.threadState = 'off'

        self.parent = parent

    @Slot()
    def run_test(self):

        '''
        This function runs a test based on which test has been selected for running
        '''
        
        if self.linearityBox.button.isChecked(): self.linearity_test()
        elif self.calibrationBox.button.isChecked(): self.calibration_test()
        elif self.deviceBox.button.isChecked(): self.device_test()
        else: self.parent.linearity_selecterror()
        
    def linearity_test(self):

        '''
        This fuction sets up for and runs the linearity test. First checks to make sure that the user input
        is formatted correctly, then runs the linearity test using linearityThread. After the linearity
        test has been completed, the function displays the results collected from the test in the results
        panel, and then enables the option to export the result data to an excel file.
        '''

        #Check if startvBox is empty or contains wrong type. If not, save contents
        if self.linearityBox.startvBox.text().encode('ascii', 'ignore') == '':
            self.parent.linearity_startverror()
            return
        else:
            try: startv = float(self.linearityBox.startvBox.text().encode('ascii', 'ignore'))
            except:
                self.parent.linearity_startvtypeerror()
                return

        #Check if endvBox is empty or contains wrong type. If not, save contents
        if self.linearityBox.endvBox.text().encode('ascii', 'ignore') == '':
            self.parent.linearity_endverror()
            return
        else:
            try: endv = float(self.linearityBox.endvBox.text().encode('ascii', 'ignore'))
            except:
                self.parent.linearity_endvtypeerror()
                return

        #Check if numBox is empty or contains wrong type. If not, save contents    
        if self.linearityBox.numBox.text().encode('ascii', 'ignore') == '':
            self.parent.linearity_reserror()
            return
        else:
            try:
                numpoints = int(self.linearityBox.numBox.text().encode('ascii', 'ignore'))
                if numpoints < 0:
                    self.parent.linearity_restypeerror1()
                    return
            except:
                self.parent.linearity_restypeerror2()
                return

        #Check if linSamplesBox is empty or contains wrong type. If not, save contents.
        try:
            samples = int(self.linearityBox.linSamplesBox.text().encode('ascii', 'ignore'))
            if samples <= 0 or not isPower(samples, 2):
                self.parent.linearity_sampleserror()
                return
        except:
            self.parent.linearity_sampleserror()
            return

        #Check if nothing is selected in outputBox. If not, check if advanced options have been selected
        #and save box contents
        if int(self.linearityBox.outputBox.currentIndex()) == -1:
            self.parent.linearity_outputerror()
            return
        else: output = self.linearityBox.outputBox.currentText().encode('ascii', 'ignore')

        #Check if nothing is selected in the afgChanBox
        if int(self.linearityBox.afgChanBox.currentIndex()) == -1:
            self.parent.linearity_afgchanerror()
            return
        else: source = int(self.linearityBox.afgChanBox.currentIndex()) + 1

        #Check if nothing is selected in the oscChanBox.
        if int(self.linearityBox.oscChanBox.currentIndex()) == -1:
            self.parent.linearity_oscchanerror()
            return
        else: channel = int(self.linearityBox.oscChanBox.currentIndex()) + 1

        #Formatting for math channel
        if channel == 5: channel = 'Math'

        #Checking the math operations if the math channel has been selected
        if int(self.linearityBox.oscChanBox.currentIndex()) == 4:
            if int(self.linearityBox.mathChanBoxA.currentIndex()) == -1:
                self.parent.linearity_matherror()
                return
            else: mathA = str(self.linearityBox.mathChanBoxA.currentText())

            if int(self.linearityBox.mathOperationBox.currentIndex()) == -1:
                self.parent.linearity_matherror()
                return
            else: mathOp = str(self.linearityBox.mathOperationBox.currentText())

            if int(self.linearityBox.mathChanBoxB.currentIndex()) == -1:
                self.parent.linearity_matherror()
                return
            else: mathB = str(self.linearityBox.mathChanBoxB.currentText())

            if mathA == mathB:
                self.parent.linearity_matherror2()
                return
            else: mathexpression = ' ' + mathA + mathOp + mathB
        else: mathexpression = None
            

        #Making sure the user has not used malicious text as input.
        # We do this by using a regex that checks for variable names, numbers and operators
        # If any of these searches comes up empty: ('', '', '', '') then some part of the string
        # is not a variable name, number or operator, and we discard user input.
        text = self.linearityBox.advancedLine.text().encode('ascii', 'ignore').strip()
        userinput = re.compile(r'(maxval)|(minval)|([0-9]+\.?[0-9]*)|([\+\-\*\/]{1,2})|(max\(mathwaveform\))|(min\(mathwaveform\))')
        for tup in userinput.findall(text):
            emptystringcount = 0
            for element in tup:
                if element == '':
                    emptystringcount+=1
            if emptystringcount == 4:
                self.parent.linearity_advancederror()
                return
        advanced = text

        #Turning on the necessary oscilloscope channels for the test.
        self.parent.AFG.write_outputstate(source, 'on')
        self.parent.scope.write_channelstate(channel, 'on')

        #Setting up for the test.
        self.controlButton.setDisabled(1)
        self.enable_test()
        dictionary = {'startv': startv, 'endv': endv, 'numpoints': numpoints, 'samples': samples, 'output': output, 'advanced': advanced,
                      'source': source, 'channel': channel, 'mathexpression': mathexpression}
        self.thread = linearityThread('Linearity', dictionary, self)
        self.thread.resultsSignal.connect(self.parent.new_linearity_results)
        self.thread.messageSignal.connect(self.new_message)
        self.new_message('Beginning new linearity test.\n')
        self.threadState = 'on'
        self.thread.start()
        self.controlButton.setEnabled(1)

    def calibration_test(self):

        '''
        This fucntions sets up for and runs the calibration test. First checks to make sure that the user input has been
        formatted correctly, then runs the calibration test using linearityThread.
        '''

        #Making sure all input has been correctly formatted.
        try: tolerance = float(self.calibrationBox.toleranceBox.text().encode('ascii', 'ignore').strip())
        except:
            self.parent.linearity_tolerancetypeerror()
            return
        
        if tolerance < 0:
            self.parent.linearity_toleranceerror()
            return

        try: samples = int(self.calibrationBox.samplesBox.text().encode('ascii', 'ignore').strip())
        except:
            self.parent.linearity_samplestypeerror()
            return

        if samples <= 0 or not isPower(samples, 2):
            self.parent.linearity_sampleserror()
            return

        if int(self.calibrationBox.calOscBox.currentIndex()) == -1:
            self.parent.linearity_oscchanerror()
            return
        else: channel = int(self.calibrationBox.calOscBox.currentIndex()) + 1

        if channel == 5: channel = 'Math'

        if int(self.calibrationBox.calAFGBox.currentIndex()) == -1:
            self.parent.linearity_afgchanerror()
            return
        else: source = int(self.calibrationBox.calAFGBox.currentIndex()) + 1

        #Need a way to error check filename!
        if self.calibrationBox.calFileName.text().encode('ascii', 'ignore').strip() == '':
            self.parent.linearity_nofileerror()
            return
        else: filename = self.calibrationBox.calFileName.text().encode('ascii', 'ignore').strip()

        if int(self.calibrationBox.calOscBox.currentIndex()) == 4:
            if int(self.calibrationBox.mathChanBoxA.currentIndex()) == -1:
                self.parent.linearity_matherror()
                return
            else: mathA = str(self.calibrationBox.mathChanBoxA.currentText())

            if int(self.calibrationBox.mathOperationBox.currentIndex()) == -1:
                self.parent.linearity_matherror()
                return
            else: mathOp = str(self.calibrationBox.mathOperationBox.currentText())

            if int(self.calibrationBox.mathChanBoxB.currentIndex()) == -1:
                self.parent.linearity_matherror()
                return
            else: mathB = str(self.calibrationBox.mathChanBoxB.currentText())

            if mathA == mathB:
                self.parent.linearity_matherror2()
                return
            else: mathexpression = ' ' + mathA + mathOp + mathB
        else: mathexpression = None

        #Enabling and running calibration test
        self.controlButton.setDisabled(1)
        self.enable_test()
        self.parent.AFG.write_outputstate(source, 'on')
        self.parent.scope.write_channelstate(channel, 'on')
        
        dictionary = {'tolerance': tolerance, 'samples': samples,
                      'filename': filename, 'channel': channel, 'source': source, 'mathexpression': mathexpression}
        self.thread = linearityThread('Calibration', dictionary, self)
        self.thread.resultsSignal.connect(self.parent.new_linearity_results)
        self.thread.messageSignal.connect(self.new_message)
        self.new_message('Beginning new calibration test.\n')
        self.threadState = 'on'
        self.thread.start()
        self.controlButton.setEnabled(1)

    def device_test(self):


        '''
        This function sets up for and runs the device test. First checks to make sure that the user input has
        been formatted correctly, then runs the device test using linearityThread
        '''
        
        if int(self.deviceBox.devOscBox.currentIndex()) == -1:
            self.parent.linearity_oscchanerror()
            return
        else: channel = int(self.deviceBox.devOscBox.currentIndex()) + 1

        if channel == 5: channel = 'Math'

        if int(self.deviceBox.devAFGBox.currentIndex()) == -1:
            self.parent.linearity_afgchanerror()
            return
        else: source = int(self.deviceBox.devAFGBox.currentIndex()) + 1

        try: samples = int(self.deviceBox.devSamplesBox.text().encode('ascii', 'ignore'))
        except:
            self.parent.linearity_samplestypeerror()
            return

        if samples <= 0 or not isPower(samples, 2):
            self.parent.linearity_sampleserror()
            return

        #Need to error check filename

        try: filename = self.deviceBox.devFileName.text().encode('ascii', 'ignore').strip()
        except: return

        if int(self.deviceBox.devOscBox.currentIndex()) == 4:
            if int(self.deviceBox.mathChanBoxA.currentIndex()) == -1:
                self.parent.linearity_matherror()
                return
            else: mathA = str(self.deviceBox.mathChanBoxA.currentText())

            if int(self.deviceBox.mathOperationBox.currentIndex()) == -1:
                self.parent.linearity_matherror()
                return
            else: mathOp = str(self.deviceBox.mathOperationBox.currentText())

            if int(self.deviceBox.mathChanBoxB.currentIndex()) == -1:
                self.parent.linearity_matherror()
                return
            else: mathB = str(self.deviceBox.mathChanBoxB.currentText())

            if mathA == mathB:
                self.parent.linearity_matherror2()
                return
            else: mathexpression = ' ' + mathA + mathOp + mathB
        else: mathexpression = None

        self.controlButton.setDisabled(1)
        self.enable_test()
        self.parent.AFG.write_outputstate(source, 'on')
        self.parent.scope.write_channelstate(channel, 'on')
        dictionary = {'channel': channel, 'source': source, 'samples': samples, 'filename': filename, 'mathexpression': mathexpression}
        self.thread = linearityThread('Device', dictionary, self)
        self.thread.resultsSignal.connect(self.parent.new_linearity_results)
        self.thread.messageSignal.connect(self.new_message)
        self.new_message('Beginning new device test.\n')
        self.threadState = 'on'
        self.thread.start()
        self.controlButton.setEnabled(1)
        
    def enable_test(self):

        '''
        This function enables testing conditions in the program by disabling other functions in the program that would
        interrupt communication with the instruments.
        '''
        
        self.threadState = 'on'
        self.parent.AFGTab.sendButton.setDisabled(1)
        self.parent.AFGTab.saveConfigButton.setDisabled(1)
        self.parent.AFGTab.loadConfigButton.setDisabled(1)
        self.parent.oscilloscopeTab.setButton.setDisabled(1)
        self.parent.oscilloscopeTab.saveStateButton.setDisabled(1)
        self.parent.oscilloscopeTab.loadStateButton.setDisabled(1)
        self.parent.oscilloscopeTab.grabWaveformButton.setDisabled(1)
        self.parent.oscilloscopeTab.sendRefButton.setDisabled(1)
        self.parent.oscilloscopeTab.chanBox1.setDisabled(1)
        self.parent.oscilloscopeTab.chanBox2.setDisabled(1)
        self.parent.oscilloscopeTab.chanBox3.setDisabled(1)
        self.parent.oscilloscopeTab.chanBox4.setDisabled(1)
        self.parent.autoTab.singleTestButton.setDisabled(1)
        self.parent.autoTab.multiTestButton.setDisabled(1)
        self.parent.autoTab.fullTestButton.setDisabled(1)
        self.controlButton.clicked.disconnect()
        self.controlButton.clicked.connect(self.quit_test)
        self.controlButton.setText('Quit Test')

    @Slot()
    def quit_test(self):

        '''
        This function kills an active thread, exits the thread, and then disables testing conditions in the program.
        '''

        self.thread.kill()
        self.thread.exit()
        self.threadState = 'off'
        self.parent.AFGTab.sendButton.setEnabled(1)
        self.parent.AFGTab.saveConfigButton.setEnabled(1)
        self.parent.AFGTab.loadConfigButton.setEnabled(1)
        self.parent.oscilloscopeTab.setButton.setEnabled(1)
        self.parent.oscilloscopeTab.saveStateButton.setEnabled(1)
        self.parent.oscilloscopeTab.loadStateButton.setEnabled(1)
        self.parent.oscilloscopeTab.grabWaveformButton.setEnabled(1)
        self.parent.oscilloscopeTab.sendRefButton.setEnabled(1)
        self.parent.oscilloscopeTab.chanBox1.setEnabled(1)
        self.parent.oscilloscopeTab.chanBox2.setEnabled(1)
        self.parent.oscilloscopeTab.chanBox3.setEnabled(1)
        self.parent.oscilloscopeTab.chanBox4.setEnabled(1)
        self.parent.autoTab.singleTestButton.setEnabled(1)
        self.parent.autoTab.multiTestButton.setEnabled(1)
        self.parent.autoTab.fullTestButton.setEnabled(1)
        self.controlButton.clicked.disconnect()
        self.change_button_name()
        self.controlButton.clicked.connect(self.run_test)
        self.new_message('Test has concluded.\n')

    @Slot()
    def change_button_name(self):

        '''
        This button changes the name of the control button to reflect which test has been selected.
        '''

        if self.threadState == 'off' or self.threadState == 'done':
            if self.linearityBox.button.isChecked(): self.controlButton.setText('Run Linearity Test')
            elif self.calibrationBox.button.isChecked(): self.controlButton.setText('Run Calibration Test')
            else: self.controlButton.setText('Run Device Test')
        else:
            return

    @Slot()
    def new_message(self, message):

        '''
        This function writes a new message to the test status box.

        Arguments: Message - The message that is written to the status box.
        '''
        
        self.statustext += '\n>>>' + message
        self.statusBox.setText(self.statustext)
        self.statusBox.moveCursor(QTextCursor.End)

class testBox(QWidget):

    '''
    testBox is the base class for the group box that holds the test setup for each test in linearityTab. It places a
    radio button next to the title of the groupbox, so the user can select which test they want to run.
    '''
    
    def __init__(self, title, parent=None):
        QWidget.__init__(self)

        self.layout = QVBoxLayout(self)
        self.groupBox = QGroupBox('        %s' %title, self)
        
        self.button = QRadioButton(parent=self)
        self.layout.addWidget(self.groupBox)

        self.button.move(25, 3)

class linearityBox(testBox):

    '''
    linearityBox is the group box that holds the test setup for a linearity test.

    Functions: enable_advanced, enable_disable_math
    '''

    def __init__(self, parent=None):

        testBox.__init__(self, 'Linearity Test Setup', parent)
        
        self.Layout = QVBoxLayout()

        #Line 1 for setup
        self.line1Layout = QHBoxLayout()

        #Label/line edit for indicating start voltage
        self.startvBox = QLineEdit()
        self.startvBox.setFixedWidth(40)

        #Label/line edit for indicating end voltage
        self.endvBox = QLineEdit()
        self.endvBox.setFixedWidth(40)

        #Formatting first line of the setup panel
        self.line1Layout.addWidget(QLabel('Start voltage: '), alignment=Qt.AlignLeft)
        self.line1Layout.addWidget(self.startvBox, alignment=Qt.AlignLeft)
        self.line1Layout.addSpacing(30)
        self.line1Layout.addWidget(QLabel('End voltage: '), alignment=Qt.AlignLeft)
        self.line1Layout.addWidget(self.endvBox, alignment=Qt.AlignLeft)
        self.line1Layout.setAlignment(Qt.AlignLeft)

        #Line edit for indicating the test resolution
        self.numBox = QLineEdit()
        self.numBox.setFixedWidth(40)

        #Line edit for indicating the number of samples
        self.linSamplesBox = QLineEdit()
        self.linSamplesBox.setFixedWidth(40)

        #Formatting the second line
        self.line2Layout = QHBoxLayout()
        self.line2Layout.addWidget(QLabel('Number of test points: '), alignment=Qt.AlignLeft)
        self.line2Layout.addWidget(self.numBox, alignment=Qt.AlignLeft)
        self.line2Layout.addSpacing(30)
        self.line2Layout.addWidget(QLabel('Number of Samples: '), alignment=Qt.AlignLeft)
        self.line2Layout.addWidget(self.linSamplesBox, alignment=Qt.AlignLeft)
        self.line2Layout.setAlignment(Qt.AlignLeft)

        #Third line of the test setup
        self.line3Layout = QHBoxLayout()

        #Combo box for selecting oscilloscope channel
        self.oscChanBox = QComboBox()
        self.oscChanBox.addItems(['1', '2', '3', '4', 'Math'])
        self.oscChanBox.setCurrentIndex(-1)
        self.oscChanBox.setFixedWidth(50)
        self.oscChanBox.currentIndexChanged.connect(self.enable_disable_math)

        #Combo box for selecting afg channel
        self.afgChanBox = QComboBox()
        self.afgChanBox.addItems(['1', '2'])
        self.afgChanBox.setCurrentIndex(-1)
        self.afgChanBox.setFixedWidth(40)

        #Formatting third line of the setup panel
        self.line3Layout.addWidget(QLabel('Oscilloscope Channel: '), alignment=Qt.AlignLeft)
        self.line3Layout.addWidget(self.oscChanBox, alignment=Qt.AlignLeft)
        self.line3Layout.addSpacing(30)
        self.line3Layout.addWidget(QLabel('Function Generator Channel: '), alignment=Qt.AlignLeft)
        self.line3Layout.addWidget(self.afgChanBox, alignment=Qt.AlignLeft)
        self.line3Layout.setAlignment(Qt.AlignLeft)

        #Fourth line of the setup panel
        self.line4Layout = QHBoxLayout()

        #Combo box for selecting first channel used for math
        self.mathChanBoxA = QComboBox()
        self.mathChanBoxA.addItems(['CH1', 'CH2', 'CH3', 'CH4'])
        self.mathChanBoxA.setFixedWidth(50)
        self.mathChanBoxA.setDisabled(1)
        self.mathChanBoxA.setCurrentIndex(-1)

        #Combo box for selecting the operation performed on channels in math
        self.mathOperationBox = QComboBox()
        self.mathOperationBox.addItems(['+', '-', '*', '/'])
        self.mathOperationBox.setFixedWidth(40)
        self.mathOperationBox.setDisabled(1)
        self.mathOperationBox.setCurrentIndex(-1)

        #Combo box for selecting second channel used for math
        self.mathChanBoxB = QComboBox()
        self.mathChanBoxB.addItems(['CH1', 'CH2', 'CH3', 'CH4'])
        self.mathChanBoxB.setFixedWidth(50)
        self.mathChanBoxB.setDisabled(1)
        self.mathChanBoxB.setCurrentIndex(-1)

        #Formatting the fourth line
        self.line4Layout.addWidget(QLabel('Write Math Operation: '), alignment=Qt.AlignLeft)
        self.line4Layout.addWidget(self.mathChanBoxA, alignment=Qt.AlignLeft)
        self.line4Layout.addWidget(self.mathOperationBox, alignment=Qt.AlignLeft)
        self.line4Layout.addWidget(self.mathChanBoxB, alignment=Qt.AlignLeft)
        self.line4Layout.setAlignment(Qt.AlignLeft)        

        #Fifth line of the setup panel
        self.line5Layout = QHBoxLayout()

        #Combo box for indicating output flag
        self.outputBox = QComboBox()
        self.outputBox.addItems(['max', 'min', 'max-min', 'gain', 'advanced'])
        self.outputBox.setCurrentIndex(-1)
        self.outputBox.currentIndexChanged.connect(self.enable_advanced)
        self.outputBox.setFixedWidth(70)

        #Line for writing advanced output
        self.advancedLine = QLineEdit()
        self.advancedLine.setDisabled(1)
        self.advancedLine.setFixedWidth(100)

        #Formatting fifth line of the setup panel
        self.line5Layout.addWidget(QLabel('Output Flag: '), alignment=Qt.AlignLeft)
        self.line5Layout.addWidget(self.outputBox, alignment=Qt.AlignLeft)
        self.line5Layout.addSpacing(30)
        self.line5Layout.addWidget(QLabel('Advanced Output: '), alignment=Qt.AlignLeft)
        self.line5Layout.addWidget(self.advancedLine, alignment=Qt.AlignLeft)

        #Formatting the group box
        self.Layout.addLayout(self.line1Layout)
        self.Layout.addLayout(self.line2Layout)
        self.Layout.addLayout(self.line3Layout)
        self.Layout.addLayout(self.line4Layout)
        self.Layout.addLayout(self.line5Layout)
        self.groupBox.setLayout(self.Layout)
        self.setFixedWidth(425)

    @Slot()
    def enable_advanced(self):

        '''
        This function enables or disables the advanced output line based on which output flag has been selected.
        '''

        self.advancedLine.setEnabled(1) if int(self.outputBox.currentIndex()) == 4 else self.advancedLine.setDisabled(1)

    @Slot()
    def enable_disable_math(self, index):

        '''
        This function enables or disables math input based on which oscilloscope channel has been selected.
        '''

        if index == 4:
            self.mathChanBoxA.setEnabled(1)
            self.mathOperationBox.setEnabled(1)
            self.mathChanBoxB.setEnabled(1)
        else:
            self.mathChanBoxA.setDisabled(1)
            self.mathOperationBox.setDisabled(1)
            self.mathChanBoxB.setDisabled(1)

class calibrationBox(testBox):

    '''
    calibrationBox is the groupBox that holds the setup panel for a calibration test.

    Functions: load_calibrationfile, enable_disable_math
    '''

    def __init__(self, parent=None):

        testBox.__init__(self, 'Calibration Test Setup', parent)

        #Line for indicating the tolerance level
        self.toleranceBox = QLineEdit()
        self.toleranceBox.setFixedWidth(40)

        #Line for indicating the number of samples
        self.samplesBox = QLineEdit()
        self.samplesBox.setFixedWidth(40)

        #Combo box for indicating the oscilloscope channel
        self.calOscBox = QComboBox()
        self.calOscBox.addItems(['1', '2', '3', '4', 'Math'])
        self.calOscBox.setCurrentIndex(-1)
        self.calOscBox.currentIndexChanged.connect(self.enable_disable_math)

        #Combo box for indicating the function generator channel
        self.calAFGBox = QComboBox()
        self.calAFGBox.addItems(['1', '2'])
        self.calAFGBox.setCurrentIndex(-1)

        #Combo box for selecting the first channel used for math
        self.mathChanBoxA = QComboBox()
        self.mathChanBoxA.addItems(['CH1', 'CH2', 'CH3', 'CH4'])
        self.mathChanBoxA.setFixedWidth(50)
        self.mathChanBoxA.setDisabled(1)
        self.mathChanBoxA.setCurrentIndex(-1)

        #Combo box for selecting the operation performed in math
        self.mathOperationBox = QComboBox()
        self.mathOperationBox.addItems(['+', '-', '*', '/'])
        self.mathOperationBox.setFixedWidth(40)
        self.mathOperationBox.setDisabled(1)
        self.mathOperationBox.setCurrentIndex(-1)

        #Combo box for selecting the second channel used for math
        self.mathChanBoxB = QComboBox()
        self.mathChanBoxB.addItems(['CH1', 'CH2', 'CH3', 'CH4'])
        self.mathChanBoxB.setFixedWidth(50)
        self.mathChanBoxB.setDisabled(1)
        self.mathChanBoxB.setCurrentIndex(-1)

        #Box for previewing the input linearity results file name
        self.calFileName = QLineEdit()
        self.calFileName.setReadOnly(1)

        #Button for selecting the input linearity results file name
        self.calFileButton = QPushButton('...')
        self.calFileButton.setFixedWidth(50)
        self.calFileButton.clicked.connect(self.load_calibrationfile)

        #Formatting calibration box
        self.Layout = QVBoxLayout()
        self.line1Layout = QHBoxLayout()
        self.line2Layout = QHBoxLayout()
        self.line3Layout = QHBoxLayout()
        self.line4Layout = QHBoxLayout()

        self.line1Layout.addWidget(QLabel('Percent Tolerance: '))
        self.line1Layout.addWidget(self.toleranceBox)
        self.line1Layout.addSpacing(30)
        self.line1Layout.addWidget(QLabel('Number of Samples: '))
        self.line1Layout.addWidget(self.samplesBox)

        self.line2Layout.addWidget(QLabel('Oscilloscope Channel: '))
        self.line2Layout.addWidget(self.calOscBox)
        self.line2Layout.addSpacing(30)
        self.line2Layout.addWidget(QLabel('Function Generator Channel: '))
        self.line2Layout.addWidget(self.calAFGBox)

        self.line3Layout.addWidget(QLabel('Write Math Operation: '), alignment=Qt.AlignLeft)
        self.line3Layout.addWidget(self.mathChanBoxA, alignment=Qt.AlignLeft)
        self.line3Layout.addWidget(self.mathOperationBox, alignment=Qt.AlignLeft)
        self.line3Layout.addWidget(self.mathChanBoxB, alignment=Qt.AlignLeft)
        self.line3Layout.setAlignment(Qt.AlignLeft)  

        self.line4Layout.addWidget(QLabel('Select File: '))
        self.line4Layout.addWidget(self.calFileName)
        self.line4Layout.addWidget(self.calFileButton)

        self.Layout.addLayout(self.line1Layout)
        self.Layout.addLayout(self.line2Layout)
        self.Layout.addLayout(self.line3Layout)
        self.Layout.addLayout(self.line4Layout)
        self.groupBox.setLayout(self.Layout)
        self.setFixedWidth(425)

    @Slot()
    def load_calibrationfile(self):

        '''
        This function opens a file dialog for allowing the user to choose an input file.
        It then stores the file name for using during the linearity test.
        '''
        
        chooseFile = QFileDialog()
        chooseFile.setNameFilter('Excel Files (*.xlsx)')
        if chooseFile.exec_():
            fileName = chooseFile.selectedFiles()
            fileName = fileName[0]
            fileName.encode('ascii', 'ignore')

            self.calFileName.setText(fileName)

    @Slot()
    def enable_disable_math(self, index):

        '''
        This function enables or disables the math combo boxes based on which oscilloscope channel has been selected.
        '''

        if index == 4:
            self.mathChanBoxA.setEnabled(1)
            self.mathOperationBox.setEnabled(1)
            self.mathChanBoxB.setEnabled(1)
        else:
            self.mathChanBoxA.setDisabled(1)
            self.mathOperationBox.setDisabled(1)
            self.mathChanBoxB.setDisabled(1)


class deviceBox(testBox):

    '''
    deviceBox is the groupBox that holds the setup panel for the device test.

    Functions: load_devicefile, enable_disable_math
    '''

    def __init__(self, parent=None):
        testBox.__init__(self, 'Device Test Setup', parent)

        #Lines for holding the controls
        self.line1Layout = QHBoxLayout()
        self.line2Layout = QHBoxLayout()
        self.line3Layout = QHBoxLayout()
        self.line4Layout = QHBoxLayout()

        #Combo box for selecting the oscilloscope channel
        self.devOscBox = QComboBox()
        self.devOscBox.addItems(['1', '2', '3', '4', 'Math'])
        self.devOscBox.setCurrentIndex(-1)
        self.devOscBox.currentIndexChanged.connect(self.enable_disable_math)

        #Combo box for selecting the function generator channel
        self.devAFGBox = QComboBox()
        self.devAFGBox.addItems(['1', '2'])
        self.devAFGBox.setCurrentIndex(-1)

        #Combo box for selecting the first channel used for math
        self.mathChanBoxA = QComboBox()
        self.mathChanBoxA.addItems(['CH1', 'CH2', 'CH3', 'CH4'])
        self.mathChanBoxA.setFixedWidth(50)
        self.mathChanBoxA.setDisabled(1)
        self.mathChanBoxA.setCurrentIndex(-1)

        #Combo box for selecting the operation used in math
        self.mathOperationBox = QComboBox()
        self.mathOperationBox.addItems(['+', '-', '*', '/'])
        self.mathOperationBox.setFixedWidth(40)
        self.mathOperationBox.setDisabled(1)
        self.mathOperationBox.setCurrentIndex(-1)

        #Combo box for selecting the second channel used for math
        self.mathChanBoxB = QComboBox()
        self.mathChanBoxB.addItems(['CH1', 'CH2', 'CH3', 'CH4'])
        self.mathChanBoxB.setFixedWidth(50)
        self.mathChanBoxB.setDisabled(1)
        self.mathChanBoxB.setCurrentIndex(-1)

        #Line for indicating the number of samples
        self.devSamplesBox = QLineEdit()
        self.devSamplesBox.setFixedWidth(40)

        #Line for showing the calibration test input file name
        self.devFileName = QLineEdit()
        self.devFileName.setReadOnly(1)

        #Button for selecting the calibration test input file name
        self.devFileButton = QPushButton('...')
        self.devFileButton.setFixedWidth(50)
        self.devFileButton.clicked.connect(self.load_devicefile)

        #Formatting device box
        self.line1Layout.addWidget(QLabel('Oscilloscope Channel: '))
        self.line1Layout.addWidget(self.devOscBox)
        self.line1Layout.addSpacing(30)
        self.line1Layout.addWidget(QLabel('Function Generator Channel: '))
        self.line1Layout.addWidget(self.devAFGBox)

        self.line2Layout.addWidget(QLabel('Number of Samples: '), alignment=Qt.AlignLeft)
        self.line2Layout.addWidget(self.devSamplesBox, alignment=Qt.AlignLeft)
        self.line2Layout.setAlignment(Qt.AlignLeft)

        self.line3Layout.addWidget(QLabel('Write Math Operation: '), alignment=Qt.AlignLeft)
        self.line3Layout.addWidget(self.mathChanBoxA, alignment=Qt.AlignLeft)
        self.line3Layout.addWidget(self.mathOperationBox, alignment=Qt.AlignLeft)
        self.line3Layout.addWidget(self.mathChanBoxB, alignment=Qt.AlignLeft)
        self.line3Layout.setAlignment(Qt.AlignLeft)  
        
        self.line4Layout.addWidget(QLabel('Select File: '))
        self.line4Layout.addWidget(self.devFileName)
        self.line4Layout.addWidget(self.devFileButton)

        self.Layout = QVBoxLayout()
        self.Layout.addLayout(self.line1Layout)
        self.Layout.addLayout(self.line2Layout)
        self.Layout.addLayout(self.line3Layout)
        self.Layout.addLayout(self.line4Layout)
        
        self.groupBox.setLayout(self.Layout)

    @Slot()
    def load_devicefile(self):

        '''
        This function opens a file dialog for selecting the name of the input calibration results file.
        '''

        chooseFile = QFileDialog()
        chooseFile.setNameFilter('Excel Files (*.xlsx)')
        if chooseFile.exec_():
            fileName = chooseFile.selectedFiles()
            fileName = fileName[0]
            fileName.encode('ascii', 'ignore')

            self.devFileName.setText(fileName)

    @Slot()
    def enable_disable_math(self, index):

        '''
        This function enables or disables the math combo boxes based on which oscilloscope channel has been selected.
        '''

        if index == 4:
            self.mathChanBoxA.setEnabled(1)
            self.mathOperationBox.setEnabled(1)
            self.mathChanBoxB.setEnabled(1)
        else:
            self.mathChanBoxA.setDisabled(1)
            self.mathOperationBox.setDisabled(1)
            self.mathChanBoxB.setDisabled(1)
            
def isPower (num, base):

    '''
    This function checks to see if a number is a power of a base. It is used in linearityTab for checking that the number of samples
    is a power of 2.
    '''
    
    if base == 1 and num != 1: return False
    if base == 1 and num == 1: return True
    if base == 0 and num != 1: return False
    power = int(log (num, base) + 0.5)
    return base ** power == num

#For previewing the tab outside of the main application.
if __name__ == '__main__':
    
    qtApp = QApplication(sys.argv)
    linearityWindow = linearityTab()   
    linearityWindow.show()
    sys.exit(qtApp.exec_())
