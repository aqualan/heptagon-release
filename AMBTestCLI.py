'''
Tests one grouping from the analog mezzanine board.

The oscilloscope must be configured as the following:
Channel 1 - negative ADC differential output from AMB testing board
Channel 2 - positive ADC differential output from AMB testing board
Channel 3 - summing output from AMB testing board

The following measurement channels are used:
1: Analog Channel 1, input
2: Analog Channel 2, input
3: Analog Channel 3, input
4: Analog Channel 4, input
5: Reference Channel 1, input
6: Reference Channel 2, input
7: Reference Channel 3, input
8: Reference Channel 4, input

Usage: 	Execute directly from command line with `python AMBTest.py`. Make sure 
		that you are plugged into the oscilloscope via USB, and that the
		scopedescriptor constant is correct

Arguments: 	None

'''

# For the 4034 scope in Mitch's Lab
# scopedescriptor = 'USB0::0x0699::0x040B::C021222::INSTR'

# For the 4054 scope in Mitch's Lab
scopedescriptor = 'USB0::0x0699::0x0401::C020278::INSTR'

# For the 4104 scope in Mitch's Lab
#scopedescriptor = 'USB0::0x0699::0x0401::c000455::INSTR'

# Constant for checking acceptability of signals; given in %
AMPTOLERANCE   = 0.05 	# 5%
RISETOLERANCE  = 0.05 	# 5%
PHASETOLERANCE = 5e-3	# 0.005 %
# Amount to add or subtract when setting high/low reference levels
DELTAREF = 10E-3 # 10mV
# Getting this value back usually indicates something gone bad
ERROR = 99100000000000005192630646116011999232.000000
# Wait for data delay
DELAY = 1
# Average # of acquisitions by which to get measurements
# 16 has been the status quo for awhile.
NUMACQ = 16

import visa
import collections
import AMBOscilloscopeSetup as init
import loadwaveform as load
import readfromfile
import time

scope = None
afg   = None

# New data type to return on measurement checks
waveform = collections.namedtuple("Waveform", ["riseTime", "peakAmp", "phase"])

### ----- HELPER METHODS ----- ###

# Setup
def initialize(scopedescriptor):
	rm = visa.ResourceManager()
	scope = rm.get_instrument(scopedescriptor)
	scope.write('header off')

numavg = NUMACQ
init.setup(scopedescriptor, numavg)

# Asks whether or not the current references are the right ones to use.
def checkRefs():
	while(True):
		userinput = raw_input("Take a look at the reference waveforms on the" + 
							  " scope. Are they correct? (Y\N)\n>>> ").lower()
		if (userinput == 'y' or userinput == "yes"):
			break
		elif (userinput == 'n' or userinput == "no"):
			raw_input("Please set the references accordingly, and then press" +
					  " any key to continue. ")
			break
		else:
			print("Input not recognized. Please try again.")

# Gets pertinent wave information - rise time, amplitude, and phase
def measureWave(channel, source):
	print("\nMeasuring parameters for Meas%d, waveform %s." %(channel, source))

	scope.write("measurement:gating screen; :measurement:meas%d:state on;" + 
				"type rise; source1 %s" 
				%(channel, source))

	# Get minimum value
	scope.write("measurement:meas%d:type mini" %(channel))
	time.sleep(DELAY)
	abslow = float(scope.ask("measurement:meas%d:value?" 
							 %(channel)).strip())
	print("Got a minimum value on %s of %s" 
		  %(source, abslow))
	
	# Get maximum value
	scope.write("measurement:meas%d:type max" %(channel))
	time.sleep(DELAY)
	abshigh = float(scope.ask("measurement:meas%d:value?" 
							  %(channel)).strip())
	print("Got a maximum value on %s of %s" 
		  %(source, abshigh))
	
	# Add leeway to each
	abshigh -= DELTAREF
	abslow  += DELTAREF
	print("After adding deltas, max and min are %s and %s, respectively." 
		  %(abshigh, abslow))

	scope.write("measurement:reflevel:method absolute;" +
				" :measurement:reflevel:absolute:low %f; high %f" 
				%(abslow, abshigh))

	time.sleep(DELAY) # Delay gives time for the scope to write data to cache
	risetime = float(scope.ask("measurement:meas%d:value?" %(channel)).strip())

	if (channel != 1 or channel != 5):
		scope.write("measurement:meas%d:type amplitude" %(channel))
		time.sleep(DELAY)
		amplitude = float(scope.ask("measurement:meas%d:value?" 
									%(channel)).strip())

	if (channel == 5 or channel == 6 or channel == 7 or channel == 8):
		# Reference waveforms have no phase to compare against
		w = waveform(risetime, amplitude, 0)
	elif (channel == 1 or channel == 2 or channel == 3 or channel == 4):
		# Whereas the input waveforms can compare against reference; 
		# all channels are 4 less than their corresponding waveform 
		scope.write("measurement:meas%d:source2 ref%d; type phase" 
					%(channel, channel+4))
		time.sleep(DELAY)
		phase = float(scope.ask("measurement:meas%d:value?" 
								%(channel)) .strip())
		w = waveform(risetime, amplitude, phase)

	# Print out values to see on console
	print("Risetime: %s // Amplitude: %s" 
		  %(risetime, amplitude))
	return w

# Checks an input wave's measurements against its reference
def checkParams(chID, channel, reference):
	upperBoundRiseTime = reference.riseTime * (1 + RISETOLERANCE)
	lowerBoundRiseTime = reference.riseTime * (1 - RISETOLERANCE)
	upperBoundPeakAmplitude = reference.peakAmp * (1 + AMPTOLERANCE)
	lowerBoundPeakAmplitude = reference.peakAmp * (1 - AMPTOLERANCE)
	upperBoundPhase = 360 * PHASETOLERANCE * 1
	lowerBoundPhase = 360 * PHASETOLERANCE * -1

	# Conditional booleans
	riseGood = False
	ampGood = False
	phaseGood = False

	while (not riseGood or not ampGood or not phaseGood):
		# Rise time checks
		if (channel.riseTime >= upperBoundRiseTime):
			pass
		elif (channel.riseTime <= lowerBoundRiseTime):
			pass
		else:
			print("Ch%d Rise Time : %f/%f" 
				  %(chID, channel.riseTime, reference.riseTime))
			riseGood = True

		# Amplitude checks
		if (channel.peakAmp >= upperBoundPeakAmplitude):
			pass
		elif (channel.peakAmp <= lowerBoundPeakAmplitude):
			pass
		else:
			print("Ch%d Amplitude : %f/%f" 
				  %(chID, channel.peakAmp, reference.peakAmp))
			ampGood = True

		# Phase checks
		if (channel.phase >= upperBoundPhase):
			pass
		elif (channel.phase <= lowerBoundPhase):
			pass
		else:
			print("Ch%d Phase Difference : %f" 
				  %(chID, channel.phase))
			phaseGood = True

		# Notify user of any failures
		if (not riseGood or not ampGood or not phaseGood):
			print("\nTesting failed on the following attributes for channel %d:" 
				  %(chID))
			if (not riseGood):
				print("Rise time; was %s but expected %s +/- %s" 
					  %(channel.riseTime, reference.riseTime, 
					  	RISETOLERANCE*reference.riseTime))
			if (not ampGood):
				print("Peak amplitude; was %s but expected %s +/- %s" 
					  %(channel.peakAmp, reference.peakAmp, 
					  	AMPTOLERANCE*reference.peakAmp))
			if (not phaseGood):
				print("Phase; was %s but expected %s +/- %s" 
					  %(channel.phase, reference.phase, 
					  	PHASETOLERANCE*reference.phase))
			
			shouldRetest = respondToFailure(chID)
			# If the following statement is not satisfied, then we will stay in 
			# the while loop and retest the channel parameters
			if (shouldRetest == False):
				break
		else:
			print("All attributes on this channel check out! Continuing onwards.")

# Prompt the user for input after a channel fails
def respondToFailure(chID):
	while (True):
		failureResponse = raw_input("Please verify what you see on the screen." + 
									" Enter 's' to enter scope mode for manual" + 
									" control, or enter 'O' to override. \n>>> ")
		if (failureResponse == "s"):
			scopeMode(chID)
			return True
		elif (failureResponse == "O"):
			print("\nChannel %d discrepancy overlooked; continuing test.\n" %(chID))
			return False
		else:
			print("Invalid response; please try again.")

# Detect if a user input signifies a quit
def quitOn(userinput):
	if (userinput == 'q' or userinput == 'Q'):
		return True
	else:
		return False

# Turn off all measurements and re-enable the scope so that the user can manually debug
def scopeMode(chID):
	print("\nRelinquishing control to user.\n")
	# Turn off all measurements
	for i in range (1, 9):
		scope.write("measurement:meas%d:state off" %(i))
	# Turn Channel 4 (presumed scope) on
	scope.write("select:ch4 on")

	userinput = ""
	while (not quitOn(userinput)):
		userinput = raw_input("Do your thing. Enter any key to quit. ")
		# Do things until user presses a key

	# Turn off Channel 4
	scope.write("select:ch4 off")
	# Return automatic control
	print("\nAssuming DIRECT control. Automatically retesting problem channel.\n")
	# Turn all measurements back on.
	for i in range (1, 9):
		scope.write("measurement:meas%d:state on" %(i))

### ===== MAIN METHOD ===== ###

# Step 1 - Put references on the screen
checkRefs()
# Step 2 - Take rise time and peak amplitude of references
r1 = measureWave(5, "ref1")
r2 = measureWave(6, "ref2")
r3 = measureWave(7, "ref3")
r4 = measureWave(8, "ref4")
ch1 = measureWave(1, "ch1")
checkParams(1, ch1, r1)
ch2 = measureWave(2, "ch2")
checkParams(2, ch2, r2)
ch3 = measureWave(3, "ch3")
checkParams(3, ch3, r3)
ch4 = measureWave(4, "math")
checkParams(4, ch4, r4)

# Clean up by removing all measurements from system.
for i in range (0, 9):
	scope.write("measurement:meas%d:state off" %(i))

print("\nTesting is complete on this grouping. Please switch to the next one.")
