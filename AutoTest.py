'''
This program allows you to check against up to 3 input waveforms plus MATH against
up to 4 references programmed on an Oscilloscope. It uses the VISA library for
communication, and is interfaced with the Heptagon GUI written by A. Ashery.
'''

'''
TODOs:
Pi switching can be more dynamic - check for when a confirmation gets back 
instead of waiting an arbitrary 2 seconds
'''

import visa
import collections
import time
import interpandtransform as interpol
from struct import pack
import socket
from thread import start_new_thread
import sys
import Queue

from PySide.QtCore import Signal
from kthread import KThread, KException

# Reference skew
DELTAREF = 10e-3
# Time in seconds to wait before asking for a measurement result
DELAY    = 1
# Getting this value back usually indicates something has gone wrong
ERROR = 99100000000000005192630646116011999232.000000

# Queue for instructions to send to the switchboard
insnQueue = Queue.Queue()

# New data type to return on measurement checks
waveform = collections.namedtuple("Waveform",
	["risetime", "amplitude", "phase"])

class testThread(KThread):

	messageSignal = Signal(str)
	errorSignal = Signal()
	resultsSignal = Signal()

	def __init__(self, scope, whichtest, argument, parent):

		KThread.__init__(self, parent)
		self.scope = scope

		self.parent = parent

		self.type = whichtest

		self.channel = argument['channel'] # *** Bools, on or off; on the SCOPE
		self.references = argument['reference'] # Ints, corresponding to channel above; can be all identical.
		# Constant for checking acceptability of signals; given in %
		self.boardchannel = argument['boardchannel'] # What on the AMB board is being tested? 40 Bools
		self.AMPTOLERANCE   = argument['amplitude']
		self.RISETOLERANCE  = argument['risetime']
		self.PHASETOLERANCE = argument['phase']

	# Whenever a new connection is made, a new thread is made to
	# respond to it
	def clientthread(self, conn):
		# Infinite loop so that functions do not terminate and the thread does not
		# end
		while True:
			# Receiving from client
			reply = ""
			data = conn.recv(1024)
			if (data == "Hey, got anything for me yet?"):
				if (not insnQueue.empty()):
					self.messageSignal.emit("Dispatching new insn.")
					reply = insnQueue.get()
				else:
					reply = "Sorry man; I got nothing."
			if (data[0:10] == "Confirmed:"):
				self.messageSignal.emit("Client confirmed that he last got:%s" %(data[10:]))
			if not data:
				break

			self.messageSignal.emit("Sending reply")
			conn.send(reply)

		self.messageSignal.emit("Client terminated connection.")
		conn.close()
		
	def run(self):

                try:
                        # Set up socket
                        HOST = '' # Intentionally blank
                        PORT = 8888
                        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        self.messageSignal.emit("Server: Socket created.")

                        try:
                                s.bind((HOST, PORT))
                        except socket.error, msg:
                                self.messageSignal.emit('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
                                return
                        self.messageSignal.emit("Socket bind complete.")

                        s.listen(5) # Allow up to 5 simultaneous connections

                        # Wait for controller to connect if needed (Multi/Full)
                        if (self.type != 'Single'):
                                while True:
                                        conn, addr = s.accept()
                                        self.messageSignal.emit("Connected with " + addr[0] + ":" + 
                                                str(addr[1]))
                                        start_new_thread(clientthread, (conn,)) # COMMA NECESSARY
                                        break
                
			if (self.type == 'Single'):
				self.test(self.channel, self.references)
			elif (self.type == 'Multi'):
				'''TODO: Operate on # of channels'''
				# For the purposes of proof of concept, this right now is set.
				# In the future, this should take the specific channel and ref
				# passed in by the GUI.

				# Tell the Pi to test this specific channel and readback data
				insnQueue.put("ICH:09:ON")
				insnQueue.put("OCH:09:ON")
				time.sleep(2) # Allow time for Pi to perform switching
				self.test(self.source, self.channel, self.references)
				insnQueue.put("ICH:09:OFF")
				insnQueue.put("OCH:09:OFF")
				time.sleep(2) # Allow time for Pi to perform switching

				pass
			else:
				# Full-auto (40)
				for i in range(1, 41):
					# Necessary to make the numbers appear as 2-digit string
					number = ""
					if (i < 10):
						number = "0" + str(i)
					else:
						number = str(i)

					insnQueue.put("ICH:%s:ON" %(number))
					insnQueue.put("OCH:%s:ON" %(number))
					time.sleep(2) # Allow time for Pi to perform switching
					self.test(self.source, self.channel, self.reference)
					insnQueue.put("ICH:%s:OFF" %(number))
					insnQueue.put("OCH:%s:OFF" %(number))
					time.sleep(2) # Allow time for Pi to perform switching

		except KException: 
			return
		finally:
			self.messageSignal.emit("Shutting down server.")
			s.close()

	### ----- HELPER METHODS ----- ###
	# Run the tests
	def test(self, channel, references):
		# Dynamically test based on number of refs to check.
		if (len(references) >= 1):
			r1 = self.measureWave(5, references[0])
			ch1 = self.measureWave(1, channel[0])
			self.checkParams(1, ch1, r1)
		if (len(references) >= 2):
			r2 = self.measureWave(6, references[1])
			ch2 = self.measureWave(2, channel[1])
			self.checkParams(2, ch2, r2)
		if (len(references) >= 3):
			r3 = self.measureWave(7, references[2])
			ch3 = self.measureWave(3, channel[2])
			self.checkParams(3, ch3, r3)
		if (len(references) >= 4):
			r4 = self.measureWave(8, references[3])
			ch4 = self.measureWave(4, channel[3])
			self.checkParams(4, ch4, r4)

	# Gets pertinent wave information - risetime, amplitude, and phase
	def measureWave(self, channel, source):
		self.messageSignal.emit("\nMeasuring parameters for Meas%d, waveform %s." %(channel, source))

		# Setup the scope
		scope.set_gatingscreen()
		scope.set_measurementstate(channel, "on")
		scope.set_measurementsource(1, channel, source)

		# Get minimum reference value
		abslow = scope.ask_measurementvalue(channel, "mini", DELAY)
		self.messageSignal.emit("Got a reference minimum value on %s of %s" %(source, abslow))

		# Get maximum reference value
		abshigh = scope.ask_measurementvalue(channel, "max", DELAY)
		self.messageSignal.emit("Got a reference maximum value on %s of %s" %(source, abshigh))

		# Add some leeway to each
		abshigh -= DELTAREF
		abslow  += DELTAREF
		self.messageSignal.emit("After adding deltas, max and min are %s and %s, respectively.")

		# Set reference levels
		scope.set_reflevels(abslow, abshigh, DELAY)
		risetime = scope.ask_measurementvalue(channel, "rise", DELAY)
		amplitude = scope.ask_measurementvalue(channel, "amplitude", DELAY)

		if (channel == 5 or channel == 6 or channel == 7 or channel == 8):
			# Reference waveforms have no phase to compare against.
			w = waveform(risetime, amplitude, 0)
		elif (channel == 1 or channel == 2 or channel == 3 or channel == 4):
			# Whereas the input waveforms can compare against their reference;
			# All channels are 4 less than their corresponding waveform
			scope.set_measurementsource(2, channel, channel+4)
			phase = scope.ask_measurementvalue(channel, "phase", DELAY)
			w = waveform(risetime, amplitude, phase)

		# Pirnt out values to see on console
		self.messageSignal.emit("Risetime: %s // Amplitude: %s"
			%(risetime, amplitude))
		return w

	# Check an input wave's measurements against its reference
	def checkParams(self, chID, channel, reference):
		upperBoundRisetime = reference.risetime * (1 + RISETOLERANCE)
		lowerBoundRisetime = reference.risetime * (1 - RISETOLERANCE)
		upperBoundAmplitude = reference.amplitude * (1 + AMPTOLERANCE)
		lowerBoundAmplitude = reference.amplitude * (1 - AMPTOLERANCE)
		upperBoundPhase = reference.phase * (1 + PHASETOLERANCE)
		lowerBoundPhase = reference.phase * (1 - PHASETOLERANCE)	

		# Conditional booleans for the following comparisons
		riseGood = False
		ampGood = False
		phaseGood = False

		# Repeatedly check parameters until everything passes
		while (not riseGood or not ampGood or not phaseGood):
			# Rise time checks
			if (channel.risetime   >= upperBoundRisetime):
				pass
			elif (channel.risetime <= lowerBoundRisetime):
				pass
			else:
				self.messageSignal.emit("Ch%d Risetime: %f/%f"
					%(chID, channel.risetime, reference.risetime))
				riseGood = True

			# Amplitude checks
			if (channel.amplitude >= upperBoundAmplitude):
				pass
			elif (channel.amplitude <= lowerBoundAmplitude):
				pass
			else:
				self.messageSignal.emit("Ch%d Amplitude : %f/%f"
					%(chID, channel.amplitude, reference.amplitude))
				ampGood = True

			# Phase checks
			if (channel.phase >= upperBoundPhase):
				pass
			elif (channel.phase <= lowerBoundPhase):
				pass
			else:
				self.messageSignal.emit("Ch%d Phase Difference : %f"
					%(chID, channel.phase))
				phaseGood = True

			# Notify user of any failures
			if (not riseGood or not ampGood or not phaseGood):
				self.messageSignal.emit("\nTesting failed on the following attributes for channel %d:"
					%(chID))
				if (not riseGood):
					self.messageSignal.emit("Risetime; was %s but expected %s +/- %s"
						%(channel.risetime, reference.risetime,
						  RISETOLERANCE*reference.riseTime))
				if (not ampGood):
					self.messageSignal.emit("Amplitude; was %s but expected %s +/- %s"
						%(channel.amplitude, reference.amplitude,
						  AMPTOLERANCE*reference.amplitude))
				if (not phaseGood):
					self.messageSignal.emit("Phase; was %s but expected %s +/- %s"
						%(channel.phase, reference.phase,
						  PHASETOLERANCE*reference.phase))

                                self.respondToFailure(chID)
				# If the following statement is not satisfied, then we will stay in
				# the while loop and retest the channel parameters
				if (shouldRetest == False):
					break
			else:
				self.messageSignal.emit("All attributes on this channel check out! Continuing onwards.")

	# Prompt the user for input after a channel fails
	'''TODO : GUI-IFY'''
	def respondToFailure(self, chID):
		while (True):
			failureResponse = raw_input("Please verify what you see on the screen." + 
										" Enter 's' to enter scope mode for manual" + 
										" control, or enter 'O' to override. \n>>> ")
			self.errorSignal.emit()

			self.userResponded = None
			while self.userResponded == None: continue
			if self.userResponded == 'scopemode': self.scopeMode(chID)
			else: self.messageSignal.emit('Channel %d discrepancy overlooked; continuing test.\n' %(chID))
                        self.userResponded = None

	# Turn off all measurements and re-enable the scope so that the user can
	# manually debug
	def scopeMode(self, chID):
		self.messageSignal.emit("\nRelinquishing control to user.\n")
		# Turn off all measurements
		for i in range (1, 9):
			scope.set_measurementstate(i, "off")
		# Turn Channel 4 (presumed scope) on
		scope.write_channelstate(4, "on")

		self.ready = False
		while not self.ready: continue

		# Turn off Channel 4
		scope.write("select:ch4 off")
		scope.write_channelstate(4, "off")
		# Return automatic control
		self.messageSignal.emit("\nAssuming DIRECT control. Automatically retesting problem channel.\n")
		# Turn all measurements back on.
		for i in range (1, 9):
			scope.set_measurementstate(i, "on")
